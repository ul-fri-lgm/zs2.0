-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2017 at 12:59 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `errorage_zaznajspoznaj`
--

-- --------------------------------------------------------

--
-- Table structure for table `zs_class`
--

CREATE TABLE `zs_class` (
  `class_id` int(11) NOT NULL,
  `class_created_user_id` int(11) DEFAULT NULL,
  `class_created_user_ip` varchar(45) DEFAULT NULL,
  `class_created_datetime` datetime DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `class_description` text,
  `class_glyph` varchar(255) DEFAULT NULL,
  `class_is_public` tinyint(4) DEFAULT '0' COMMENT '0 if not deleted, 1 if deleted',
  `class_deleted` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_class`
--

INSERT INTO `zs_class` (`class_id`, `class_created_user_id`, `class_created_user_ip`, `class_created_datetime`, `class_name`, `class_description`, `class_glyph`, `class_is_public`, `class_deleted`) VALUES
(33, 9, '123.123.123.123', '2015-09-28 14:40:49', 'Končne igre (ZSSM)', 'ZSSM naj v ta razred vstavi končen nabor iger.', 'files/prikazne slike/sport.svg', 0, 0),
(34, 17, '123.123.123.123', '2015-09-29 15:13:05', 'Brajica', 'Brajica je pisava, ki slepim osebam omogoča branje in pisanje. Izumil jo je Louis Braille, po katerem se tudi imenuje.<br>Če želiš brajico bolje spoznati in se v branju in pisanju tudi preizkusiti, potem si na pravem mestu', 'files/prikazne slike/brajica.svg', 0, 0),
(35, 9, '123.123.123.123', '2015-09-30 15:56:50', 'Desetprstno tipkanje', 'Osnovna navodila za učenje tipkanja lahko odpreš s tipko F9 med izvajanjem vaje. Vajo začasno ustaviš s tipko F12 (pavza).', 'files/prikazne slike/slepo_tipkanje.svg', 0, 0),
(36, 1, '::1', '2016-08-17 12:10:22', 'adas', 'dsada', 'files/prikazne slike/kopalnica.svg', 1, 1),
(37, 1, '::1', '2016-08-19 11:24:17', 'a', 'a', 'files/prikazne slike/crke_stevilke.svg', 1, 1),
(38, 1, '::1', '2017-01-22 19:07:48', 'ab', 'abc', 'files/slike/preview.png', 0, 1),
(39, 1, '::1', '2017-01-22 19:08:42', 'abc', 'abc', 'files/prikazne slike/brajica.svg', 1, 1),
(40, 1, '::1', '2017-01-22 19:16:55', 'abc', 'acbc', 'files/prikazne slike/geometrija.svg', 0, 1),
(41, 1, '::1', '2017-02-13 11:19:12', 'Test', 'Test class', 'files/prikazne slike/geometrija.svg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zs_constant`
--

CREATE TABLE `zs_constant` (
  `constant_id` int(11) NOT NULL,
  `constant_game_id` int(11) DEFAULT NULL,
  `constant_name` varchar(45) DEFAULT NULL,
  `constant_value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_constant`
--

INSERT INTO `zs_constant` (`constant_id`, `constant_game_id`, `constant_name`, `constant_value`) VALUES
(11466, 362, 'zvocnik_off.svg', 'files/upload/14/2/zvocnik_off.svg'),
(11465, 362, 'zvocnik_on.svg', 'files/upload/14/2/zvocnik_on.svg'),
(11464, 362, 'tipkanje_uvod_curves-13.svg', 'files/upload/14/1/tipkanje_uvod_curves-13.svg'),
(11463, 362, 'tipkanje_uvod_curves-12.svg', 'files/upload/14/1/tipkanje_uvod_curves-12.svg'),
(11462, 362, 'tipkanje_uvod_curves-11.svg', 'files/upload/14/0/tipkanje_uvod_curves-11.svg'),
(11461, 362, 'tipkanje_uvod_curves-10.svg', 'files/upload/14/0/tipkanje_uvod_curves-10.svg'),
(11460, 362, 'tipkanje_uvod_curves-09.svg', 'files/upload/14/0/tipkanje_uvod_curves-09.svg'),
(11459, 362, 'tipkanje_uvod_curves-08.svg', 'files/upload/14/0/tipkanje_uvod_curves-08.svg'),
(11458, 362, 'tipkanje_uvod_curves-07.svg', 'files/upload/14/0/tipkanje_uvod_curves-07.svg'),
(11457, 362, 'tipkanje_uvod_curves-06.svg', 'files/upload/14/0/tipkanje_uvod_curves-06.svg'),
(11456, 362, 'tipkanje_uvod_curves-05.svg', 'files/upload/14/0/tipkanje_uvod_curves-05.svg'),
(11455, 362, 'tipkanje_uvod_curves-04.svg', 'files/upload/14/0/tipkanje_uvod_curves-04.svg'),
(11454, 362, 'tipkanje_uvod_curves-03.svg', 'files/upload/14/0/tipkanje_uvod_curves-03.svg'),
(11453, 362, 'tipkanje_uvod_curves-02.svg', 'files/upload/14/0/tipkanje_uvod_curves-02.svg'),
(11452, 362, 'tipkanje_uvod_curves-01.svg', 'files/upload/14/0/tipkanje_uvod_curves-01.svg'),
(11451, 362, 'D_roka.svg', 'files/upload/14/0/D_roka.svg'),
(11450, 362, 'L_roka.svg', 'files/upload/14/0/L_roka.svg'),
(11449, 362, '_manjse.mp3', 'files/tipkanje/zvoki/_manjse.mp3'),
(11448, 362, '_vecje.mp3', 'files/tipkanje/zvoki/_vecje.mp3'),
(11447, 362, '_podpicje.mp3', 'files/brajica/zvok/podpicje.mp3'),
(11446, 362, '_podcrtaj.mp3', 'files/tipkanje/zvoki/_podcrtaj.mp3'),
(11445, 362, '_enacaj.mp3', 'files/tipkanje/zvoki/_enacaj.mp3'),
(11444, 362, '_dvopicje.mp3', 'files/brajica/zvok/dvopicje.mp3'),
(11443, 362, 'ch.mp3', 'files/brajica/zvok/CC.mp3'),
(11442, 362, 'D_palec.svg', 'files/tipkanje/slike/D_palec.svg'),
(11441, 362, 'arrow-caret-up-icon.png', 'files/tipkanje/slike/arrow-caret-up-icon.png'),
(11440, 362, 'D_mezinec.svg', 'files/tipkanje/slike/D_mezinec.svg'),
(11439, 362, 'L_palec.svg', 'files/tipkanje/slike/L_palec.svg'),
(11438, 362, 'D_prstanec.svg', 'files/tipkanje/slike/D_prstanec.svg'),
(11437, 362, 'L_prstanec.svg', 'files/tipkanje/slike/L_prstanec.svg'),
(11436, 362, 'D_sredinec.svg', 'files/tipkanje/slike/D_sredinec.svg'),
(11435, 362, 'tipkovnica.svg', 'files/tipkanje/slike/tipkovnica.svg'),
(11434, 362, 'zvezda_polna.svg', 'files/tipkanje/slike/zvezda_polna.svg'),
(11432, 362, 'D.svg', 'files/tipkanje/slike/D.svg'),
(11431, 362, 'zvezda_prazna.svg', 'files/tipkanje/slike/zvezda_prazna.svg'),
(11430, 362, 'tipkovnica_curves.svg', 'files/upload/14/0/tipkovnica_curves.svg'),
(11429, 362, 'L_mezinec.svg', 'files/tipkanje/slike/L_mezinec.svg'),
(11428, 362, 'D_kazalec.svg', 'files/tipkanje/slike/D_kazalec.svg'),
(11427, 362, 'L_kazalec.svg', 'files/tipkanje/slike/L_kazalec.svg'),
(11426, 362, 'pocasi.svg', 'files/tipkanje/slike/pocasi.svg'),
(11425, 362, 'L_sredinec.svg', 'files/tipkanje/slike/L_sredinec.svg'),
(11424, 362, 'L.svg', 'files/tipkanje/slike/L.svg'),
(11423, 362, 'hitro.svg', 'files/tipkanje/slike/hitro.svg'),
(11422, 362, '_navednica.mp3', 'files/brajica/zvok/narekovj.mp3'),
(11421, 362, '9.mp3', 'files/brajica/zvok/9.mp3'),
(11420, 362, 'z.mp3', 'files/brajica/zvok/Z.mp3'),
(11419, 362, '1.mp3', 'files/brajica/zvok/1.mp3'),
(11418, 362, '_posevnica.mp3', 'files/tipkanje/zvoki/_posevnica.mp3'),
(11417, 362, 's.mp3', 'files/brajica/zvok/S.mp3'),
(11416, 362, 'n.mp3', 'files/brajica/zvok/N.mp3'),
(11415, 362, '_zvezdica.mp3', 'files/tipkanje/zvoki/_zvezdica.mp3'),
(11414, 362, '_vejica.mp3', 'files/brajica/zvok/vejica.mp3'),
(11413, 362, 'm.mp3', 'files/brajica/zvok/M.mp3'),
(11412, 362, 'd.mp3', 'files/brajica/zvok/D.mp3'),
(11411, 362, 'zh.mp3', 'files/brajica/zvok/ZZ.mp3'),
(11410, 362, 'c.mp3', 'files/brajica/zvok/C.mp3'),
(11409, 362, 'p.mp3', 'files/brajica/zvok/P.mp3'),
(11408, 362, '_dolar.mp3', 'files/tipkanje/zvoki/_dolar.mp3'),
(11407, 362, '2.mp3', 'files/brajica/zvok/2.mp3'),
(11406, 362, '_klicaj.mp3', 'files/brajica/zvok/klicaj.mp3'),
(11405, 362, '_zaklepaj.mp3', 'files/brajica/zvok/zaklepaj.mp3'),
(11404, 362, '6.mp3', 'files/brajica/zvok/6.mp3'),
(11403, 362, '4.mp3', 'files/brajica/zvok/4.mp3'),
(11402, 362, 'sh.mp3', 'files/brajica/zvok/SS.mp3'),
(11401, 362, '__napaka.wav', 'files/tipkanje/zvoki/__napaka.wav'),
(11400, 362, 'k.mp3', 'files/brajica/zvok/K.mp3'),
(11399, 362, '__konec.wav', 'files/tipkanje/zvoki/__konec.wav'),
(11398, 362, '_plus.mp3', 'files/tipkanje/zvoki/_plus.mp3'),
(11397, 362, '3.mp3', 'files/brajica/zvok/3.mp3'),
(11396, 362, 'u.mp3', 'files/brajica/zvok/U.mp3'),
(11395, 362, 'i.mp3', 'files/brajica/zvok/I.mp3'),
(11394, 362, 'prazen.svg', 'files/tipkanje/slike/prazen.svg'),
(11393, 362, '_in.mp3', 'files/tipkanje/zvoki/_in.mp3'),
(11392, 362, 'f.mp3', 'files/brajica/zvok/F.mp3'),
(11391, 362, '_pika.mp3', 'files/brajica/zvok/pika.mp3'),
(11390, 362, 'e.mp3', 'files/brajica/zvok/E.mp3'),
(11389, 362, '_oklepaj.mp3', 'files/tipkanje/zvoki/_oklepaj.mp3'),
(11388, 362, '_preslednica.mp3', 'files/tipkanje/zvoki/_preslednica.mp3'),
(11387, 362, 'l.mp3', 'files/brajica/zvok/L.mp3'),
(11386, 362, '_apostrof.mp3', 'files/tipkanje/zvoki/_apostrof.mp3'),
(11385, 362, 'v.mp3', 'files/brajica/zvok/V.mp3'),
(11384, 362, '0.mp3', 'files/brajica/zvok/0.mp3'),
(11383, 362, 'o.mp3', 'files/brajica/zvok/O.mp3'),
(11382, 362, '5.mp3', 'files/brajica/zvok/5.mp3'),
(11381, 362, '7.mp3', 'files/brajica/zvok/7.mp3'),
(11380, 362, 'g.mp3', 'files/brajica/zvok/G.mp3'),
(11379, 362, '_pomisljaj.mp3', 'files/tipkanje/zvoki/_pomisljaj.mp3'),
(11378, 362, 'b.mp3', 'files/brajica/zvok/B.mp3'),
(11377, 362, 'r.mp3', 'files/brajica/zvok/R.mp3'),
(11376, 362, 'a.mp3', 'files/brajica/zvok/A.mp3'),
(11375, 362, '_vprasaj.mp3', 'files/brajica/zvok/vprasaj.mp3'),
(11374, 362, '8.mp3', 'files/brajica/zvok/8.mp3'),
(11373, 362, '_lojtra.mp3', 'files/tipkanje/zvoki/_lojtra.mp3'),
(11372, 362, 't.mp3', 'files/brajica/zvok/T.mp3'),
(11371, 362, 'h.mp3', 'files/brajica/zvok/H.mp3'),
(11370, 362, '_procent.mp3', 'files/tipkanje/zvoki/_procent.mp3'),
(11369, 362, 'j.mp3', 'files/brajica/zvok/J.mp3'),
(11253, 359, 'FontBrailEot', 'files/brajica/font/BrailleSlo_6dot.eot'),
(11252, 359, 'FontBrailTtf', 'files/brajica/font/BrailleSlo_6dot.ttf'),
(11251, 358, 'FontBrailTtf', 'files/brajica/font/BrailleSlo_6dot.ttf'),
(11250, 358, 'FontBrailEot', 'files/brajica/font/BrailleSlo_6dot.eot'),
(11249, 357, 'FontBrailEot', 'files/brajica/font/BrailleSlo_6dot.eot'),
(11248, 357, 'FontBrailTtf', 'files/brajica/font/BrailleSlo_6dot.ttf'),
(11247, 356, 'FontBrailTtf', 'files/brajica/font/BrailleSlo_6dot.ttf'),
(11246, 356, 'FontBrailEot', 'files/brajica/font/BrailleSlo_6dot.eot'),
(11245, 356, 'UI_ZvezdaPrazna', 'files/brajica/slike/zvezda_prazna.svg'),
(11244, 356, 'stCrk', '10'),
(11243, 356, 'UI_ZvezdaPolna', 'files/brajica/slike/zvezda_polna.svg'),
(11242, 356, 'UI_Help', 'files/brajica/slike/question.png'),
(11241, 356, 'UI_SoundFolder', 'files/brajica/zvok/'),
(11240, 356, 'UI_Zvocnik_B', 'files/brajica/slike/zvocnik_B.svg'),
(11239, 356, 'UI_Zvocnik_W', 'files/brajica/slike/zvocnik_W.svg'),
(11229, 354, 'FontBrailTtf', 'files/brajica/font/BrailleSlo_6dot.ttf'),
(11228, 354, 'FontBrailEot', 'files/brajica/font/BrailleSlo_6dot.eot'),
(11227, 354, 'UI_ZvezdaPrazna', 'files/brajica/slike/zvezda_prazna.svg'),
(11225, 354, 'stCrk', '10'),
(11226, 354, 'UI_ZvezdaPolna', 'files/brajica/slike/zvezda_polna.svg'),
(11224, 354, 'UI_Help', 'files/brajica/slike/question.png'),
(11223, 354, 'UI_SoundFolder', 'files/brajica/zvok/'),
(11222, 354, 'UI_Zvocnik_B', 'files/brajica/slike/zvocnik_B.svg'),
(11221, 354, 'UI_Zvocnik_W', 'files/brajica/slike/zvocnik_W.svg'),
(10978, 337, 'UI_STAR_ROTATED', 'files/ui/games/zvezda_rotacija.svg'),
(10977, 337, 'UI_LINE', 'files/ui/games/line.svg'),
(10976, 337, 'gameType', '6'),
(10975, 337, 'UI_CharacterUnhappy', 'files/ui/games/zvezda_zalostna_senca.svg'),
(10974, 337, 'UI_ExampleImage', 'files/ui/games/nazaj.svg'),
(10973, 337, 'UI_EnlargeButtonOff', 'files/ui/games/min01.svg'),
(10972, 337, 'UI_EnlargeButtonOn', 'files/ui/games/max01.svg'),
(10971, 337, 'UI_LevelUpArrowInactive', 'files/ui/games/up_unactive.svg'),
(10970, 337, 'UI_LevelUpArrow', 'files/ui/games/up.svg'),
(10969, 337, 'UI_StarFull', 'files/ui/games/zvezda_polna.svg'),
(10968, 337, 'UI_Star', 'files/ui/games/zvezda_prazna.svg'),
(10967, 337, 'UI_ButtonExit', 'files/ui/games/x_izhod.svg'),
(10966, 337, 'UI_ArrowContinue', 'files/ui/games/naprej_igra.svg'),
(10965, 337, 'UI_Character', 'files/ui/games/star_shadow_game.svg'),
(10964, 337, 'UI_SpeechBubble', 'files/ui/games/oblacek.svg'),
(10963, 337, 'UI_ArrowBack', 'files/ui/games/nazaj_igra.svg'),
(10946, 335, 'UI_STAR_ROTATED', 'files/ui/games/zvezda_rotacija.svg'),
(10945, 335, 'gameType', '7'),
(10944, 335, 'UI_LINE', 'files/ui/games/line.svg'),
(10943, 335, 'UI_CharacterUnhappy', 'files/ui/games/zvezda_zalostna_senca.svg'),
(10942, 335, 'UI_ExampleImage', 'files/ui/games/nazaj.svg'),
(10941, 335, 'UI_EnlargeButtonOff', 'files/ui/games/min01.svg'),
(10940, 335, 'UI_EnlargeButtonOn', 'files/ui/games/max01.svg'),
(10939, 335, 'UI_LevelUpArrowInactive', 'files/ui/games/up_unactive.svg'),
(10938, 335, 'UI_LevelUpArrow', 'files/ui/games/up.svg'),
(10937, 335, 'UI_StarFull', 'files/ui/games/zvezda_polna.svg'),
(10936, 335, 'UI_Star', 'files/ui/games/zvezda_prazna.svg'),
(10935, 335, 'UI_ButtonExit', 'files/ui/games/x_izhod.svg'),
(10934, 335, 'UI_ArrowContinue', 'files/ui/games/naprej_igra.svg'),
(10933, 335, 'UI_Character', 'files/ui/games/star_shadow_game.svg'),
(10932, 335, 'UI_SpeechBubble', 'files/ui/games/oblacek.svg'),
(10931, 335, 'UI_ArrowBack', 'files/ui/games/nazaj_igra.svg'),
(10754, 323, 'UI_ArrowBack', 'files/ui/games/nazaj_igra.svg'),
(10753, 323, 'UI_SpeechBubble', 'files/ui/games/oblacek.svg'),
(10752, 323, 'UI_Character', 'files/ui/games/star_shadow_game.svg'),
(10751, 323, 'UI_ArrowContinue', 'files/ui/games/naprej_igra.svg'),
(10750, 323, 'UI_ButtonExit', 'files/ui/games/x_izhod.svg'),
(10749, 323, 'UI_Star', 'files/ui/games/zvezda_prazna.svg'),
(10748, 323, 'UI_StarFull', 'files/ui/games/zvezda_polna.svg'),
(10747, 323, 'UI_LevelUpArrow', 'files/ui/games/up.svg'),
(10746, 323, 'UI_LevelUpArrowInactive', 'files/ui/games/up_unactive.svg'),
(10745, 323, 'UI_EnlargeButtonOn', 'files/ui/games/max01.svg'),
(10744, 323, 'UI_EnlargeButtonOff', 'files/ui/games/min01.svg'),
(10743, 323, 'UI_ExampleImage', 'files/ui/games/nazaj.svg'),
(10742, 323, 'UI_CharacterUnhappy', 'files/ui/games/zvezda_zalostna_senca.svg'),
(10741, 323, 'gameType', '5'),
(10740, 323, 'UI_LINE', 'files/ui/games/line.svg'),
(10739, 323, 'UI_STAR_ROTATED', 'files/ui/games/zvezda_rotacija.svg'),
(10706, 320, 'UI_ArrowBack', 'files/ui/games/nazaj_igra.svg'),
(10705, 320, 'UI_SpeechBubble', 'files/ui/games/oblacek.svg'),
(10704, 320, 'UI_Character', 'files/ui/games/star_shadow_game.svg'),
(10703, 320, 'UI_ArrowContinue', 'files/ui/games/naprej_igra.svg'),
(10702, 320, 'UI_ButtonExit', 'files/ui/games/x_izhod.svg'),
(10701, 320, 'UI_Star', 'files/ui/games/zvezda_prazna.svg'),
(10700, 320, 'UI_StarFull', 'files/ui/games/zvezda_polna.svg'),
(10699, 320, 'UI_LevelUpArrow', 'files/ui/games/up.svg'),
(10698, 320, 'UI_LevelUpArrowInactive', 'files/ui/games/up_unactive.svg'),
(10697, 320, 'UI_EnlargeButtonOn', 'files/ui/games/max01.svg'),
(10696, 320, 'UI_EnlargeButtonOff', 'files/ui/games/min01.svg'),
(10695, 320, 'UI_ExampleImage', 'files/ui/games/nazaj.svg'),
(10694, 320, 'UI_CharacterUnhappy', 'files/ui/games/zvezda_zalostna_senca.svg'),
(10693, 320, 'gameType', '1'),
(10692, 320, 'UI_LINE', 'files/ui/games/line.svg'),
(10691, 320, 'UI_STAR_ROTATED', 'files/ui/games/zvezda_rotacija.svg'),
(10674, 318, 'UI_ArrowBack', 'files/ui/games/nazaj_igra.svg'),
(10673, 318, 'UI_SpeechBubble', 'files/ui/games/oblacek.svg'),
(10672, 318, 'UI_Character', 'files/ui/games/star_shadow_game.svg'),
(10671, 318, 'UI_ArrowContinue', 'files/ui/games/naprej_igra.svg'),
(10670, 318, 'UI_ButtonExit', 'files/ui/games/x_izhod.svg'),
(10669, 318, 'UI_Star', 'files/ui/games/zvezda_prazna.svg'),
(10668, 318, 'UI_StarFull', 'files/ui/games/zvezda_polna.svg'),
(10667, 318, 'UI_LevelUpArrow', 'files/ui/games/up.svg'),
(10666, 318, 'UI_LevelUpArrowInactive', 'files/ui/games/up_unactive.svg'),
(10665, 318, 'UI_EnlargeButtonOn', 'files/ui/games/max01.svg'),
(10664, 318, 'UI_EnlargeButtonOff', 'files/ui/games/min01.svg'),
(10663, 318, 'UI_ExampleImage', 'files/ui/games/nazaj.svg'),
(10662, 318, 'UI_CharacterUnhappy', 'files/ui/games/zvezda_zalostna_senca.svg'),
(10661, 318, 'gameType', '2'),
(10660, 318, 'UI_LINE', 'files/ui/games/line.svg'),
(10659, 318, 'UI_STAR_ROTATED', 'files/ui/games/zvezda_rotacija.svg'),
(10626, 315, 'UI_ArrowBack', 'files/ui/games/nazaj_igra.svg'),
(10625, 315, 'UI_SpeechBubble', 'files/ui/games/oblacek.svg'),
(10624, 315, 'UI_Character', 'files/ui/games/star_shadow_game.svg'),
(10623, 315, 'UI_ArrowContinue', 'files/ui/games/naprej_igra.svg'),
(10622, 315, 'UI_ButtonExit', 'files/ui/games/x_izhod.svg'),
(10621, 315, 'UI_Star', 'files/ui/games/zvezda_prazna.svg'),
(10620, 315, 'UI_StarFull', 'files/ui/games/zvezda_polna.svg'),
(10619, 315, 'UI_LevelUpArrow', 'files/ui/games/up.svg'),
(10618, 315, 'UI_LevelUpArrowInactive', 'files/ui/games/up_unactive.svg'),
(10617, 315, 'UI_EnlargeButtonOn', 'files/ui/games/max01.svg'),
(10616, 315, 'UI_EnlargeButtonOff', 'files/ui/games/min01.svg'),
(10615, 315, 'UI_ExampleImage', 'files/ui/games/nazaj.svg'),
(10614, 315, 'UI_CharacterUnhappy', 'files/ui/games/zvezda_zalostna_senca.svg'),
(10613, 315, 'gameType', '3'),
(10612, 315, 'UI_LINE', 'files/ui/games/line.svg'),
(10611, 315, 'UI_STAR_ROTATED', 'files/ui/games/zvezda_rotacija.svg'),
(10578, 312, 'UI_ArrowBack', 'files/ui/games/nazaj_igra.svg'),
(10577, 312, 'UI_SpeechBubble', 'files/ui/games/oblacek.svg'),
(10576, 312, 'UI_Character', 'files/ui/games/star_shadow_game.svg'),
(10575, 312, 'UI_ArrowContinue', 'files/ui/games/naprej_igra.svg'),
(10574, 312, 'UI_ButtonExit', 'files/ui/games/x_izhod.svg'),
(10573, 312, 'UI_Star', 'files/ui/games/zvezda_prazna.svg'),
(10572, 312, 'UI_StarFull', 'files/ui/games/zvezda_polna.svg'),
(10571, 312, 'UI_LevelUpArrow', 'files/ui/games/up.svg'),
(10570, 312, 'UI_LevelUpArrowInactive', 'files/ui/games/up_unactive.svg'),
(10569, 312, 'UI_EnlargeButtonOn', 'files/ui/games/max01.svg'),
(10568, 312, 'UI_EnlargeButtonOff', 'files/ui/games/min01.svg'),
(10567, 312, 'UI_ExampleImage', 'files/ui/games/nazaj.svg'),
(10566, 312, 'UI_CharacterUnhappy', 'files/ui/games/zvezda_zalostna_senca.svg'),
(10565, 312, 'UI_LINE', 'files/ui/games/line.svg'),
(10564, 312, 'gameType', '4'),
(10563, 312, 'UI_STAR_ROTATED', 'files/ui/games/zvezda_rotacija.svg');

-- --------------------------------------------------------

--
-- Table structure for table `zs_game`
--

CREATE TABLE `zs_game` (
  `game_id` int(11) NOT NULL,
  `game_upload_user_id` int(11) DEFAULT NULL,
  `game_upload_user_ip` varchar(45) DEFAULT NULL,
  `game_upload_datetime` datetime DEFAULT NULL,
  `game_name` varchar(255) DEFAULT NULL,
  `game_file_location` varchar(255) DEFAULT NULL,
  `game_description` text,
  `game_signature_version` varchar(45) DEFAULT NULL,
  `game_author` text,
  `game_date_of_build` date DEFAULT NULL,
  `game_version` varchar(45) DEFAULT NULL,
  `game_license` text,
  `game_teacher_instructions` text,
  `game_comment` text,
  `game_styles_available` text,
  `game_languages` text,
  `game_dictionary` text,
  `game_variables` text,
  `game_difficulty` text,
  `game_time_limit` text,
  `game_levels` text,
  `game_retries_available` text,
  `game_deleted` int(11) DEFAULT NULL COMMENT '0 if not deleted, 1 if deleted'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_game`
--

INSERT INTO `zs_game` (`game_id`, `game_upload_user_id`, `game_upload_user_ip`, `game_upload_datetime`, `game_name`, `game_file_location`, `game_description`, `game_signature_version`, `game_author`, `game_date_of_build`, `game_version`, `game_license`, `game_teacher_instructions`, `game_comment`, `game_styles_available`, `game_languages`, `game_dictionary`, `game_variables`, `game_difficulty`, `game_time_limit`, `game_levels`, `game_retries_available`, `game_deleted`) VALUES
(312, 1, '123.123.123.123', '2015-09-27 12:25:30', 'Zaporedja', 'games/game.html', 'Igra s karticami - Pojavi se nabor slik, ki si jih uporabnik mora zapomniti v pravem zaporedju. Nato slike zginejo, uporabnik pa jih mora poiskati v pravem zaporedju. Učitelj lahko določa nabor slik in težavnost.', '1.0', 'Matevž Baloh', '2015-07-22', '1.0', 'AGPL', 'V tej igri se uporabniku prikaže nabor slik, katerega si mora zaponiti. Nato slike zginejo in se prikaže drug, večji nabor. Izmed novih slik mora uporabnik v pravem zaporedju izbrati prej videne. Ko pravilno odgovri na nekaj primerov, lahko napreduje v naslednji nivo, v katerem se povečata števili slik v obeh naborih. Učitelj lahko določi nabor slik, koliko slik se pokaže v obeh naborih v prvem nivoju, koliko pravilnih odgovorov je potrebnih za napredovat v višji nivo in koliko napredka uporabnik izgubi, če je njegov odgovor napačen.', 'COMMENT', 'default|inverted|contrast', 'SL|EN', '{"EN":{"language":"English","dict":{"L_INSTRUCTIONS_VIEW":"Look at the images, when ready, click continue.","L_INSTRUCTIONS_SELECT_SEEN":"Find the previously seen images.","L_INSTRUCTIONS_SELECT_UNSEEN":"Find the new images.","L_INCORRECT_LINE_ONE":"Don''t give up,","L_INCORRECT_LINE_TWO":"try again.","L_CORRECT_LINE_ONE":"Congratulations","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Continue","L_SELECT_STATUS_DISPLAY_1":"Selected:","L_SELECT_STATUS_DISPLAY_2":"out of","L_CONTINUE":"Continue","L_BEGIN":"BEGIN","L_NEXT_LEVEL":"Next level"}},"SL":{"language":"Slovensko","dict":{"L_INSTRUCTIONS_VIEW":"Dobro si oglej slike in nadaljuj.","L_INSTRUCTIONS_SELECT_SEEN":"Izberi prej videne slike.","L_INSTRUCTIONS_SELECT_UNSEEN":"Izberi nove slike.","L_INCORRECT_LINE_ONE":"Ne obupaj,","L_INCORRECT_LINE_TWO":"poskusi ponovno.","L_CORRECT_LINE_ONE":"Čestitam","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Naprej","L_SELECT_STATUS_DISPLAY_1":"Izbrani:","L_SELECT_STATUS_DISPLAY_2":"od","L_CONTINUE":"Nadaljuj","L_BEGIN":"ZAČNI","L_NEXT_LEVEL":"Naslednja stopnja"}}}', '{"instructions":{"type":"string","default_value":"Pojavile se bodo slike, ki si jih zapomni. Med novimi slikami izberi prej videne v pravem zaporedju.","description":"Uvodna navodila.","edit_instructions":"Navodila prilagodite tako, da opi\\u0161ejo igro, ki jo priredite, sicer jih pustite taka kot so","constraints":"1|300"},"selectN":{"type":"int","default_value":"2","description":"\\u0160tevilo slik, ki si jih mora uporabnik zapomnit (na prvi stopnji).","edit_instructions":"Izberite primerno \\u0161tevilo, da ne bo u\\u010dencem prelahko na za\\u010detku. Npr. 2\\/3 za za\\u010detnike, 4\\/5 za napredne, 6\\/7 je maksimum, ki ga predlagamo.","constraints":"1|21|1"},"groupN":{"type":"int","default_value":"4","description":"\\u0160tevilo slik, med katerimi uporabnik poi\\u0161\\u010de \\u0161e ne videne.","edit_instructions":"Izberite primerno \\u0161tevilo, da ne bo u\\u010dencem prelahko na za\\u010detku. Npr. 3-5 za za\\u010detnike, 6-10 za napredne, 11-15 je maksimum, ki ga predlagamo.","constraints":"1|21|1"},"starLossOnFail":{"type":"int","default_value":"1","description":"\\u0160tevilo zvezdic, ki jih uporabnik izgubi ob nepravilnem odgovoru.","edit_instructions":"Za redno igro predlagamo 0 ali 1, lahko se pa tudi pove\\u010da na npr. 5 in igro igra tako, da mora u\\u010denec igro pravilno re\\u0161iti npr. 5 krat zapored, \\u010de kadarkoli zgre\\u0161i, izgubi vse zvezdice in mora od za\\u010detka (Za tak na\\u010din igre nastavite to in progressToContinue na isto \\u0161tevilo).","constraints":"1|100|1"},"progressToContinue":{"type":"int","default_value":"3","description":"\\u0160tevilo zvezdic, potrebnih za napredovanje v naslednjo stopnjo.","edit_instructions":"Za hitro napredovanje predlagamo 3, za normalno igro 5-7, za po\\u010dasno igro 10, lahko pa poljubno mnogo. Igralec lahko ostane na istem nivoju tudi ko \\u017ee dose\\u017ee potrebno \\u0161tevilo zvezdic za napredovanje.","constraints":"1|100|1"},"files":{"type":"filelist","default_value":["files\\/slike\\/orodje\\/barvno\\/copic.svg","files\\/slike\\/orodje\\/barvno\\/copic01.svg","files\\/slike\\/orodje\\/barvno\\/izvijac.svg","files\\/slike\\/orodje\\/barvno\\/izvijac01.svg","files\\/slike\\/orodje\\/barvno\\/izvijac02.svg","files\\/slike\\/orodje\\/barvno\\/kladivo.svg","files\\/slike\\/orodje\\/barvno\\/kljuc.svg","files\\/slike\\/orodje\\/barvno\\/kljuc01.svg","files\\/slike\\/orodje\\/barvno\\/kljuc02.svg","files\\/slike\\/orodje\\/barvno\\/lestev.svg","files\\/slike\\/orodje\\/barvno\\/matica.svg","files\\/slike\\/orodje\\/barvno\\/ravnilo.svg","files\\/slike\\/orodje\\/barvno\\/ravnilo01.svg","files\\/slike\\/orodje\\/barvno\\/valjcek.svg","files\\/slike\\/orodje\\/barvno\\/zaga.svg","files\\/slike\\/orodje\\/barvno\\/zebelj.svg","files\\/slike\\/orodje\\/barvno\\/zebelj01.svg","files\\/slike\\/kuhinja\\/barvno\\/detergent.svg","files\\/slike\\/kuhinja\\/barvno\\/prasek.svg","files\\/slike\\/kuhinja\\/barvno\\/skleda.svg","files\\/slike\\/kuhinja\\/barvno\\/spuzva.svg","files\\/slike\\/kuhinja\\/barvno\\/vedro.svg","files\\/slike\\/kopalnica\\/barvno\\/skarje.svg"],"description":"Nabor slik.","edit_instructions":"Za la\\u017eje igre izberite zelo razli\\u010dne slike, za te\\u017eje igre izberite podobne slike. Tu lahko igro priredite, da ustreza potrebam va\\u0161ega predmeta, zato predlagamo, da izbirate slike, ki so povezane s predmetom, ki ga u\\u010dite.","constraints":"1|100"}}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', 0),
(315, 1, '123.123.123.123', '2015-09-27 12:54:09', 'Kaj manjka?', 'games/game.html', 'Igra s karticami - Pojavi se nabor slik, ki si jih uporabnik mora zapomniti. Nato nekaj slik zgine. Uporabnik mora poiskati tiste, ki so zginile. Učitelj lahko določi nabor slik in težavnost. ', '1.0', 'Matevž Baloh', '2015-07-22', '1.0', 'AGPL', 'V tej igri se uporabniku prikaže nabor slik, katerega si mora zaponiti. Nato nekatere od slik zginejo, pod preostalimi pa se pojavi nov nabor slik, izmed katerih mora uporabnik izbrati izginule. Ko pravilno odgovri na nekaj primerov, lahko napreduje v naslednji nivo, v katerem se povečata števili slik v obeh naborih, število izginulih slik pa ostane enako. Učitelj lahko določi nabor slik, koliko slik se pokaže v obeh naborih v prvem nivoju, koliko slik izgine iz drugega nabora, koliko pravilnih odgovorov je potrebnih za napredovat v višji nivo in koliko napredka uporabnik izgubi, če je njegov odgovor napačen.', 'COMMENT', 'default|inverted|contrast', 'SL|EN', '{"EN":{"language":"English","dict":{"L_INSTRUCTIONS_VIEW":"Look at the images, when ready, click continue.","L_INSTRUCTIONS_SELECT_SEEN":"Find the previously seen images.","L_INSTRUCTIONS_SELECT_UNSEEN":"Find the new images.","L_INCORRECT_LINE_ONE":"Don''t give up,","L_INCORRECT_LINE_TWO":"try again.","L_CORRECT_LINE_ONE":"Congratulations","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Continue","L_SELECT_STATUS_DISPLAY_1":"Selected:","L_SELECT_STATUS_DISPLAY_2":"out of","L_CONTINUE":"Continue","L_BEGIN":"BEGIN","L_NEXT_LEVEL":"Next level"}},"SL":{"language":"Slovensko","dict":{"L_INSTRUCTIONS_VIEW":"Dobro si oglej slike in nadaljuj.","L_INSTRUCTIONS_SELECT_SEEN":"Izberi prej videne slike.","L_INSTRUCTIONS_SELECT_UNSEEN":"Izberi nove slike.","L_INCORRECT_LINE_ONE":"Ne obupaj,","L_INCORRECT_LINE_TWO":"poskusi ponovno.","L_CORRECT_LINE_ONE":"Čestitam","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Naprej","L_SELECT_STATUS_DISPLAY_1":"Izbrani:","L_SELECT_STATUS_DISPLAY_2":"od","L_CONTINUE":"Nadaljuj","L_BEGIN":"ZAČNI","L_NEXT_LEVEL":"Naslednja stopnja"}}}', '{"instructions":{"type":"string","default_value":"Pojavile se bodo slike, ki si jih zapomni. Med novimi slikami poi\\u0161\\u010di manjkajo\\u010de.","description":"Uvodna navodila","edit_instructions":"Navodila prilagodite tako, da opi\\u0161ejo igro, ki jo priredite, sicer jih pustite taka kot so","constraints":"1|300"},"selectN":{"type":"int","default_value":"2","description":"\\u0160tevilo slik, ki si jih mora uporabnik zapomnit (na prvi stopnji).","edit_instructions":"Izberite primerno \\u0161tevilo, da ne bo u\\u010dencem prelahko na za\\u010detku. Npr. 2\\/3 za za\\u010detnike, 4\\/5 za napredne, 6\\/7 je maksimum, ki ga predlagamo.","constraints":"1|10|1"},"reduceN":{"type":"int","default_value":"1","description":"\\u0160tevilo slik, ki se bo skrilo.","edit_instructions":"\\u0160tevilo naj bo manj\\u0161e ali enako \\u0161tevilu slik, ki si jih mora uporabnik zapomnit.","constraints":"1|10|1"},"groupN":{"type":"int","default_value":"4","description":"\\u0160tevilo slik, med katerimi uporabnik izbere izginulo\\/e.","edit_instructions":"Izberite primerno \\u0161tevilo, da ne bo u\\u010dencem prelahko na za\\u010detku. Npr. 3-5 za za\\u010detnike, 6-10 za napredne, 11-15 je maksimum, ki ga predlagamo.","constraints":"1|21|1"},"starLossOnFail":{"type":"int","default_value":"1","description":"\\u0160tevilo zvezdic, ki jih uporabnik izgubi ob nepravilnem odgovoru.","edit_instructions":"Za redno igro predlagamo 0 ali 1, lahko se pa tudi pove\\u010da na npr. 5 in igro igra tako, da mora u\\u010denec igro pravilno re\\u0161iti npr. 5 krat zapored, \\u010de kadarkoli zgre\\u0161i, izgubi vse zvezdice in mora od za\\u010detka (Za tak na\\u010din igre nastavite to in progressToContinue na isto \\u0161tevilo).","constraints":"1|100|1"},"progressToContinue":{"type":"int","default_value":"3","description":"\\u0160tevilo zvezdic, potrebnih za napredovanje v naslednjo stopnjo.","edit_instructions":"Za hitro napredovanje predlagamo 3, za normalno igro 5-7, za po\\u010dasno igro 10, lahko pa poljubno mnogo. Igralec lahko ostane na istem nivoju tudi ko \\u017ee dose\\u017ee potrebno \\u0161tevilo zvezdic za napredovanje.","constraints":"1|100|1"},"files":{"type":"filelist","default_value":["files\\/slike\\/garderoba\\/barvno\\/bluza (1).svg","files\\/slike\\/garderoba\\/barvno\\/bluza (2).svg","files\\/slike\\/garderoba\\/barvno\\/bluza (3).svg","files\\/slike\\/garderoba\\/barvno\\/bluza (4).svg","files\\/slike\\/garderoba\\/barvno\\/bluza (5).svg","files\\/slike\\/garderoba\\/barvno\\/bluza (6).svg","files\\/slike\\/garderoba\\/barvno\\/deznik (1).svg","files\\/slike\\/garderoba\\/barvno\\/deznik (2).svg","files\\/slike\\/garderoba\\/barvno\\/deznik (3).svg","files\\/slike\\/garderoba\\/barvno\\/deznik (4).svg","files\\/slike\\/garderoba\\/barvno\\/deznik (5).svg","files\\/slike\\/garderoba\\/barvno\\/deznik (6).svg","files\\/slike\\/garderoba\\/barvno\\/dolge_hlace.svg","files\\/slike\\/garderoba\\/barvno\\/gleznar.svg","files\\/slike\\/garderoba\\/barvno\\/gumb (1).svg","files\\/slike\\/garderoba\\/barvno\\/gumb (2).svg","files\\/slike\\/garderoba\\/barvno\\/gumb (3).svg","files\\/slike\\/garderoba\\/barvno\\/gumb (4).svg","files\\/slike\\/garderoba\\/barvno\\/gumb (5).svg","files\\/slike\\/garderoba\\/barvno\\/gumb (6).svg","files\\/slike\\/garderoba\\/barvno\\/jopica.svg","files\\/slike\\/garderoba\\/barvno\\/kapa (1).svg","files\\/slike\\/garderoba\\/barvno\\/kapa (2).svg","files\\/slike\\/garderoba\\/barvno\\/kapa (3).svg","files\\/slike\\/garderoba\\/barvno\\/kapa (4).svg","files\\/slike\\/garderoba\\/barvno\\/kapa (5).svg","files\\/slike\\/garderoba\\/barvno\\/kapa (6).svg","files\\/slike\\/garderoba\\/barvno\\/kopalke.svg","files\\/slike\\/garderoba\\/barvno\\/kratke_hlace.svg","files\\/slike\\/garderoba\\/barvno\\/krilo.svg","files\\/slike\\/garderoba\\/barvno\\/majica.svg","files\\/slike\\/garderoba\\/barvno\\/majica_rokav.svg","files\\/slike\\/garderoba\\/barvno\\/natikaci.svg","files\\/slike\\/garderoba\\/barvno\\/nogavice.svg","files\\/slike\\/garderoba\\/barvno\\/nogavice01.svg","files\\/slike\\/garderoba\\/barvno\\/obesalnik.svg","files\\/slike\\/garderoba\\/barvno\\/obleka (1).svg","files\\/slike\\/garderoba\\/barvno\\/obleka (2).svg","files\\/slike\\/garderoba\\/barvno\\/obleka (3).svg","files\\/slike\\/garderoba\\/barvno\\/obleka (4).svg","files\\/slike\\/garderoba\\/barvno\\/obleka (5).svg","files\\/slike\\/garderoba\\/barvno\\/pas.svg","files\\/slike\\/garderoba\\/barvno\\/rokavice.svg","files\\/slike\\/garderoba\\/barvno\\/sal.svg","files\\/slike\\/garderoba\\/barvno\\/salonar.svg","files\\/slike\\/garderoba\\/barvno\\/spodnja_majica.svg","files\\/slike\\/garderoba\\/barvno\\/spodnjice.svg"],"description":"Nabor slik.","edit_instructions":"Za la\\u017eje igre izberite zelo razli\\u010dne slike, za te\\u017eje igre izberite podobne slike. Tu lahko igro priredite, da ustreza potrebam va\\u0161ega predmeta, zato predlagamo, da izbirate slike, ki so povezane s predmetom, ki ga u\\u010dite.","constraints":"1|100"}}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', 0),
(318, 1, '123.123.123.123', '2015-09-27 13:51:57', 'Kaj je novega?', 'games/game.html', 'Igra s karticami - Najprej se pojavi nabor slik, ki si jih uporabnik mora zapomniti. Nato zginejo in se pojavi večji nabor slik. Uporabmik mora izmed nabora poiskati vse nove slike. Učitelj lahko določa nabor slik in težavnost.', '1.0', 'Matevž Baloh', '2015-07-22', '1.0', 'AGPL', 'V tej igri se uporabniku prikaže nabor slik, katerega si mora zaponiti. Nato slike zginejo in se prikaže drug, večji nabor. Izmed novih slik mora uporabnik izbrati vse nove - tiste, ki se niso pojavile v prvem naboru. Ko pravilno odgovri na nekaj primerov, lahko napreduje v naslednji nivo, v katerem se povečata števili slik v obeh naborih. Učitelj lahko določi nabor slik, koliko slik se pokaže v obeh naborih v prvem nivoju, koliko pravilnih odgovorov je potrebnih za napredovat v višji nivo in koliko napredka uporabnik izgubi, če je njegov odgovor napačen.', 'COMMENT', 'default|inverted|contrast', 'SL|EN', '{"EN":{"language":"English","dict":{"L_INSTRUCTIONS_VIEW":"Look at the images, when ready, click continue.","L_INSTRUCTIONS_SELECT_SEEN":"Find the previously seen images.","L_INSTRUCTIONS_SELECT_UNSEEN":"Find the new images.","L_INCORRECT_LINE_ONE":"Don''t give up,","L_INCORRECT_LINE_TWO":"try again.","L_CORRECT_LINE_ONE":"Congratulations","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Continue","L_SELECT_STATUS_DISPLAY_1":"Selected:","L_SELECT_STATUS_DISPLAY_2":"out of","L_CONTINUE":"Continue","L_BEGIN":"BEGIN","L_NEXT_LEVEL":"Next level"}},"SL":{"language":"Slovensko","dict":{"L_INSTRUCTIONS_VIEW":"Dobro si oglej slike in nadaljuj.","L_INSTRUCTIONS_SELECT_SEEN":"Izberi prej videne slike.","L_INSTRUCTIONS_SELECT_UNSEEN":"Izberi nove slike.","L_INCORRECT_LINE_ONE":"Ne obupaj,","L_INCORRECT_LINE_TWO":"poskusi ponovno.","L_CORRECT_LINE_ONE":"Čestitam","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Naprej","L_SELECT_STATUS_DISPLAY_1":"Izbrani:","L_SELECT_STATUS_DISPLAY_2":"od","L_CONTINUE":"Nadaljuj","L_BEGIN":"ZAČNI","L_NEXT_LEVEL":"Naslednja stopnja"}}}', '{"instructions":{"type":"string","default_value":"Pojavile se bodo slike, ki si jih dobro oglej. Med novimi slikami izberi samo nove.","description":"Uvodna navodila.","edit_instructions":"Navodila prilagodite tako, da opi\\u0161ejo igro, ki jo priredite, sicer jih pustite taka kot so","constraints":"1|300"},"selectN":{"type":"int","default_value":"2","description":"\\u0160tevilo slik, ki si jih mora uporabnik zapomnit (na prvi stopnji).","edit_instructions":"Izberite primerno \\u0161tevilo, da ne bo u\\u010dencem prelahko na za\\u010detku. Npr. 2\\/3 za za\\u010detnike, 4\\/5 za napredne, 6\\/7 je maksimum, ki ga predlagamo.","constraints":"1|21|1"},"groupN":{"type":"int","default_value":"4","description":"\\u0160tevilo slik, med katerimi uporabnik poi\\u0161\\u010de \\u0161e ne videne.","edit_instructions":"Izberite primerno \\u0161tevilo, da ne bo u\\u010dencem prelahko na za\\u010detku. Npr. 3-5 za za\\u010detnike, 6-10 za napredne, 11-15 je maksimum, ki ga predlagamo.","constraints":"1|21|1"},"starLossOnFail":{"type":"int","default_value":"1","description":"\\u0160tevilo zvezdic, ki jih uporabnik izgubi ob nepravilnem odgovoru.","edit_instructions":"Za redno igro predlagamo 0 ali 1, lahko se pa tudi pove\\u010da na npr. 5 in igro igra tako, da mora u\\u010denec igro pravilno re\\u0161iti npr. 5 krat zapored, \\u010de kadarkoli zgre\\u0161i, izgubi vse zvezdice in mora od za\\u010detka (Za tak na\\u010din igre nastavite to in progressToContinue na isto \\u0161tevilo).","constraints":"1|100|1"},"progressToContinue":{"type":"int","default_value":"3","description":"\\u0160tevilo zvezdic, potrebnih za napredovanje v naslednjo stopnjo.","edit_instructions":"Za hitro napredovanje predlagamo 3, za normalno igro 5-7, za po\\u010dasno igro 10, lahko pa poljubno mnogo. Igralec lahko ostane na istem nivoju tudi ko \\u017ee dose\\u017ee potrebno \\u0161tevilo zvezdic za napredovanje.","constraints":"1|100|1"},"files":{"type":"filelist","default_value":["files\\/slike\\/kuhinja\\/barvno\\/cajnik.svg","files\\/slike\\/kuhinja\\/barvno\\/cajnik01.svg","files\\/slike\\/kuhinja\\/barvno\\/cajnik02.svg","files\\/slike\\/kuhinja\\/barvno\\/cedilo.svg","files\\/slike\\/kuhinja\\/barvno\\/deska.svg","files\\/slike\\/kuhinja\\/barvno\\/deska01.svg","files\\/slike\\/kuhinja\\/barvno\\/kapa.svg","files\\/slike\\/kuhinja\\/barvno\\/kos.svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (1).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (2).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (3).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (4).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (5).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (6).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec01.svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec02.svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec03.svg","files\\/slike\\/kuhinja\\/barvno\\/kroznik.svg","files\\/slike\\/kuhinja\\/barvno\\/krozniki.svg","files\\/slike\\/kuhinja\\/barvno\\/kuhalnica.svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (1).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (2).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (3).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (4).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (5).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (6).svg","files\\/slike\\/kuhinja\\/barvno\\/noz.svg","files\\/slike\\/kuhinja\\/barvno\\/noz01.svg","files\\/slike\\/kuhinja\\/barvno\\/noz02.svg","files\\/slike\\/kuhinja\\/barvno\\/obracalka.svg","files\\/slike\\/kuhinja\\/barvno\\/obracalka01.svg","files\\/slike\\/kuhinja\\/barvno\\/pladen.svg","files\\/slike\\/kuhinja\\/barvno\\/ponev.svg","files\\/slike\\/kuhinja\\/barvno\\/ponev01.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda01.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda02.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda03.svg","files\\/slike\\/kuhinja\\/barvno\\/valjar.svg","files\\/slike\\/kuhinja\\/barvno\\/vrc.svg","files\\/slike\\/kuhinja\\/barvno\\/vrc01.svg"],"description":"Nabor slik.","edit_instructions":"Za la\\u017eje igre izberite zelo razli\\u010dne slike, za te\\u017eje igre izberite podobne slike. Tu lahko igro priredite, da ustreza potrebam va\\u0161ega predmeta, zato predlagamo, da izbirate slike, ki so povezane s predmetom, ki ga u\\u010dite.","constraints":"1|100"}}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', 0),
(335, 1, '123.123.123.123', '2015-09-28 18:10:00', 'Skriti predmeti', 'games/game.html', 'Uporabnik mora na sliki poiskati skrite predmete.', '1.0', 'Matevž Baloh', '2015-07-22', '1.0', 'AGPL', 'V tej igri mora uporabnik na sliki najti skrite predmete, ki so prikazani na desni. To igro lahko učitelj precej prilagaja, a mora za to posebej pripraviti SVG datoteko s točkami, kjer se lahko elementi pojavijo. Za to predlagamo, da učitelj spreminja zgolj število zvezdic, ki ga mora učenec doseči za merjenje napredka.', 'COMMENT', 'default|inverted|contrast', 'SL|EN', '{"SL":{"language":"Slovensko","dict":{"L_INSTRUCTIONS_VIEW":"Dobro si oglej sli\\u010dice. Ko bo\\u0161 pripravljen, klikni naprej.","L_INSTRUCTIONS_SELECT_SEEN":"Med danimi sli\\u010dicami poi\\u0161\\u010di prej videne.","L_INSTRUCTIONS_SELECT_UNSEEN":"Med danimi sli\\u010dicami poi\\u0161\\u010di nove.","L_INCORRECT_LINE_ONE":"Ne obupaj,","L_INCORRECT_LINE_TWO":"Poskusi ponovno.","L_CORRECT_LINE_ONE":"\\u010cestitam","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Naprej","L_SELECT_STATUS_DISPLAY_1":"Izbrani:","L_SELECT_STATUS_DISPLAY_2":"od","L_CONTINUE":"Nadaljuj","L_BEGIN":"ZA\\u010cNI","L_NEXT_LEVEL":"Naslednji nivo"}},"EN":{"language":"English","dict":{"L_INSTRUCTIONS_VIEW":"Look at the images, when ready, click continue.","L_INSTRUCTIONS_SELECT_SEEN":"Find the previously seen images.","L_INSTRUCTIONS_SELECT_UNSEEN":"Find the new images.","L_INCORRECT_LINE_ONE":"Don","L_INCORRECT_LINE_TWO":"Try again.","L_CORRECT_LINE_ONE":"Congratulations","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Continue","L_SELECT_STATUS_DISPLAY_1":"Selected:","L_SELECT_STATUS_DISPLAY_2":"out of","L_CONTINUE":"Continue","L_BEGIN":"BEGIN","L_NEXT_LEVEL":"Next level"}}}', '{"instructions":{"type":"string","default_value":" Na sliki poi\\u0161\\u010di skrite predmete. Predmete, ki jih mora\\u0161 najti, so v kvadratkih na desni.","description":"Uvodna navodila","edit_instructions":"Navodila prilagodite tako, da opi\\u0161ejo igro, ki jo priredite, sicer jih pustite taka kot so","constraints":"1|300"},"layer1":{"type":"filelist","default_value":["files\\/skriti predmeti\\/scena\\/kuhinja\\/barvno\\/kuhinja_zid.svg"],"description":"Ozadje 1","edit_instructions":"Ne priporo\\u010damo spreminjanja, sicer so pa nivoji od 1 do 6 prikazani en nad drugim. Slika na istem mestu v teh nivojih sestavljajo isto sceno. Npr. tretja slika v vsakem od nivojev se zdru\\u017ei v eno sceno.","constraints":""},"layer2":{"type":"filelist","default_value":["files\\/skriti predmeti\\/scena\\/kuhinja\\/barvno\\/kuhinja_ozadje.svg"],"description":"Ozadje 2","edit_instructions":"Ne priporo\\u010damo spreminjanja, sicer so pa nivoji od 1 do 6 prikazani en nad drugim. Slika na istem mestu v teh nivojih sestavljajo isto sceno. Npr. tretja slika v vsakem od nivojev se zdru\\u017ei v eno sceno.","constraints":""},"layer3":{"type":"filelist","default_value":[""],"description":"Ozadje 3","edit_instructions":"Ne priporo\\u010damo spreminjanja, sicer so pa nivoji od 1 do 6 prikazani en nad drugim. Slika na istem mestu v teh nivojih sestavljajo isto sceno. Npr. tretja slika v vsakem od nivojev se zdru\\u017ei v eno sceno.","constraints":""},"layer4":{"type":"filelist","default_value":[""],"description":"Ozadje 4","edit_instructions":"Ne priporo\\u010damo spreminjanja, sicer so pa nivoji od 1 do 6 prikazani en nad drugim. Slika na istem mestu v teh nivojih sestavljajo isto sceno. Npr. tretja slika v vsakem od nivojev se zdru\\u017ei v eno sceno.","constraints":""},"pivot":{"type":"filelist","default_value":["files\\/upload\\/30\\/14\\/pivot (1).svg"],"description":"To\\u010dke, kjer se pojavijo elementi (svg datoteka)","edit_instructions":"Ne priporo\\u010damo spreminjanja, sicer so pa nivoji od 1 do 6 prikazani en nad drugim. Slika na istem mestu v teh nivojih sestavljajo isto sceno. Npr. tretja slika v vsakem od nivojev se zdru\\u017ei v eno sceno.","constraints":""},"layer6":{"type":"filelist","default_value":["files\\/skriti predmeti\\/scena\\/kuhinja\\/barvno\\/kuhinja_ospredje.svg"],"description":"Ospredje - elementi nad predmeti","edit_instructions":"Ne priporo\\u010damo spreminjanja, sicer so pa nivoji od 1 do 6 prikazani en nad drugim. Slika na istem mestu v teh nivojih sestavljajo isto sceno. Npr. tretja slika v vsakem od nivojev se zdru\\u017ei v eno sceno.","constraints":""},"elements1":{"type":"filelist","default_value":["files\\/slike\\/hrana\\/barvno\\/cesnja (1).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (2).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (3).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (4).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (5).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (6).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (1).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (2).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (3).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (4).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (5).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (6).svg","files\\/slike\\/hrana\\/barvno\\/hruska.svg","files\\/slike\\/hrana\\/barvno\\/jabolko.svg","files\\/slike\\/hrana\\/barvno\\/sliva.svg","files\\/slike\\/hrana\\/barvno\\/marelica.svg","files\\/slike\\/prevoz\\/barvno\\/balon (1).svg","files\\/slike\\/prevoz\\/barvno\\/balon (2).svg","files\\/slike\\/prevoz\\/barvno\\/balon (3).svg","files\\/slike\\/prevoz\\/barvno\\/balon (4).svg","files\\/slike\\/prevoz\\/barvno\\/balon (5).svg","files\\/slike\\/prevoz\\/barvno\\/balon (6).svg","files\\/slike\\/zivali\\/barvno\\/cebela.svg","files\\/slike\\/zivali\\/barvno\\/metulj (1).svg","files\\/slike\\/zivali\\/barvno\\/metulj (2).svg","files\\/slike\\/zivali\\/barvno\\/metulj (3).svg","files\\/slike\\/zivali\\/barvno\\/metulj (4).svg","files\\/slike\\/zivali\\/barvno\\/metulj (5).svg","files\\/slike\\/zivali\\/barvno\\/metulj (6).svg"],"description":"Nabor slik - skupina 1","edit_instructions":"Nabor slik predmetov, ki se lahko pojavijo na eni od lokacij. Lokacije so definirane s pivoti, zato ne priporo\\u010damo spreminjanje tega.","constraints":""},"elements2":{"type":"filelist","default_value":["files\\/slike\\/prevoz\\/barvno\\/balon (1).svg","files\\/slike\\/prevoz\\/barvno\\/balon (2).svg","files\\/slike\\/prevoz\\/barvno\\/balon (3).svg","files\\/slike\\/prevoz\\/barvno\\/balon (4).svg","files\\/slike\\/prevoz\\/barvno\\/balon (5).svg","files\\/slike\\/prevoz\\/barvno\\/balon (6).svg","files\\/slike\\/zivali\\/barvno\\/cebela.svg","files\\/slike\\/zivali\\/barvno\\/metulj (1).svg","files\\/slike\\/zivali\\/barvno\\/metulj (2).svg","files\\/slike\\/zivali\\/barvno\\/metulj (3).svg","files\\/slike\\/zivali\\/barvno\\/metulj (4).svg","files\\/slike\\/zivali\\/barvno\\/metulj (5).svg","files\\/slike\\/zivali\\/barvno\\/metulj (6).svg"],"description":"Nabor slik - skupina 2","edit_instructions":"Nabor slik predmetov, ki se lahko pojavijo na eni od lokacij. Lokacije so definirane s pivoti, zato ne priporo\\u010damo spreminjanje tega.","constraints":""},"elements3":{"type":"filelist","default_value":["files\\/slike\\/rastline\\/barvno\\/bukev.svg","files\\/slike\\/rastline\\/barvno\\/detelja.svg","files\\/slike\\/rastline\\/barvno\\/hrast.svg","files\\/slike\\/rastline\\/barvno\\/iglavec.svg","files\\/slike\\/rastline\\/barvno\\/javor.svg","files\\/slike\\/rastline\\/barvno\\/lipa.svg","files\\/slike\\/rastline\\/barvno\\/listavec.svg","files\\/slike\\/rastline\\/barvno\\/marjetica (1).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (2).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (3).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (4).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (5).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (6).svg","files\\/slike\\/rastline\\/barvno\\/tulipan.svg","files\\/slike\\/rastline\\/barvno\\/zvoncek.svg","files\\/slike\\/sport\\/barvno\\/kosarka.svg","files\\/slike\\/sport\\/barvno\\/odbojka.svg","files\\/slike\\/sport\\/barvno\\/tenis01.svg","files\\/slike\\/zivali\\/barvno\\/cebela.svg","files\\/slike\\/zivali\\/barvno\\/hrosc.svg","files\\/slike\\/zivali\\/barvno\\/jez.svg","files\\/slike\\/zivali\\/barvno\\/pajek.svg","files\\/slike\\/zivali\\/barvno\\/raca.svg","files\\/slike\\/zivali\\/barvno\\/raca01.svg",""],"description":"Nabor slik - skupina 3","edit_instructions":"Nabor slik predmetov, ki se lahko pojavijo na eni od lokacij. Lokacije so definirane s pivoti, zato ne priporo\\u010damo spreminjanje tega.","constraints":""},"elements4":{"type":"filelist","default_value":["files\\/slike\\/rastline\\/barvno\\/bukev.svg","files\\/slike\\/rastline\\/barvno\\/detelja.svg","files\\/slike\\/rastline\\/barvno\\/hrast.svg","files\\/slike\\/rastline\\/barvno\\/iglavec.svg","files\\/slike\\/rastline\\/barvno\\/javor.svg","files\\/slike\\/rastline\\/barvno\\/lipa.svg","files\\/slike\\/rastline\\/barvno\\/listavec.svg","files\\/slike\\/rastline\\/barvno\\/marjetica (1).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (2).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (3).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (4).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (5).svg","files\\/slike\\/rastline\\/barvno\\/marjetica (6).svg","files\\/slike\\/rastline\\/barvno\\/tulipan.svg","files\\/slike\\/rastline\\/barvno\\/zvoncek.svg","files\\/slike\\/sport\\/barvno\\/kosarka.svg","files\\/slike\\/sport\\/barvno\\/odbojka.svg","files\\/slike\\/sport\\/barvno\\/tenis01.svg","files\\/slike\\/zivali\\/barvno\\/cebela.svg","files\\/slike\\/zivali\\/barvno\\/hrosc.svg","files\\/slike\\/zivali\\/barvno\\/jez.svg","files\\/slike\\/zivali\\/barvno\\/pajek.svg","files\\/slike\\/zivali\\/barvno\\/raca.svg","files\\/slike\\/zivali\\/barvno\\/raca01.svg","files\\/slike\\/zivali\\/barvno\\/macka.svg","files\\/slike\\/zivali\\/barvno\\/medved.svg","files\\/slike\\/zivali\\/barvno\\/lev.svg","files\\/slike\\/zivali\\/barvno\\/mis.svg","files\\/slike\\/zivali\\/barvno\\/ovca.svg","files\\/slike\\/zivali\\/barvno\\/pujs.svg","files\\/slike\\/zivali\\/barvno\\/sova.svg","files\\/slike\\/zivali\\/barvno\\/tiger.svg","files\\/slike\\/zivali\\/barvno\\/zaba (1).svg","files\\/slike\\/zivali\\/barvno\\/zaba (2).svg","files\\/slike\\/zivali\\/barvno\\/zaba (3).svg","files\\/slike\\/zivali\\/barvno\\/zaba (4).svg","files\\/slike\\/zivali\\/barvno\\/zaba (5).svg","files\\/slike\\/zivali\\/barvno\\/zaba (6).svg","files\\/slike\\/zivali\\/barvno\\/zajec.svg"],"description":"Nabor slik - skupina 4","edit_instructions":"Nabor slik predmetov, ki se lahko pojavijo na eni od lokacij. Lokacije so definirane s pivoti, zato ne priporo\\u010damo spreminjanje tega.","constraints":""},"elements5":{"type":"filelist","default_value":["files\\/slike\\/zivali\\/barvno\\/riba (1).svg","files\\/slike\\/zivali\\/barvno\\/riba (2).svg","files\\/slike\\/zivali\\/barvno\\/riba (3).svg","files\\/slike\\/zivali\\/barvno\\/riba (4).svg","files\\/slike\\/zivali\\/barvno\\/riba (5).svg","files\\/slike\\/zivali\\/barvno\\/riba (6).svg","files\\/slike\\/prevoz\\/barvno\\/jadrnica (1).svg","files\\/slike\\/prevoz\\/barvno\\/jadrnica (2).svg","files\\/slike\\/prevoz\\/barvno\\/jadrnica (3).svg","files\\/slike\\/prevoz\\/barvno\\/jadrnica (4).svg","files\\/slike\\/prevoz\\/barvno\\/jadrnica (5).svg","files\\/slike\\/prevoz\\/barvno\\/jadrnica (6).svg","files\\/slike\\/prevoz\\/barvno\\/ladja.svg","files\\/slike\\/prevoz\\/barvno\\/podmornica.svg"],"description":"Nabor slik - skupina 5","edit_instructions":"Nabor slik predmetov, ki se lahko pojavijo na eni od lokacij. Lokacije so definirane s pivoti, zato ne priporo\\u010damo spreminjanje tega.","constraints":""},"elements6":{"type":"filelist","default_value":["files\\/slike\\/hrana\\/barvno\\/banana.svg","files\\/slike\\/hrana\\/barvno\\/bonbon.svg","files\\/slike\\/hrana\\/barvno\\/brokoli.svg","files\\/slike\\/hrana\\/barvno\\/cebula.svg","files\\/slike\\/hrana\\/barvno\\/cesen.svg","files\\/slike\\/hrana\\/barvno\\/cesnja (1).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (2).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (3).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (4).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (5).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (6).svg","files\\/slike\\/hrana\\/barvno\\/cili.svg","files\\/slike\\/hrana\\/barvno\\/fizol.svg","files\\/slike\\/hrana\\/barvno\\/fizolcki.svg","files\\/slike\\/hrana\\/barvno\\/goba.svg","files\\/slike\\/hrana\\/barvno\\/grozdje (1).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (2).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (3).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (4).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (5).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (6).svg","files\\/slike\\/hrana\\/barvno\\/hlebec.svg","files\\/slike\\/hrana\\/barvno\\/hruska.svg","files\\/slike\\/hrana\\/barvno\\/jabolko.svg","files\\/slike\\/hrana\\/barvno\\/jagoda.svg","files\\/slike\\/hrana\\/barvno\\/jajce.svg","files\\/slike\\/hrana\\/barvno\\/jajcevec.svg","files\\/slike\\/hrana\\/barvno\\/kivi.svg","files\\/slike\\/hrana\\/barvno\\/korenje (1).svg","files\\/slike\\/hrana\\/barvno\\/korenje (2).svg","files\\/slike\\/hrana\\/barvno\\/korenje (3).svg","files\\/slike\\/hrana\\/barvno\\/korenje (4).svg","files\\/slike\\/hrana\\/barvno\\/korenje (5).svg","files\\/slike\\/hrana\\/barvno\\/korenje (6).svg","files\\/slike\\/hrana\\/barvno\\/limona.svg","files\\/slike\\/hrana\\/barvno\\/lubenica.svg","files\\/slike\\/hrana\\/barvno\\/marelica.svg","files\\/slike\\/hrana\\/barvno\\/meso.svg","files\\/slike\\/hrana\\/barvno\\/paprika.svg","files\\/slike\\/hrana\\/barvno\\/paradiznik.svg","files\\/slike\\/hrana\\/barvno\\/pomaranca.svg","files\\/slike\\/hrana\\/barvno\\/repa.svg","files\\/slike\\/hrana\\/barvno\\/sladoled.svg","files\\/slike\\/hrana\\/barvno\\/sladoled01.svg","files\\/slike\\/hrana\\/barvno\\/sliva.svg","files\\/slike\\/hrana\\/barvno\\/toast.svg","files\\/slike\\/kuhinja\\/barvno\\/cajnik.svg","files\\/slike\\/kuhinja\\/barvno\\/cajnik01.svg","files\\/slike\\/kuhinja\\/barvno\\/cajnik02.svg","files\\/slike\\/kuhinja\\/barvno\\/cedilo.svg","files\\/slike\\/kuhinja\\/barvno\\/deska.svg","files\\/slike\\/kuhinja\\/barvno\\/deska01.svg","files\\/slike\\/kuhinja\\/barvno\\/detergent.svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (1).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (2).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (3).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (4).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (5).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec (6).svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec01.svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec02.svg","files\\/slike\\/kuhinja\\/barvno\\/kozarec03.svg","files\\/slike\\/kuhinja\\/barvno\\/kroznik.svg","files\\/slike\\/kuhinja\\/barvno\\/krozniki.svg","files\\/slike\\/kuhinja\\/barvno\\/kuhalnica.svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (1).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (2).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (3).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (6).svg","files\\/slike\\/kuhinja\\/barvno\\/pladen.svg","files\\/slike\\/kuhinja\\/barvno\\/ponev.svg","files\\/slike\\/kuhinja\\/barvno\\/ponev01.svg","files\\/slike\\/kuhinja\\/barvno\\/poper.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda01.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda02.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda03.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda04.svg","files\\/slike\\/kuhinja\\/barvno\\/posoda05.svg","files\\/slike\\/kuhinja\\/barvno\\/skleda.svg","files\\/slike\\/kuhinja\\/barvno\\/skleda01.svg","files\\/slike\\/kuhinja\\/barvno\\/skleda02.svg","files\\/slike\\/kuhinja\\/barvno\\/skodelica.svg","files\\/slike\\/kuhinja\\/barvno\\/skodelica01.svg","files\\/slike\\/kuhinja\\/barvno\\/skodelica02.svg","files\\/slike\\/kuhinja\\/barvno\\/sol.svg","files\\/slike\\/kuhinja\\/barvno\\/steklenica.svg","files\\/slike\\/kuhinja\\/barvno\\/steklenica01.svg","files\\/slike\\/kuhinja\\/barvno\\/steklenica02.svg","files\\/slike\\/kuhinja\\/barvno\\/vaza.svg","files\\/slike\\/kuhinja\\/barvno\\/vaza01.svg","files\\/slike\\/kuhinja\\/barvno\\/vaza02.svg","files\\/slike\\/kuhinja\\/barvno\\/vaza03.svg","files\\/slike\\/kuhinja\\/barvno\\/vrc.svg","files\\/slike\\/kuhinja\\/barvno\\/vrc01.svg"],"description":"Nabor slik - skupina 6","edit_instructions":"Nabor slik predmetov, ki se lahko pojavijo na eni od lokacij. Lokacije so definirane s pivoti, zato ne priporo\\u010damo spreminjanje tega.","constraints":""},"elements7":{"type":"filelist","default_value":["files\\/slike\\/kuhinja\\/barvno\\/kuhalnica.svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (1).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (2).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (3).svg","files\\/slike\\/kuhinja\\/barvno\\/metlica (6).svg","files\\/slike\\/kuhinja\\/barvno\\/noz.svg","files\\/slike\\/kuhinja\\/barvno\\/noz01.svg","files\\/slike\\/kuhinja\\/barvno\\/noz02.svg","files\\/slike\\/kuhinja\\/barvno\\/obracalka.svg","files\\/slike\\/kuhinja\\/barvno\\/obracalka.svg","files\\/slike\\/kuhinja\\/barvno\\/vilice.svg","files\\/slike\\/kuhinja\\/barvno\\/zajemalka.svg","files\\/slike\\/kuhinja\\/barvno\\/zlica (2).svg","files\\/slike\\/kuhinja\\/barvno\\/zlica.svg","files\\/slike\\/kuhinja\\/barvno\\/zlica01.svg",""],"description":"Nabor slik - skupina 7","edit_instructions":"Nabor slik predmetov, ki se lahko pojavijo na eni od lokacij. Lokacije so definirane s pivoti, zato ne priporo\\u010damo spreminjanje tega.","constraints":""}}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', 0),
(337, 1, '123.123.123.123', '2015-09-28 18:57:35', 'Sestavi me', 'games/game.html', 'Uporabnik mora sestaviti sestavljanko. Učitelj lahko izbira slike, ki jih mora uporabnik sestaviti in kako so razrezane v sestavljanko.', '1.0', 'Matevž Baloh', '2015-07-22', '1.0', 'AGPL', 'V tej igri mora uporabnik sestaviti sestavljanko na podanem polju. Učitelj lahko določi nabor možnih slik ter nabor možnih razrezov. Pri izboru razrezov mora izbrati take, ki pravilno razrežejo podano sliko - da se razrezi ne bodo prekrivali in da ne bo praznih delov.', 'COMMENT', 'default|inverted|contrast', 'SL|EN', '{"SL":{"language":"Slovensko","dict":{"L_INSTRUCTIONS_VIEW":"Dobro si oglej slike in nadaljuj.","L_INSTRUCTIONS_SELECT_SEEN":"Izberi prej videne slike.","L_INSTRUCTIONS_SELECT_UNSEEN":"Izberi nove slike.","L_INCORRECT_LINE_ONE":"Ne obupaj,","L_INCORRECT_LINE_TWO":"poskusi ponovno.","L_CORRECT_LINE_ONE":"\\u010cestitam","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Naprej","L_SELECT_STATUS_DISPLAY_1":"Izbrani:","L_SELECT_STATUS_DISPLAY_2":"od","L_CONTINUE":"Nadaljuj","L_BEGIN":"ZA\\u010cNI","L_NEXT_LEVEL":"Naslednja stopnja"}},"EN":{"language":"English","dict":{"L_INSTRUCTIONS_VIEW":"Look at the images, when ready, click continue.","L_INSTRUCTIONS_SELECT_SEEN":"Find the previously seen images.","L_INSTRUCTIONS_SELECT_UNSEEN":"Find the new images.","L_INCORRECT_LINE_ONE":"Don","L_INCORRECT_LINE_TWO":"try again.","L_CORRECT_LINE_ONE":"Congratulations","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Continue","L_SELECT_STATUS_DISPLAY_1":"Selected:","L_SELECT_STATUS_DISPLAY_2":"out of","L_CONTINUE":"Continue","L_BEGIN":"BEGIN","L_NEXT_LEVEL":"Next level"}}}', '{"instructions":{"type":"string","default_value":"Sestavi sestavljanko.","description":"Uvodna navodila","edit_instructions":"Navodila prilagodite tako, da opi\\u0161ejo igro, ki jo priredite, sicer jih pustite taka kot so","constraints":"1|300"},"files":{"type":"filelist","default_value":["files\\/sestavi me\\/motiv\\/barvno\\/kuhar.svg","files\\/sestavi me\\/motiv\\/barvno\\/jez.svg","files\\/sestavi me\\/motiv\\/barvno\\/morje.svg","files\\/sestavi me\\/motiv\\/barvno\\/pingvin.svg","files\\/sestavi me\\/motiv\\/barvno\\/potapljac.svg","files\\/sestavi me\\/motiv\\/barvno\\/polje.svg","files\\/sestavi me\\/motiv\\/barvno\\/luna.svg","files\\/sestavi me\\/motiv\\/barvno\\/sova.svg","files\\/sestavi me\\/motiv\\/barvno\\/raca.svg","files\\/sestavi me\\/motiv\\/barvno\\/riba.svg","files\\/sestavi me\\/motiv\\/barvno\\/ucenec.svg","files\\/sestavi me\\/motiv\\/barvno\\/tenis.svg"],"description":"Motiv.","edit_instructions":"Izberite take slike, za katere bo jasno videti kako se sestavijo, tudi ko bodo razrezane. Predlagamo slike, ki nimajo prozornega ozadja, saj se lahko zgodi, da bo kak\\u0161en od ko\\u0161\\u010dkov popolnoma prozoren, kar pomeni, da bo sestavljanka nere\\u0161ljiva.","constraints":""},"puzzles0":{"type":"filelist","default_value":["files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (1).svg","files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (2).svg","files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (3).svg","files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (4).svg","files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (5).svg","files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (6).svg","files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (7).svg","files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (8).svg","files\\/sestavi me\\/razrez\\/puzli_9\\/puzli_9 (9).svg"],"description":"Oblika razreza 1.","edit_instructions":"Izberite vse slike v setu, da zagotovite, da se bo dalo celo sliko sestavit.","constraints":""},"puzzles1":{"type":"filelist","default_value":["files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (1).svg","files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (2).svg","files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (3).svg","files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (4).svg","files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (5).svg","files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (6).svg","files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (7).svg","files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (8).svg","files\\/sestavi me\\/razrez\\/puzli01_9\\/puzli01_9 (9).svg"],"description":"Oblika razreza 2.","edit_instructions":"Izberite vse slike v setu, da zagotovite, da se bo dalo celo sliko sestavit.","constraints":""},"puzzles2":{"type":"filelist","default_value":["files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (1).svg","files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (2).svg","files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (3).svg","files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (4).svg","files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (5).svg","files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (6).svg","files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (7).svg","files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (8).svg","files\\/sestavi me\\/razrez\\/kvadrat_9\\/kvadrat_9 (9).svg"],"description":"Oblika razreza 3.","edit_instructions":"Izberite vse slike v setu, da zagotovite, da se bo dalo celo sliko sestavit.","constraints":""},"puzzles3":{"type":"filelist","default_value":[""],"description":"Oblika razreza 4.","edit_instructions":"Izberite vse slike v setu, da zagotovite, da se bo dalo celo sliko sestavit.","constraints":""},"puzzles4":{"type":"filelist","default_value":[""],"description":"Oblika razreza 5.","edit_instructions":"Izberite vse slike v setu, da zagotovite, da se bo dalo celo sliko sestavit.","constraints":""}}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', 0),
(323, 1, '123.123.123.123', '2015-09-28 15:12:35', 'Manjkajoči del', 'games/game.html', 'Odeja ima luknjo! Uporabnik mora iz nabora izbrati tisti kos, ki luknjo pravilno zakrpa. Učitelj lahko izbere možne oblike luknje in možne vzorce.', '1.0', 'Matevž Baloh', '2015-07-22', '1.0', 'AGPL', 'V tej igri se uporabniku prikaže zavesa, v kateri je luknja. Na desni strani pa se prikaže nabor izrezov. En od izrezov je primeren, da luknjo zakrpa. Uporabnik mora tega izbrati. Učitelj lahko določi nabor vzorcev za zaveso, nabor možnih oblik luknje v zavesi. Pri izboru obeh naborov mora učitelj poskrbeti, da bodo vsi možni izrezi izbranih vzorcev unikatni - da ne bosta dva enaka izreza različnih vzorcev kreirala isti izrez. ', 'COMMENT', 'default|inverted|contrast', 'SL|EN', '{"EN":{"language":"English","dict":{"L_INSTRUCTIONS_VIEW":"Look at the images, when ready, click continue.","L_INSTRUCTIONS_SELECT_SEEN":"Find the previously seen images.","L_INSTRUCTIONS_SELECT_UNSEEN":"Find the new images.","L_INCORRECT_LINE_ONE":"Don''t give up,","L_INCORRECT_LINE_TWO":"try again.","L_CORRECT_LINE_ONE":"Congratulations","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Continue","L_SELECT_STATUS_DISPLAY_1":"Selected:","L_SELECT_STATUS_DISPLAY_2":"out of","L_CONTINUE":"Continue","L_BEGIN":"BEGIN","L_NEXT_LEVEL":"Next level"}},"SL":{"language":"Slovensko","dict":{"L_INSTRUCTIONS_VIEW":"Dobro si oglej slike in nadaljuj.","L_INSTRUCTIONS_SELECT_SEEN":"Izberi prej videne slike.","L_INSTRUCTIONS_SELECT_UNSEEN":"Izberi nove slike.","L_INCORRECT_LINE_ONE":"Ne obupaj,","L_INCORRECT_LINE_TWO":"poskusi ponovno.","L_CORRECT_LINE_ONE":"Čestitam","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Naprej","L_SELECT_STATUS_DISPLAY_1":"Izbrani:","L_SELECT_STATUS_DISPLAY_2":"od","L_CONTINUE":"Nadaljuj","L_BEGIN":"ZAČNI","L_NEXT_LEVEL":"Naslednja stopnja"}}}', '{"instructions":{"type":"string","default_value":"Pojavila se bo slika odeje z luknjo in nabor slik, ki odejo zakrpajo. Izberite sliko, ki luknjo zakrpa.","description":"Uvodna navodila","edit_instructions":"Navodila prilagodite tako, da opi\\u0161ejo igro, ki jo priredite, sicer jih pustite taka kot so","constraints":"1|300"},"selectN":{"type":"int","default_value":"9","description":"\\u0160tevilo mo\\u017enih odgovorov.","edit_instructions":"Izberite primerno \\u0161tevilo za va\\u0161e u\\u010dence","constraints":"1|9|1"},"files":{"type":"filelist","default_value":["files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (1).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (2).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (3).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (4).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (5).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (6).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (7).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (8).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (9).svg","files\\/manjkajoci del\\/vzorec\\/barvno\\/ladja_jadrnica\\/ladja_jadrnica (10).svg"],"description":"Vzorec.","edit_instructions":"Izberite vzorce, ki se lahko pojavijo na odeji. Poskusite izbrat take, ki zgledajo vredu \\u010de so v vlogi vzorca. Za pomo\\u010d si lahko predstavljate mre\\u017eo teh slik.","constraints":""},"masks":{"type":"filelist","default_value":["files\\/manjkajoci del\\/izrez\\/krog.svg","files\\/manjkajoci del\\/izrez\\/kvadrat.svg","files\\/manjkajoci del\\/izrez\\/osemkotnik.svg"],"description":"Oblika izreza.","edit_instructions":"Izbirate lahko katerokoli sliko s prozornim ozadjem, ne glede na barvo. \\u010ce npr. izberete \\u010de\\u0161njo s prozornim ozadjem, se bo lahko v odeji pojavila luknja v obliki \\u010de\\u0161nje","constraints":""}}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', 0);
INSERT INTO `zs_game` (`game_id`, `game_upload_user_id`, `game_upload_user_ip`, `game_upload_datetime`, `game_name`, `game_file_location`, `game_description`, `game_signature_version`, `game_author`, `game_date_of_build`, `game_version`, `game_license`, `game_teacher_instructions`, `game_comment`, `game_styles_available`, `game_languages`, `game_dictionary`, `game_variables`, `game_difficulty`, `game_time_limit`, `game_levels`, `game_retries_available`, `game_deleted`) VALUES
(320, 1, '123.123.123.123', '2015-09-27 14:24:11', 'Kaj sem videl?', 'games/game.html', 'Igra s karticami - Najprej se pojavi nabor slik, ki si jih uporabnik mora zapomniti. Nato zginejo in se pojavi večji nabor slik. Uporabmik mora izmed nabora poiskati vse prej videne. Učitelj lahko določa nabor slik in težavnost.', '1.0', 'Matevž Baloh', '2015-07-22', '1.0', 'AGPL', 'V tej igri se uporabniku prikaže nabor slik, katerega si mora zaponiti. Nato slike zginejo in se prikaže drug, večji nabor. Izmed novih slik mora uporabnik izbrati prej videne. Ko pravilno odgovri na nekaj primerov, lahko napreduje v naslednji nivo, v katerem se povečata števili slik v obeh naborih. Učitelj lahko določi nabor slik, koliko slik se pokaže v obeh naborih v prvem nivoju, koliko pravilnih odgovorov je potrebnih za napredovat v višji nivo in koliko napredka uporabnik izgubi, če je njegov odgovor napačen.', 'COMMENT', 'default|inverted|contrast', 'SL|EN', '{"EN":{"language":"English","dict":{"L_INSTRUCTIONS_VIEW":"Look at the images, when ready, click continue.","L_INSTRUCTIONS_SELECT_SEEN":"Find the previously seen images.","L_INSTRUCTIONS_SELECT_UNSEEN":"Find the new images.","L_INCORRECT_LINE_ONE":"Don''t give up,","L_INCORRECT_LINE_TWO":"try again.","L_CORRECT_LINE_ONE":"Congratulations","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Continue","L_SELECT_STATUS_DISPLAY_1":"Selected:","L_SELECT_STATUS_DISPLAY_2":"out of","L_CONTINUE":"Continue","L_BEGIN":"BEGIN","L_NEXT_LEVEL":"Next level"}},"SL":{"language":"Slovensko","dict":{"L_INSTRUCTIONS_VIEW":"Dobro si oglej slike in nadaljuj.","L_INSTRUCTIONS_SELECT_SEEN":"Izberi prej videne slike.","L_INSTRUCTIONS_SELECT_UNSEEN":"Izberi nove slike.","L_INCORRECT_LINE_ONE":"Ne obupaj,","L_INCORRECT_LINE_TWO":"poskusi ponovno.","L_CORRECT_LINE_ONE":"Čestitam","L_CORRECT_LINE_TWO":"","L_BUTTON_CONTINUE":"Naprej","L_SELECT_STATUS_DISPLAY_1":"Izbrani:","L_SELECT_STATUS_DISPLAY_2":"od","L_CONTINUE":"Nadaljuj","L_BEGIN":"ZAČNI","L_NEXT_LEVEL":"Naslednja stopnja"}}}', '{"instructions":{"type":"string","default_value":"Pojavile se bodo slike, ki si jih zapomni. Med mno\\u017eico slik izberi tiste, ki si jih prej videl. ","description":"Uvodna navodila","edit_instructions":"Navodila prilagodite tako, da opi\\u0161ejo igro, ki jo priredite, sicer jih pustite taka kot so","constraints":"1|300"},"selectN":{"type":"int","default_value":"2","description":"\\u0160tevilo slik, ki si jih mora uporabnik zapomnit (na prvi stopnji).","edit_instructions":"Izberite primerno \\u0161tevilo, da ne bo u\\u010dencem prelahko na za\\u010detku. Npr. 2\\/3 za za\\u010detnike, 4\\/5 za napredne, 6\\/7 je maksimum, ki ga predlagamo.","constraints":"1|21|1"},"groupN":{"type":"int","default_value":"4","description":"\\u0160tevilo slik, med katerimi uporabnik izbere predhodno videne.","edit_instructions":"Izberite primerno \\u0161tevilo, da ne bo u\\u010dencem prelahko na za\\u010detku. Npr. 3-5 za za\\u010detnike, 6-10 za napredne, 11-15 je maksimum, ki ga predlagamo.","constraints":"1|21|1"},"starLossOnFail":{"type":"int","default_value":"1","description":"\\u0160tevilo zvezdic, ki jih uporabnik izgubi ob nepravilnem odgovoru.","edit_instructions":"Za redno igro predlagamo 0 ali 1, lahko se pa tudi pove\\u010da na npr. 5 in igro igra tako, da mora u\\u010denec igro pravilno re\\u0161iti npr. 5 krat zapored, \\u010de kadarkoli zgre\\u0161i, izgubi vse zvezdice in mora od za\\u010detka (Za tak na\\u010din igre nastavite to in progressToContinue na isto \\u0161tevilo).","constraints":"1|100|1"},"progressToContinue":{"type":"int","default_value":"3","description":"\\u0160tevilo zvezdic, potrebnih za napredovanje v naslednjo stopnjo.","edit_instructions":"Za hitro napredovanje predlagamo 3, za normalno igro 5-7, za po\\u010dasno igro 10, lahko pa poljubno mnogo. Igralec lahko ostane na istem nivoju tudi ko \\u017ee dose\\u017ee potrebno \\u0161tevilo zvezdic za napredovanje.","constraints":"1|100|1"},"files":{"type":"filelist","default_value":["files\\/slike\\/hrana\\/barvno\\/banana.svg","files\\/slike\\/hrana\\/barvno\\/bonbon.svg","files\\/slike\\/hrana\\/barvno\\/brokoli.svg","files\\/slike\\/hrana\\/barvno\\/cebula.svg","files\\/slike\\/hrana\\/barvno\\/cesen.svg","files\\/slike\\/hrana\\/barvno\\/cesnja (1).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (2).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (3).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (4).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (5).svg","files\\/slike\\/hrana\\/barvno\\/cesnja (6).svg","files\\/slike\\/hrana\\/barvno\\/cili.svg","files\\/slike\\/hrana\\/barvno\\/fizol.svg","files\\/slike\\/hrana\\/barvno\\/fizolcki.svg","files\\/slike\\/hrana\\/barvno\\/goba.svg","files\\/slike\\/hrana\\/barvno\\/grozdje (1).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (2).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (3).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (4).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (5).svg","files\\/slike\\/hrana\\/barvno\\/grozdje (6).svg","files\\/slike\\/hrana\\/barvno\\/hlebec.svg","files\\/slike\\/hrana\\/barvno\\/hruska.svg","files\\/slike\\/hrana\\/barvno\\/jabolko.svg","files\\/slike\\/hrana\\/barvno\\/jagoda.svg","files\\/slike\\/hrana\\/barvno\\/jajcevec.svg","files\\/slike\\/hrana\\/barvno\\/korenje (1).svg","files\\/slike\\/hrana\\/barvno\\/korenje (2).svg","files\\/slike\\/hrana\\/barvno\\/korenje (3).svg","files\\/slike\\/hrana\\/barvno\\/korenje (4).svg","files\\/slike\\/hrana\\/barvno\\/korenje (5).svg","files\\/slike\\/hrana\\/barvno\\/korenje (6).svg","files\\/slike\\/hrana\\/barvno\\/lubenica.svg","files\\/slike\\/hrana\\/barvno\\/marelica.svg","files\\/slike\\/hrana\\/barvno\\/paprika.svg","files\\/slike\\/hrana\\/barvno\\/paradiznik.svg","files\\/slike\\/hrana\\/barvno\\/sladoled.svg","files\\/slike\\/hrana\\/barvno\\/sladoled01.svg","files\\/slike\\/hrana\\/barvno\\/sliva.svg","files\\/slike\\/hrana\\/barvno\\/toast.svg"],"description":"Nabor slik.","edit_instructions":"Za la\\u017eje igre izberite zelo razli\\u010dne slike, za te\\u017eje igre izberite podobne slike. Tu lahko igro priredite, da ustreza potrebam va\\u0161ega predmeta, zato predlagamo, da izbirate slike, ki so povezane s predmetom, ki ga u\\u010dite.","constraints":"1|100"}}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":1,"editable":0,"edit_instructions":""}', 0),
(354, 1, '123.123.123.123', '2015-09-29 15:30:47', 'Brajica Crke', 'games/upload/370/index.html', 'Brajica Game Crke', '1.0', 'Mojca Črnigoj', '2015-09-29', '1', 'AGPL', 'Navodila učitelju', 'En komentarček', 'default|blind', 'SL|EN', '[]', '{"crke":{"type":"stringlist","default_value":["a","b","c","\\u010d","d","e","f","g","h","i","j","k","l","m","n","o","p","r","s","\\u0161","t","u","v","z","\\u017e"],"description":"Vpi\\u0161ite \\u010drke, ki se bodo uporabile kot nabor \\u010drk igre","edit_instructions":"Vpi\\u0161ite \\u010drke, ki se bodo uporabile kot nabor \\u010drk igre","constraints":""},"vrstaZacetneAbecede":{"type":"int","default_value":"0","description":"0=Brajica, 1= latinica: Vpi\\u0161ite 0 ali 1 za izbiro igre z vpra\\u0161ano \\u010drko v Brajici ali v \\u010drnem tisku","edit_instructions":"0=Brajica, 1= latinica: Vpi\\u0161ite 0 ali 1 za izbiro igre z vpra\\u0161ano \\u010drko v Brajici ali v \\u010drnem tisku","constraints":"between 0 and 1"}}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":10,"editable":1,"edit_instructions":"\\u010casovni limit"}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', 0),
(356, 1, '123.123.123.123', '2015-09-29 15:41:18', 'Brajica Besede', 'games/upload/372/index.html', 'Brajica Game Crke', '1.0', 'Brajica Besede', '2015-09-29', '1', 'AGPL', 'Navodila učitelju', 'En komentarček', 'default|blind', 'SL|EN', '[]', '{"besede":{"type":"stringlist","default_value":["soNce","maslo","Jagoda","morje","korala","de\\u017dNik","medved","\\u0161olar","\\u010distilo","ma\\u010dka","\\u010de\\u0161nja","vrti\\u010dek","\\u010debula","\\u010dmrlj","VIJAK","trikot","zmaga","skodela","kostanj"],"description":"Vpi\\u0161ite \\u010drke, ki se bodo uporabile kot nabor \\u010drk igre","edit_instructions":"Vpi\\u0161ite \\u010drke, ki se bodo uporabile kot nabor \\u010drk igre","constraints":""},"vrstaZacetneAbecede":{"type":"int","default_value":"0","description":"0=Brajica, 1= latinica: Vpi\\u0161ite 0 ali 1 za izbiro igre z vpra\\u0161ano \\u010drko v Brajici ali v \\u010drnem tisku","edit_instructions":"0=Brajica, 1= latinica: Vpi\\u0161ite 0 ali 1 za izbiro igre z vpra\\u0161ano \\u010drko v Brajici ali v \\u010drnem tisku","constraints":"between 0 and 1"}}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":10,"editable":1,"edit_instructions":"\\u010casovni limit"}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', 0),
(357, 1, '123.123.123.123', '2015-09-29 15:55:58', 'Brajica Zgodovina', 'games/upload/373/index.html', 'Brajica Game Crke', '1.0', 'Mojca Črnigoj', '2015-09-29', '1', 'AGPL', 'Navodila učitelju', 'En komentarček', 'default|blind', 'SL|EN', '[]', '[]', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":10,"editable":1,"edit_instructions":"\\u010casovni limit"}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', 0),
(358, 1, '123.123.123.123', '2015-09-29 16:03:37', 'Brajica  Kaj je', 'games/upload/374/index.html', 'Zgodovina Brajice', '1.0', 'Brajica Zgodovina', '2015-09-29', '1', 'AGPL', 'Navodila učitelju', 'En komentarček', 'default|blind', 'SL|EN', '[]', '[]', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":10,"editable":1,"edit_instructions":"\\u010casovni limit"}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', 0),
(359, 1, '123.123.123.123', '2015-09-29 16:06:58', 'Brajica  Zrcalce Zrcalce', 'games/upload/375/index.html', 'Prikaz pretvorbe', '1.0', 'Brajica  Kaj je', '2015-09-29', '1', 'AGPL', 'Navodila učitelju', 'En komentarček', 'default|blind', 'SL|EN', '[]', '[]', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":10,"default":10,"editable":1,"edit_instructions":"\\u010casovni limit"}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":""}', 0),
(362, 1, '123.123.123.123', '2015-09-30 16:38:13', 'Slepo tipkanje', 'games/upload/379/index.html', 'Tipkanje', '1.0', 'Slepo tipkanje', '2015-05-05', '1.0', 'AGPL', 'TEACHER INSTRUCTIONS', 'COMMENT', 'default', 'SL|EN', '[]', '{"text":{"type":"string","default_value":"a b c d e f g h i j k l m n o p q r s t u v w x y z","description":"Besede za vajo","edit_instructions":"Besede naj bodo lo\\u010dene s presledki","constraints":"1|5000"},"displayType":{"type":"int","default_value":"1","description":"Na\\u010din prikaza besedila","edit_instructions":"1 - celo besedilo, 2 - vrstica besedila, 3 - vrstica besedila z zamikanjem, 4 - ena beseda, 5 - ena \\u010drka","constraints":"1|5"},"lineLength":{"type":"int","default_value":"3","description":"\\u0160tevilo besed v generiranem besedilu","edit_instructions":"Celo \\u0161tevilo","constraints":"1|100"},"lineLengthWhole":{"type":"int","default_value":"3","description":"\\u0160tevilo znakov v vrstici za na\\u010dina 2 in 3","edit_instructions":"Celo \\u0161tevilo","constraints":"1|100"},"markColor":{"type":"string","default_value":"000000","description":"Barva obrobe \\u010drke, ki je trenutno v fokusu","edit_instructions":"Hex","constraints":"1|100"},"fontColor":{"type":"string","default_value":"223344","description":"Barva nenatipkanega besedila","edit_instructions":"Hex","constraints":"1|100"},"fontSize":{"type":"int","default_value":"50","description":"Velikost pisave","edit_instructions":"Celo \\u0161tevilo","constraints":"12|200"},"cFontColor":{"type":"string","default_value":"000000","description":"Barva pisave pravilno natipkanega besedila","edit_instructions":"Hex","constraints":"1|100"},"mFontColor":{"type":"string","default_value":"000000","description":"Barva pisave napa\\u010dno natipkanega besedila","edit_instructions":"Hex","constraints":"1|100"},"cBackColor":{"type":"string","default_value":"FFF277","description":"Barva ozadja pravilno natipkanega besedila","edit_instructions":"Hex","constraints":"1|100"},"mBackColor":{"type":"string","default_value":"FF7C78","description":"Barva ozadja napa\\u010dno natipkanega besedila","edit_instructions":"Hex","constraints":"1|100"},"backgroundColor":{"type":"string","default_value":"ffffff","description":"Barva ozadja","edit_instructions":"Hex","constraints":"1|100"},"nacin":{"type":"int","default_value":"1","description":"Zahtevnost","edit_instructions":"0 - osnovni na\\u010din, 1 - napredni na\\u010din","constraints":"0|1"}}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":"irrelevant"}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":"irrelevant"}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":"irrelevant"}', '{"min":1,"max":1,"default":1,"editable":0,"edit_instructions":"irrelevant"}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `zs_instance`
--

CREATE TABLE `zs_instance` (
  `instance_id` int(11) NOT NULL,
  `instance_name` varchar(255) DEFAULT NULL,
  `instance_glyph` varchar(255) DEFAULT NULL,
  `instance_description` text,
  `instance_class_id` int(11) DEFAULT NULL,
  `instance_created_datetime` datetime DEFAULT NULL,
  `instance_created_user_id` int(11) DEFAULT NULL,
  `instance_game_id` int(11) DEFAULT NULL,
  `instance_difficulty` int(11) DEFAULT NULL,
  `instance_time_limit` int(11) DEFAULT NULL,
  `instance_levels` int(11) DEFAULT NULL,
  `instance_retries_available` int(11) DEFAULT NULL,
  `instance_deleted` int(11) DEFAULT '0' COMMENT '0 if not deleted, 1 if deleted'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_instance`
--

INSERT INTO `zs_instance` (`instance_id`, `instance_name`, `instance_glyph`, `instance_description`, `instance_class_id`, `instance_created_datetime`, `instance_created_user_id`, `instance_game_id`, `instance_difficulty`, `instance_time_limit`, `instance_levels`, `instance_retries_available`, `instance_deleted`) VALUES
(639, 'Vse črke', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 23:07:05', 1, 362, 0, 0, 0, 0, 0),
(637, 'Ž', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:59:49', 1, 362, 0, 0, 0, 0, 0),
(638, 'Ž besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 23:00:24', 1, 362, 0, 0, 0, 0, 0),
(635, 'Š.', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:58:42', 1, 362, 0, 0, 0, 0, 0),
(636, 'Š. besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:59:16', 1, 362, 0, 0, 0, 0, 0),
(634, 'TZ besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:57:40', 1, 362, 0, 0, 0, 0, 0),
(632, 'BN besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:56:28', 1, 362, 0, 0, 0, 0, 0),
(630, 'OP besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:54:58', 1, 362, 0, 0, 0, 0, 0),
(628, 'C, besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:53:35', 1, 362, 0, 0, 0, 0, 0),
(625, 'VM', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:51:55', 1, 362, 0, 0, 0, 0, 0),
(624, 'RU besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:51:16', 1, 362, 0, 0, 0, 0, 0),
(621, 'EI', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:49:13', 1, 362, 0, 0, 0, 0, 0),
(619, 'GH', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:48:08', 1, 362, 0, 0, 0, 0, 0),
(618, 'ASDF JKLČ', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:47:27', 1, 362, 0, 0, 0, 0, 0),
(616, 'AČ', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:46:15', 1, 362, 0, 0, 0, 0, 0),
(617, 'SL', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:46:52', 1, 362, 0, 0, 0, 0, 0),
(614, 'FJ', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:44:14', 1, 362, 0, 0, 0, 0, 0),
(615, 'DK', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:45:22', 1, 362, 0, 0, 0, 0, 0),
(576, ' Zrcalce, povej!', 'files/prikazne%20slike/brajica.svg', '', 34, '2015-09-29 16:07:28', 1, 359, 0, 10, 0, 0, 0),
(569, 'Brajev stroj - znaki', 'files/prikazne%20slike/brajica.svg', '', 34, '2015-09-29 15:34:44', 1, 354, 0, 10, 0, 0, 0),
(542, 'Kaj je novega', 'files/slike/zivali/sivinsko/cebela.svg', '', 33, '2015-09-28 21:41:37', 1, 318, 0, 0, 0, 0, 0),
(540, 'Sestavi me 2', 'files/sestavi me/motiv/linijsko/ucenec.svg', '', 33, '2015-09-28 21:29:35', 1, 337, 0, 0, 0, 0, 0),
(537, 'Kaj je novega? (2)', 'files/slike/garderoba/temno_ozadje/deznik (1).svg', '', 33, '2015-09-28 19:13:01', 1, 318, 0, 0, 0, 0, 0),
(535, 'Sestavi me', 'files/sestavi me/motiv/barvno/pingvin.svg', '', 33, '2015-09-28 19:00:51', 1, 337, 0, 0, 0, 0, 0),
(534, 'Zaporedja (2)', 'files/slike/geometrija/sivinsko/petkotnik (5).svg', '', 33, '2015-09-28 18:59:43', 1, 312, 0, 0, 0, 0, 0),
(530, 'Kaj sem videl? (2)', 'files/slike/zivali/linijsko/zajec.svg', '', 33, '2015-09-28 18:43:57', 1, 320, 0, 0, 0, 0, 0),
(527, 'Skriti predmeti', 'files/prikazne slike/kuhinja01.svg', '', 33, '2015-09-28 18:26:07', 1, 335, 0, 0, 0, 0, 0),
(522, 'Kaj sem videl?', 'files/prikazne slike/sadje.svg', '', 33, '2015-09-28 18:12:00', 1, 320, 0, 0, 0, 0, 0),
(517, 'Zaporedja', 'files/prikazne slike/orodje.svg', '', 33, '2015-09-28 18:06:41', 1, 312, 0, 0, 0, 0, 0),
(543, 'Kaj manjka 2', 'files/slike/rastline/linijsko/cvetlicni_lonec (3).svg', '', 33, '2015-09-28 21:47:27', 1, 315, 0, 0, 0, 0, 0),
(541, 'Zaporedja', 'files/slike/prazniki/sivinsko/netopir.svg', '', 33, '2015-09-28 21:34:01', 1, 312, 0, 0, 0, 0, 0),
(633, 'TZ', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:57:04', 1, 362, 0, 0, 0, 0, 0),
(631, 'BN', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:55:44', 1, 362, 0, 0, 0, 0, 0),
(629, 'OP', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:54:16', 1, 362, 0, 0, 0, 0, 0),
(627, 'C,', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:53:01', 1, 362, 0, 0, 0, 0, 0),
(626, 'VM besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:52:27', 1, 362, 0, 0, 0, 0, 0),
(525, 'Manjkajoči del', 'files/prikazne slike/prevozna_sredstva.svg', '', 33, '2015-09-28 18:22:33', 1, 323, 0, 0, 0, 0, 0),
(520, 'Kaj manjka?', 'files/prikazne slike/garderoba.svg', '', 33, '2015-09-28 18:10:07', 1, 315, 0, 0, 0, 0, 0),
(519, 'Kaj je novega?', 'files/prikazne slike/kuhinja.svg', '', 33, '2015-09-28 18:08:36', 1, 318, 0, 0, 0, 0, 0),
(623, 'RU', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:50:31', 1, 362, 0, 0, 0, 0, 0),
(622, 'EI besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:49:52', 1, 362, 0, 0, 0, 0, 0),
(620, 'GH besede', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 22:48:41', 1, 362, 0, 0, 0, 0, 0),
(544, 'Kaj sem videl 2', 'files/slike/prevoz/linijsko/ladja.svg', '', 33, '2015-09-28 21:52:30', 1, 320, 0, 0, 0, 0, 0),
(643, 'Zmešnjava besed', 'files/prikazne%20slike/slepo_tipkanje.svg', '', 35, '2015-09-30 23:18:05', 1, 362, 0, 0, 0, 0, 0),
(644, 'Manjkajoči del (2)', 'files/manjkajoci del/vzorec/sivinsko/metulj/metuj (2).svg', '', 33, '2015-10-01 08:23:20', 1, 323, 0, 0, 0, 0, 0),
(645, 'Skriti znak', 'files/prikazne%20slike/brajica.svg', '', 34, '2015-10-01 08:29:24', 1, 354, 0, 10, 0, 0, 0),
(647, 'Skrita beseda', 'files/prikazne%20slike/brajica.svg', '', 34, '2015-10-01 08:31:38', 1, 356, 0, 10, 0, 0, 0),
(648, 'Brajev stroj - besede', 'files/prikazne%20slike/brajica.svg', '', 34, '2015-10-01 08:34:39', 1, 356, 0, 10, 0, 0, 0),
(649, 'Kaj je brajica?', 'files/prikazne%20slike/brajica.svg', '', 34, '2015-10-01 08:36:54', 1, 357, 0, 10, 0, 0, 0),
(650, 'Kako je brajica nastala?', 'files/prikazne%20slike/brajica.svg', '', 34, '2015-10-01 08:37:20', 1, 358, 0, 10, 0, 0, 0),
(651, 'TestnaAktivnost', 'files/ui/privzete_vrednosti.svg', '', 34, '2016-08-04 14:26:18', 1, 354, 0, 10, 0, 0, 1),
(652, 'abc', 'files/prikazne slike/geometrija.svg', '', 34, '2016-08-04 14:48:45', 1, 357, 0, 10, 0, 0, 1),
(653, 'MojaAktivnost3', 'files/prikazne slike/kopalnica01.svg', '', 34, '2016-08-05 12:03:05', 1, 354, 0, 10, 0, 0, 1),
(654, 'aktivnost..', 'files/prikazne slike/orodje.svg', '', 36, '2016-08-17 12:14:35', 1, 354, 0, 10, 0, 0, 1),
(655, 'test123', 'files/prikazne slike/kopalnica01.svg', '', 34, '2016-08-23 09:19:06', 1, 354, 0, 10, 0, 0, 1),
(656, 'fdufgdgšhfgs', 'files/prikazne slike/kuhinja.svg', '', 34, '2016-08-23 09:57:49', 1, 354, 0, 10, 0, 0, 1),
(657, 'fdufgdgšhfgs', 'files/prikazne slike/kuhinja.svg', '', 34, '2016-08-23 09:59:28', 1, 354, 0, 10, 0, 0, 1),
(658, 'fgšsaghadfgifdg', 'files/prikazne slike/sadje.svg', '', 34, '2016-08-23 09:59:53', 1, 354, 0, 10, 0, 0, 1),
(659, 'fše0wifjsd', 'files/prikazne slike/sport.svg', '', 34, '2016-08-23 10:01:28', 1, 354, 0, 10, 0, 0, 1),
(660, 'Test2', 'files/slike/preview.png', '', 34, '2017-02-22 18:59:08', 1, 356, 0, 10, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zs_score`
--

CREATE TABLE `zs_score` (
  `score_id` int(11) NOT NULL,
  `score_datetime` datetime DEFAULT NULL,
  `score_user_id` int(11) DEFAULT NULL,
  `score_user_ip` varchar(45) DEFAULT NULL,
  `score_instance_id` int(11) DEFAULT NULL,
  `score_value` int(11) DEFAULT NULL,
  `score_description` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_score`
--

INSERT INTO `zs_score` (`score_id`, `score_datetime`, `score_user_id`, `score_user_ip`, `score_instance_id`, `score_value`, `score_description`) VALUES
(339, '2016-08-03 12:06:14', 1, '::1', 527, 1, ''),
(340, '2016-08-04 14:40:41', 1, '::1', 517, 3, ''),
(341, '2016-08-04 14:41:37', 1, '::1', 530, 3, ''),
(342, '2016-08-04 16:25:44', 1, '::1', 615, 1, 'hitrost:186|hitrostNapake:-358|procent:41'),
(343, '2016-08-05 11:58:59', 1, '::1', 517, 1, ''),
(344, '2016-08-18 09:53:18', 1, '::1', 615, 1, 'hitrost:2|hitrostNapake:1|procent:86'),
(345, '2016-08-18 12:25:33', 1, '::1', 616, 1, 'hitrost:95|hitrostNapake:-231|procent:31'),
(346, '2016-08-18 12:42:17', 1, '::1', 616, 3, 'hitrost:24|hitrostNapake:16|procent:93'),
(347, '2016-08-18 13:28:40', 1, '::1', 614, 1, 'hitrost:233|hitrostNapake:-685|procent:21'),
(348, '2016-08-18 13:30:55', 1, '::1', 614, 1, 'hitrost:283|hitrostNapake:-517|procent:43'),
(349, '2016-08-18 13:31:33', 1, '::1', 614, 1, 'hitrost:290|hitrostNapake:-544|procent:42'),
(350, '2016-08-18 13:31:59', 1, '::1', 614, 1, 'hitrost:183|hitrostNapake:-620|procent:12'),
(351, '2016-08-18 13:32:16', 1, '::1', 614, 1, 'hitrost:242|hitrostNapake:-970|procent:0'),
(352, '2016-08-18 13:33:38', 1, '::1', 614, 1, 'hitrost:276|hitrostNapake:-491|procent:44'),
(353, '2016-08-18 13:40:15', 1, '::1', 614, 1, 'hitrost:305|hitrostNapake:-1218|procent:0'),
(354, '2016-08-18 13:40:59', 1, '::1', 614, 1, 'hitrost:305|hitrostNapake:-634|procent:38'),
(355, '2016-08-18 13:45:36', 1, '::1', 615, 1, 'hitrost:224|hitrostNapake:-387|procent:45'),
(356, '2016-08-18 13:46:05', 1, '::1', 617, 1, 'hitrost:107|hitrostNapake:-288|procent:26'),
(357, '2016-08-18 13:54:37', 1, '::1', 614, 1, 'hitrost:224|hitrostNapake:-455|procent:39'),
(358, '2016-08-18 13:55:00', 1, '::1', 614, 1, 'hitrost:220|hitrostNapake:-524|procent:32'),
(359, '2016-08-18 13:56:56', 1, '::1', 614, 1, 'hitrost:41|hitrostNapake:-50|procent:56'),
(360, '2016-08-18 14:24:21', 1, '::1', 615, 1, 'hitrost:183|hitrostNapake:-297|procent:47'),
(361, '2016-08-18 14:39:57', 1, '::1', 615, 1, 'hitrost:228|hitrostNapake:-452|procent:40'),
(362, '2016-08-18 14:40:17', 1, '::1', 615, 1, 'hitrost:154|hitrostNapake:-337|procent:36'),
(363, '2016-08-18 14:40:51', 1, '::1', 614, 1, 'hitrost:87|hitrostNapake:-115|procent:54'),
(364, '2016-08-18 15:01:04', 1, '::1', 614, 1, 'hitrost:145|hitrostNapake:-250|procent:45'),
(365, '2016-08-18 15:01:30', 1, '::1', 614, 1, 'hitrost:163|hitrostNapake:-314|procent:41'),
(366, '2016-08-18 15:02:10', 1, '::1', 614, 1, 'hitrost:92|hitrostNapake:-94|procent:60'),
(367, '2016-08-18 15:22:17', 1, '::1', 614, 1, 'hitrost:28|hitrostNapake:-90|procent:15'),
(368, '2016-08-18 15:27:31', 1, '::1', 614, 1, 'hitrost:297|hitrostNapake:-498|procent:46'),
(369, '2016-08-18 15:27:57', 1, '::1', 616, 1, 'hitrost:192|hitrostNapake:-525|procent:25'),
(370, '2016-08-18 15:36:59', 1, '::1', 616, 1, 'hitrost:264|hitrostNapake:-949|procent:8'),
(371, '2016-08-18 15:37:23', 1, '::1', 615, 1, 'hitrost:143|hitrostNapake:-327|procent:34'),
(372, '2016-08-18 15:44:04', 1, '::1', 616, 1, 'hitrost:34|hitrostNapake:-91|procent:26'),
(373, '2016-08-18 15:48:28', 1, '::1', 614, 1, 'hitrost:339|hitrostNapake:-741|procent:36'),
(374, '2016-08-18 15:49:11', 1, '::1', 616, 1, 'hitrost:238|hitrostNapake:-506|procent:37'),
(375, '2016-08-18 15:52:49', 1, '::1', 615, 1, 'hitrost:108|hitrostNapake:-312|procent:22'),
(376, '2016-08-18 15:55:31', 1, '::1', 615, 1, 'hitrost:80|hitrostNapake:-142|procent:44'),
(377, '2016-08-18 15:56:11', 1, '::1', 615, 1, 'hitrost:270|hitrostNapake:-575|procent:37'),
(378, '2016-08-18 16:00:20', 1, '::1', 615, 1, 'hitrost:264|hitrostNapake:-403|procent:49'),
(379, '2016-08-18 16:03:21', 1, '::1', 615, 1, 'hitrost:290|hitrostNapake:-515|procent:44'),
(380, '2016-08-19 08:59:50', 1, '::1', 614, 1, 'hitrost:56|hitrostNapake:-144|procent:28'),
(381, '2016-08-19 09:02:26', 1, '::1', 614, 1, 'hitrost:73|hitrostNapake:-148|procent:39'),
(382, '2016-08-19 09:31:37', 1, '::1', 614, 1, 'hitrost:6|hitrostNapake:3|procent:92'),
(383, '2016-08-19 09:32:18', 1, '::1', 614, 5, 'hitrost:38|hitrostNapake:29|procent:95'),
(384, '2016-08-19 11:47:47', 1, '::1', 625, 1, 'hitrost:36|hitrostNapake:-61|procent:45'),
(385, '2016-08-19 12:46:02', 1, '::1', 615, 4, 'hitrost:27|hitrostNapake:22|procent:96'),
(386, '2016-08-19 12:55:54', 1, '::1', 615, 1, 'hitrost:242|hitrostNapake:-664|procent:25'),
(387, '2016-08-19 13:10:03', 1, '::1', 615, 1, 'hitrost:158|hitrostNapake:-242|procent:49'),
(388, '2016-08-19 13:31:21', 1, '::1', 615, 1, 'hitrost:56|hitrostNapake:-113|procent:39'),
(389, '2016-08-19 13:37:18', 1, '::1', 615, 1, 'hitrost:83|hitrostNapake:-143|procent:45'),
(390, '2016-08-22 14:29:49', 1, '::1', 615, 1, 'hitrost:212|hitrostNapake:-838|procent:1'),
(391, '2016-08-22 15:10:17', 1, '::1', 517, 3, ''),
(392, '2016-08-22 15:41:55', 1, '::1', 569, 2, 'Testno shranjevanje'),
(393, '2016-08-22 15:41:58', 1, '::1', 569, 2, 'Testno shranjevanje'),
(394, '2016-08-22 15:43:03', 1, '::1', 569, 3, 'Testno shranjevanje'),
(395, '2016-08-22 15:50:03', 1, '::1', 569, 4, '4 | 20 | 1 | ž ž c c c c j o r '),
(396, '2016-08-22 15:51:43', 1, '::1', 569, 2, 'Pravilnih: 2 | Porabljen cas: 25 | Stevilo poskusov: 1 | Pogoste napake: f f f e e e z j i č ž p p '),
(397, '2016-08-23 13:31:01', 1, '::1', 656, 2, 'Pravilnih: 2 | Porabljen cas: 12 | Stevilo poskusov: 1 | Pogoste napake: d d b b e e e e e e e e e e c c '),
(398, '2016-08-23 13:31:01', 1, '::1', 656, 2, 'Pravilnih: 2 | Porabljen cas: 13 | Stevilo poskusov: 1 | Pogoste napake: d d b b e e e e e e e e e e c c c '),
(399, '2016-08-23 13:32:12', 1, '::1', 569, 3, 'Pravilnih: 3 | Porabljen cas: 24 | Stevilo poskusov: 1 | Pogoste napake: e e e e e o j i i z č u g p '),
(400, '2016-08-23 13:42:50', 1, '::1', 653, 0, 'Pravilnih: 0 | Porabljen cas: 4 | Stevilo poskusov: 1 | Pogoste napake: g g c c c c k k k k k k '),
(401, '2016-08-23 13:42:51', 1, '::1', 653, 0, 'Pravilnih: 0 | Porabljen cas: 4 | Stevilo poskusov: 1 | Pogoste napake: g g c c c c k k k k k k g g g g g g g '),
(402, '2016-08-24 14:45:26', 1, '::1', 517, 2, ''),
(403, '2016-08-25 09:17:51', 1, '::1', 517, 4, ''),
(404, '2016-08-25 09:27:07', 1, '::1', 519, 3, ''),
(405, '2016-08-25 10:36:42', 1, '::1', 517, 1, ''),
(406, '2016-08-25 10:48:22', 1, '::1', 520, 3, ''),
(407, '2016-08-25 10:49:39', 1, '::1', 522, 1, ''),
(408, '2016-08-25 10:50:17', 1, '::1', 522, 1, ''),
(409, '2016-08-25 10:50:28', 1, '::1', 522, 1, ''),
(410, '2016-08-25 10:51:30', 1, '::1', 520, 2, ''),
(411, '2016-08-25 11:35:57', 1, '::1', 527, 2, ''),
(412, '2016-08-25 11:37:29', 1, '::1', 530, 1, ''),
(413, '2016-08-25 11:40:40', 1, '::1', 541, 1, ''),
(414, '2016-08-25 11:41:59', 1, '::1', 535, 1, ''),
(415, '2016-08-25 11:42:22', 1, '::1', 543, 1, ''),
(416, '2016-08-25 11:42:40', 1, '::1', 541, 1, ''),
(417, '2016-08-25 12:00:22', 1, '::1', 519, 1, ''),
(418, '2016-08-25 12:02:25', 1, '::1', 519, 1, ''),
(419, '2016-08-25 12:15:33', 1, '::1', 522, 3, ''),
(420, '2016-08-25 12:15:47', 1, '::1', 522, 1, ''),
(421, '2016-08-25 12:25:28', 1, '::1', 527, 5, ''),
(422, '2016-08-25 12:26:56', 1, '::1', 535, 1, ''),
(423, '2016-08-25 12:48:04', 1, '::1', 535, 1, ''),
(424, '2016-08-25 13:02:43', 1, '::1', 525, 2, ''),
(425, '2016-08-25 13:23:26', 1, '::1', 616, 1, 'hitrost:105|hitrostNapake:-389|procent:6'),
(426, '2016-08-25 13:27:40', 1, '::1', 616, 1, 'hitrost:258|hitrostNapake:-1033|procent:0'),
(427, '2016-08-25 13:29:37', 1, '::1', 616, 1, 'hitrost:258|hitrostNapake:-1033|procent:0'),
(428, '2016-08-25 13:30:10', 1, '::1', 616, 1, 'hitrost:264|hitrostNapake:-1029|procent:2'),
(429, '2016-08-25 13:36:04', 1, '::1', 616, 1, 'hitrost:258|hitrostNapake:-1033|procent:0'),
(430, '2016-08-25 13:41:30', 1, '::1', 616, 1, 'hitrost:258|hitrostNapake:-1033|procent:0');

-- --------------------------------------------------------

--
-- Table structure for table `zs_session`
--

CREATE TABLE `zs_session` (
  `session_id` varchar(255) NOT NULL,
  `session_user_id` int(11) DEFAULT NULL,
  `session_user_ip` varchar(45) DEFAULT NULL,
  `session_datetime` datetime DEFAULT NULL,
  `session_user_agent` varchar(255) DEFAULT NULL,
  `session_last_access` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_session`
--

INSERT INTO `zs_session` (`session_id`, `session_user_id`, `session_user_ip`, `session_datetime`, `session_user_agent`, `session_last_access`) VALUES
('1779756', 1, '::1', '2016-08-25 13:13:11', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-25 13:13:11'),
('6200084', 1, '::1', '2016-08-03 11:10:08', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0', '2016-08-03 11:10:08'),
('1083357', 1, '::1', '2016-08-16 14:16:42', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-16 14:16:42'),
('7630596', 1, '::1', '2016-08-16 14:33:40', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-16 14:33:40'),
('6910968', 1, '::1', '2016-08-17 09:35:46', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-17 09:35:46'),
('8552292', 1, '::1', '2016-08-18 08:59:17', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-18 08:59:17'),
('7968197', 1, '::1', '2016-08-18 09:21:08', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-18 09:21:08'),
('1028028', 1, '::1', '2016-08-18 09:29:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36 OPR/39.0.2256.48', '2016-08-18 09:29:00'),
('9305796', 1, '::1', '2016-08-18 10:13:45', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-18 10:13:45'),
('8017531', 1, '::1', '2016-08-18 15:18:57', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-18 15:18:57'),
('1753875', 1, '::1', '2016-08-18 15:19:50', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-18 15:19:50'),
('7902243', 1, '::1', '2016-08-23 12:36:05', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36', '2016-08-23 12:36:05'),
('6549899', 1, '::1', '2016-08-23 13:02:03', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-23 13:02:03'),
('6940767', 1, '::1', '2016-08-23 13:38:07', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', '2016-08-23 13:38:07'),
('8821070', 1, '::1', '2017-01-22 16:52:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2017-01-22 16:52:00'),
('7950608', 1, '::1', '2017-02-22 18:39:51', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '2017-02-22 18:39:51'),
('7521370', 1, '::1', '2017-03-01 23:24:51', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '2017-03-01 23:24:51'),
('1898206', 1, '::1', '2017-03-01 23:31:03', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '2017-03-01 23:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `zs_student`
--

CREATE TABLE `zs_student` (
  `student_id` int(11) NOT NULL,
  `student_user_id` int(11) DEFAULT NULL,
  `student_class_id` int(11) DEFAULT NULL,
  `student_enroll_datetime` datetime DEFAULT NULL,
  `student_role` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_student`
--

INSERT INTO `zs_student` (`student_id`, `student_user_id`, `student_class_id`, `student_enroll_datetime`, `student_role`) VALUES
(107, 1, 33, '2016-08-23 11:24:39', 2);

-- --------------------------------------------------------

--
-- Table structure for table `zs_user`
--

CREATE TABLE `zs_user` (
  `user_id` int(11) NOT NULL,
  `user_ip` varchar(45) DEFAULT NULL,
  `user_datetime` datetime DEFAULT NULL,
  `user_username` varchar(45) DEFAULT NULL,
  `user_password_hash` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_surname` varchar(255) DEFAULT NULL,
  `user_role` int(11) DEFAULT NULL,
  `user_language` varchar(45) DEFAULT NULL,
  `user_styles` varchar(255) DEFAULT NULL,
  `user_deleted` int(11) DEFAULT NULL COMMENT '0 if not deleted; 1 if deleted'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_user`
--

INSERT INTO `zs_user` (`user_id`, `user_ip`, `user_datetime`, `user_username`, `user_password_hash`, `user_name`, `user_surname`, `user_role`, `user_language`, `user_styles`, `user_deleted`) VALUES
(1, '::1', '2015-10-09 11:39:07', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'Admin', 2, 'SL', 'default', 0),
(68, '::1', '2017-02-23 18:34:23', 'ziga', 'afd0f4c7056b59541ac92c64fb75d78b', 'Test', 'Test', 1, 'SL', 'default', 0),
(67, '::1', '2017-01-22 16:43:29', 'ucenec', '21232f297a57a5a743894a0e4a801fc3', 'Ucenec', 'Ucenec', 1, 'SL', 'default', 0),
(69, '::1', '2017-02-23 18:35:15', 'abc', '900150983cd24fb0d6963f7d28e17f72', 'abc', 'abc', 1, 'SL', 'default', 0),
(70, '::1', '2017-02-23 18:38:56', 'qqq', 'b2ca678b4c936f905fb82f2733f5297f', 'qqq', 'qqq', 1, 'SL', 'default', 0);

-- --------------------------------------------------------

--
-- Table structure for table `zs_variable`
--

CREATE TABLE `zs_variable` (
  `variable_id` int(11) NOT NULL,
  `variable_instance_id` int(11) DEFAULT NULL,
  `variable_game_id` int(11) DEFAULT NULL,
  `variable_name` varchar(45) DEFAULT NULL,
  `variable_type` varchar(45) DEFAULT NULL,
  `variable_value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zs_variable`
--

INSERT INTO `zs_variable` (`variable_id`, `variable_instance_id`, `variable_game_id`, `variable_name`, `variable_type`, `variable_value`) VALUES
(3546, 517, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih zapomni. Med novimi slikami izberi prej videne v pravem zaporedju.'),
(3547, 517, 0, 'selectN', 'int', '2'),
(3548, 517, 0, 'groupN', 'int', '4'),
(3549, 517, 0, 'starLossOnFail', 'int', '1'),
(3550, 517, 0, 'progressToContinue', 'int', '3'),
(3551, 517, 0, 'files', 'filelist', 'files/slike/orodje/barvno/copic.svg|files/slike/orodje/barvno/copic01.svg|files/slike/orodje/barvno/izvijac.svg|files/slike/orodje/barvno/izvijac01.svg|files/slike/orodje/barvno/izvijac02.svg|files/slike/orodje/barvno/kladivo.svg|files/slike/orodje/barvno/kljuc.svg|files/slike/orodje/barvno/kljuc01.svg|files/slike/orodje/barvno/kljuc02.svg|files/slike/orodje/barvno/lestev.svg|files/slike/orodje/barvno/matica.svg|files/slike/orodje/barvno/ravnilo.svg|files/slike/orodje/barvno/ravnilo01.svg|files/slike/orodje/barvno/valjcek.svg|files/slike/orodje/barvno/zaga.svg|files/slike/orodje/barvno/zebelj.svg|files/slike/orodje/barvno/zebelj01.svg|files/slike/kuhinja/barvno/detergent.svg|files/slike/kuhinja/barvno/prasek.svg|files/slike/kuhinja/barvno/skleda.svg|files/slike/kuhinja/barvno/spuzva.svg|files/slike/kuhinja/barvno/vedro.svg|files/slike/kopalnica/barvno/skarje.svg'),
(3558, 519, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih dobro oglej. Med novimi slikami izberi samo nove.'),
(3559, 519, 0, 'selectN', 'int', '2'),
(3560, 519, 0, 'groupN', 'int', '4'),
(3561, 519, 0, 'starLossOnFail', 'int', '1'),
(3562, 519, 0, 'progressToContinue', 'int', '3'),
(3563, 519, 0, 'files', 'filelist', 'files/slike/kuhinja/barvno/cajnik.svg|files/slike/kuhinja/barvno/cajnik01.svg|files/slike/kuhinja/barvno/cajnik02.svg|files/slike/kuhinja/barvno/cedilo.svg|files/slike/kuhinja/barvno/deska.svg|files/slike/kuhinja/barvno/deska01.svg|files/slike/kuhinja/barvno/kapa.svg|files/slike/kuhinja/barvno/kos.svg|files/slike/kuhinja/barvno/kozarec (1).svg|files/slike/kuhinja/barvno/kozarec (2).svg|files/slike/kuhinja/barvno/kozarec (3).svg|files/slike/kuhinja/barvno/kozarec (4).svg|files/slike/kuhinja/barvno/kozarec (5).svg|files/slike/kuhinja/barvno/kozarec (6).svg|files/slike/kuhinja/barvno/kozarec01.svg|files/slike/kuhinja/barvno/kozarec02.svg|files/slike/kuhinja/barvno/kozarec03.svg|files/slike/kuhinja/barvno/kroznik.svg|files/slike/kuhinja/barvno/krozniki.svg|files/slike/kuhinja/barvno/kuhalnica.svg|files/slike/kuhinja/barvno/metlica (1).svg|files/slike/kuhinja/barvno/metlica (2).svg|files/slike/kuhinja/barvno/metlica (3).svg|files/slike/kuhinja/barvno/metlica (4).svg|files/slike/kuhinja/barvno/metlica (5).svg|files/slike/kuhinja/barvno/metlica (6).svg|files/slike/kuhinja/barvno/noz.svg|files/slike/kuhinja/barvno/noz01.svg|files/slike/kuhinja/barvno/noz02.svg|files/slike/kuhinja/barvno/obracalka.svg|files/slike/kuhinja/barvno/obracalka01.svg|files/slike/kuhinja/barvno/pladen.svg|files/slike/kuhinja/barvno/ponev.svg|files/slike/kuhinja/barvno/ponev01.svg|files/slike/kuhinja/barvno/posoda.svg|files/slike/kuhinja/barvno/posoda01.svg|files/slike/kuhinja/barvno/posoda02.svg|files/slike/kuhinja/barvno/posoda03.svg|files/slike/kuhinja/barvno/valjar.svg|files/slike/kuhinja/barvno/vrc.svg|files/slike/kuhinja/barvno/vrc01.svg'),
(3564, 520, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih zapomni. Med novimi slikami poišči manjkajoče.'),
(3565, 520, 0, 'selectN', 'int', '2'),
(3566, 520, 0, 'reduceN', 'int', '1'),
(3567, 520, 0, 'groupN', 'int', '4'),
(3568, 520, 0, 'starLossOnFail', 'int', '1'),
(3569, 520, 0, 'progressToContinue', 'int', '3'),
(3570, 520, 0, 'files', 'filelist', 'files/slike/garderoba/barvno/bluza (1).svg|files/slike/garderoba/barvno/bluza (2).svg|files/slike/garderoba/barvno/bluza (3).svg|files/slike/garderoba/barvno/bluza (4).svg|files/slike/garderoba/barvno/bluza (5).svg|files/slike/garderoba/barvno/bluza (6).svg|files/slike/garderoba/barvno/deznik (1).svg|files/slike/garderoba/barvno/deznik (2).svg|files/slike/garderoba/barvno/deznik (3).svg|files/slike/garderoba/barvno/deznik (4).svg|files/slike/garderoba/barvno/deznik (5).svg|files/slike/garderoba/barvno/deznik (6).svg|files/slike/garderoba/barvno/dolge_hlace.svg|files/slike/garderoba/barvno/gleznar.svg|files/slike/garderoba/barvno/gumb (1).svg|files/slike/garderoba/barvno/gumb (2).svg|files/slike/garderoba/barvno/gumb (3).svg|files/slike/garderoba/barvno/gumb (4).svg|files/slike/garderoba/barvno/gumb (5).svg|files/slike/garderoba/barvno/gumb (6).svg|files/slike/garderoba/barvno/jopica.svg|files/slike/garderoba/barvno/kapa (1).svg|files/slike/garderoba/barvno/kapa (2).svg|files/slike/garderoba/barvno/kapa (3).svg|files/slike/garderoba/barvno/kapa (4).svg|files/slike/garderoba/barvno/kapa (5).svg|files/slike/garderoba/barvno/kapa (6).svg|files/slike/garderoba/barvno/kopalke.svg|files/slike/garderoba/barvno/kratke_hlace.svg|files/slike/garderoba/barvno/krilo.svg|files/slike/garderoba/barvno/majica.svg|files/slike/garderoba/barvno/majica_rokav.svg|files/slike/garderoba/barvno/natikaci.svg|files/slike/garderoba/barvno/nogavice.svg|files/slike/garderoba/barvno/nogavice01.svg|files/slike/garderoba/barvno/obesalnik.svg|files/slike/garderoba/barvno/obleka (1).svg|files/slike/garderoba/barvno/obleka (2).svg|files/slike/garderoba/barvno/obleka (3).svg|files/slike/garderoba/barvno/obleka (4).svg|files/slike/garderoba/barvno/obleka (5).svg|files/slike/garderoba/barvno/pas.svg|files/slike/garderoba/barvno/rokavice.svg|files/slike/garderoba/barvno/sal.svg|files/slike/garderoba/barvno/salonar.svg|files/slike/garderoba/barvno/spodnja_majica.svg|files/slike/garderoba/barvno/spodnjice.svg'),
(3585, 522, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih zapomni. Med množico slik izberi tiste, ki si jih prej videl. '),
(3586, 522, 0, 'selectN', 'int', '2'),
(3587, 522, 0, 'groupN', 'int', '4'),
(3588, 522, 0, 'starLossOnFail', 'int', '1'),
(3589, 522, 0, 'progressToContinue', 'int', '3'),
(3590, 522, 0, 'files', 'filelist', 'files/slike/hrana/barvno/banana.svg|files/slike/hrana/barvno/bonbon.svg|files/slike/hrana/barvno/brokoli.svg|files/slike/hrana/barvno/cebula.svg|files/slike/hrana/barvno/cesen.svg|files/slike/hrana/barvno/cesnja (1).svg|files/slike/hrana/barvno/cesnja (2).svg|files/slike/hrana/barvno/cesnja (3).svg|files/slike/hrana/barvno/cesnja (4).svg|files/slike/hrana/barvno/cesnja (5).svg|files/slike/hrana/barvno/cesnja (6).svg|files/slike/hrana/barvno/cili.svg|files/slike/hrana/barvno/fizol.svg|files/slike/hrana/barvno/fizolcki.svg|files/slike/hrana/barvno/goba.svg|files/slike/hrana/barvno/grozdje (1).svg|files/slike/hrana/barvno/grozdje (2).svg|files/slike/hrana/barvno/grozdje (3).svg|files/slike/hrana/barvno/grozdje (4).svg|files/slike/hrana/barvno/grozdje (5).svg|files/slike/hrana/barvno/grozdje (6).svg|files/slike/hrana/barvno/hlebec.svg|files/slike/hrana/barvno/hruska.svg|files/slike/hrana/barvno/jabolko.svg|files/slike/hrana/barvno/jagoda.svg|files/slike/hrana/barvno/jajcevec.svg|files/slike/hrana/barvno/korenje (1).svg|files/slike/hrana/barvno/korenje (2).svg|files/slike/hrana/barvno/korenje (3).svg|files/slike/hrana/barvno/korenje (4).svg|files/slike/hrana/barvno/korenje (5).svg|files/slike/hrana/barvno/korenje (6).svg|files/slike/hrana/barvno/lubenica.svg|files/slike/hrana/barvno/marelica.svg|files/slike/hrana/barvno/paprika.svg|files/slike/hrana/barvno/paradiznik.svg|files/slike/hrana/barvno/sladoled.svg|files/slike/hrana/barvno/sladoled01.svg|files/slike/hrana/barvno/sliva.svg|files/slike/hrana/barvno/toast.svg'),
(3599, 525, 0, 'instructions', 'string', 'Kopalniška zavesa ima luknjo. Katera slika jo zakrpa?'),
(3600, 525, 0, 'selectN', 'int', '9'),
(3601, 525, 0, 'files', 'filelist', 'files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (1).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (2).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (3).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (4).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (5).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (6).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (7).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (8).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (9).svg|files/manjkajoci del/vzorec/barvno/ladja_jadrnica/ladja_jadrnica (10).svg'),
(3602, 525, 0, 'masks', 'filelist', 'files/manjkajoci del/izrez/krog.svg|files/manjkajoci del/izrez/kvadrat.svg|files/manjkajoci del/izrez/osemkotnik.svg'),
(3617, 527, 0, 'instructions', 'string', ' Na sliki poišči skrite predmete. Predmeti, ki jih moraš najti, so v kvadratkih na desni.'),
(3618, 527, 0, 'layer1', 'filelist', 'files/skriti predmeti/scena/kuhinja/barvno/kuhinja_zid.svg'),
(3619, 527, 0, 'layer2', 'filelist', 'files/skriti predmeti/scena/kuhinja/barvno/kuhinja_ozadje.svg'),
(3620, 527, 0, 'layer3', 'filelist', ''),
(3621, 527, 0, 'layer4', 'filelist', ''),
(3622, 527, 0, 'pivot', 'filelist', 'files/upload/30/14/pivot (1).svg'),
(3623, 527, 0, 'layer6', 'filelist', 'files/skriti predmeti/scena/kuhinja/barvno/kuhinja_ospredje.svg'),
(3624, 527, 0, 'elements1', 'filelist', 'files/slike/hrana/barvno/cesnja (1).svg|files/slike/hrana/barvno/cesnja (2).svg|files/slike/hrana/barvno/cesnja (3).svg|files/slike/hrana/barvno/cesnja (4).svg|files/slike/hrana/barvno/cesnja (5).svg|files/slike/hrana/barvno/cesnja (6).svg|files/slike/hrana/barvno/grozdje (1).svg|files/slike/hrana/barvno/grozdje (2).svg|files/slike/hrana/barvno/grozdje (3).svg|files/slike/hrana/barvno/grozdje (4).svg|files/slike/hrana/barvno/grozdje (5).svg|files/slike/hrana/barvno/grozdje (6).svg|files/slike/hrana/barvno/hruska.svg|files/slike/hrana/barvno/jabolko.svg|files/slike/hrana/barvno/sliva.svg|files/slike/hrana/barvno/marelica.svg|files/slike/prevoz/barvno/balon (1).svg|files/slike/prevoz/barvno/balon (2).svg|files/slike/prevoz/barvno/balon (3).svg|files/slike/prevoz/barvno/balon (4).svg|files/slike/prevoz/barvno/balon (5).svg|files/slike/prevoz/barvno/balon (6).svg|files/slike/zivali/barvno/cebela.svg|files/slike/zivali/barvno/metulj (1).svg|files/slike/zivali/barvno/metulj (2).svg|files/slike/zivali/barvno/metulj (3).svg|files/slike/zivali/barvno/metulj (4).svg|files/slike/zivali/barvno/metulj (5).svg|files/slike/zivali/barvno/metulj (6).svg'),
(3625, 527, 0, 'elements2', 'filelist', 'files/slike/prevoz/barvno/balon (1).svg|files/slike/prevoz/barvno/balon (2).svg|files/slike/prevoz/barvno/balon (3).svg|files/slike/prevoz/barvno/balon (4).svg|files/slike/prevoz/barvno/balon (5).svg|files/slike/prevoz/barvno/balon (6).svg|files/slike/zivali/barvno/cebela.svg|files/slike/zivali/barvno/metulj (1).svg|files/slike/zivali/barvno/metulj (2).svg|files/slike/zivali/barvno/metulj (3).svg|files/slike/zivali/barvno/metulj (4).svg|files/slike/zivali/barvno/metulj (5).svg|files/slike/zivali/barvno/metulj (6).svg'),
(3626, 527, 0, 'elements3', 'filelist', 'files/slike/rastline/barvno/bukev.svg|files/slike/rastline/barvno/detelja.svg|files/slike/rastline/barvno/hrast.svg|files/slike/rastline/barvno/iglavec.svg|files/slike/rastline/barvno/javor.svg|files/slike/rastline/barvno/lipa.svg|files/slike/rastline/barvno/listavec.svg|files/slike/rastline/barvno/marjetica (1).svg|files/slike/rastline/barvno/marjetica (2).svg|files/slike/rastline/barvno/marjetica (3).svg|files/slike/rastline/barvno/marjetica (4).svg|files/slike/rastline/barvno/marjetica (5).svg|files/slike/rastline/barvno/marjetica (6).svg|files/slike/rastline/barvno/tulipan.svg|files/slike/rastline/barvno/zvoncek.svg|files/slike/sport/barvno/kosarka.svg|files/slike/sport/barvno/odbojka.svg|files/slike/sport/barvno/tenis01.svg|files/slike/zivali/barvno/cebela.svg|files/slike/zivali/barvno/hrosc.svg|files/slike/zivali/barvno/jez.svg|files/slike/zivali/barvno/pajek.svg|files/slike/zivali/barvno/raca.svg|files/slike/zivali/barvno/raca01.svg'),
(3627, 527, 0, 'elements4', 'filelist', 'files/slike/rastline/barvno/bukev.svg|files/slike/rastline/barvno/detelja.svg|files/slike/rastline/barvno/hrast.svg|files/slike/rastline/barvno/iglavec.svg|files/slike/rastline/barvno/javor.svg|files/slike/rastline/barvno/lipa.svg|files/slike/rastline/barvno/listavec.svg|files/slike/rastline/barvno/marjetica (1).svg|files/slike/rastline/barvno/marjetica (2).svg|files/slike/rastline/barvno/marjetica (3).svg|files/slike/rastline/barvno/marjetica (4).svg|files/slike/rastline/barvno/marjetica (5).svg|files/slike/rastline/barvno/marjetica (6).svg|files/slike/rastline/barvno/tulipan.svg|files/slike/rastline/barvno/zvoncek.svg|files/slike/sport/barvno/kosarka.svg|files/slike/sport/barvno/odbojka.svg|files/slike/sport/barvno/tenis01.svg|files/slike/zivali/barvno/cebela.svg|files/slike/zivali/barvno/hrosc.svg|files/slike/zivali/barvno/jez.svg|files/slike/zivali/barvno/pajek.svg|files/slike/zivali/barvno/raca.svg|files/slike/zivali/barvno/raca01.svg|files/slike/zivali/barvno/macka.svg|files/slike/zivali/barvno/medved.svg|files/slike/zivali/barvno/lev.svg|files/slike/zivali/barvno/mis.svg|files/slike/zivali/barvno/ovca.svg|files/slike/zivali/barvno/pujs.svg|files/slike/zivali/barvno/sova.svg|files/slike/zivali/barvno/tiger.svg|files/slike/zivali/barvno/zaba (1).svg|files/slike/zivali/barvno/zaba (2).svg|files/slike/zivali/barvno/zaba (3).svg|files/slike/zivali/barvno/zaba (4).svg|files/slike/zivali/barvno/zaba (5).svg|files/slike/zivali/barvno/zaba (6).svg|files/slike/zivali/barvno/zajec.svg'),
(3628, 527, 0, 'elements5', 'filelist', 'files/slike/zivali/barvno/riba (1).svg|files/slike/zivali/barvno/riba (2).svg|files/slike/zivali/barvno/riba (3).svg|files/slike/zivali/barvno/riba (4).svg|files/slike/zivali/barvno/riba (5).svg|files/slike/zivali/barvno/riba (6).svg|files/slike/prevoz/barvno/jadrnica (1).svg|files/slike/prevoz/barvno/jadrnica (2).svg|files/slike/prevoz/barvno/jadrnica (3).svg|files/slike/prevoz/barvno/jadrnica (4).svg|files/slike/prevoz/barvno/jadrnica (5).svg|files/slike/prevoz/barvno/jadrnica (6).svg|files/slike/prevoz/barvno/ladja.svg|files/slike/prevoz/barvno/podmornica.svg'),
(3629, 527, 0, 'elements6', 'filelist', 'files/slike/hrana/barvno/banana.svg|files/slike/hrana/barvno/bonbon.svg|files/slike/hrana/barvno/brokoli.svg|files/slike/hrana/barvno/cebula.svg|files/slike/hrana/barvno/cesen.svg|files/slike/hrana/barvno/cesnja (1).svg|files/slike/hrana/barvno/cesnja (2).svg|files/slike/hrana/barvno/cesnja (3).svg|files/slike/hrana/barvno/cesnja (4).svg|files/slike/hrana/barvno/cesnja (5).svg|files/slike/hrana/barvno/cesnja (6).svg|files/slike/hrana/barvno/cili.svg|files/slike/hrana/barvno/fizol.svg|files/slike/hrana/barvno/fizolcki.svg|files/slike/hrana/barvno/goba.svg|files/slike/hrana/barvno/grozdje (1).svg|files/slike/hrana/barvno/grozdje (2).svg|files/slike/hrana/barvno/grozdje (3).svg|files/slike/hrana/barvno/grozdje (4).svg|files/slike/hrana/barvno/grozdje (5).svg|files/slike/hrana/barvno/grozdje (6).svg|files/slike/hrana/barvno/hlebec.svg|files/slike/hrana/barvno/hruska.svg|files/slike/hrana/barvno/jabolko.svg|files/slike/hrana/barvno/jagoda.svg|files/slike/hrana/barvno/jajce.svg|files/slike/hrana/barvno/jajcevec.svg|files/slike/hrana/barvno/kivi.svg|files/slike/hrana/barvno/korenje (1).svg|files/slike/hrana/barvno/korenje (2).svg|files/slike/hrana/barvno/korenje (3).svg|files/slike/hrana/barvno/korenje (4).svg|files/slike/hrana/barvno/korenje (5).svg|files/slike/hrana/barvno/korenje (6).svg|files/slike/hrana/barvno/limona.svg|files/slike/hrana/barvno/lubenica.svg|files/slike/hrana/barvno/marelica.svg|files/slike/hrana/barvno/meso.svg|files/slike/hrana/barvno/paprika.svg|files/slike/hrana/barvno/paradiznik.svg|files/slike/hrana/barvno/pomaranca.svg|files/slike/hrana/barvno/repa.svg|files/slike/hrana/barvno/sladoled.svg|files/slike/hrana/barvno/sladoled01.svg|files/slike/hrana/barvno/sliva.svg|files/slike/hrana/barvno/toast.svg|files/slike/kuhinja/barvno/cajnik.svg|files/slike/kuhinja/barvno/cajnik01.svg|files/slike/kuhinja/barvno/cajnik02.svg|files/slike/kuhinja/barvno/cedilo.svg|files/slike/kuhinja/barvno/deska.svg|files/slike/kuhinja/barvno/deska01.svg|files/slike/kuhinja/barvno/detergent.svg|files/slike/kuhinja/barvno/kozarec (1).svg|files/slike/kuhinja/barvno/kozarec (2).svg|files/slike/kuhinja/barvno/kozarec (3).svg|files/slike/kuhinja/barvno/kozarec (4).svg|files/slike/kuhinja/barvno/kozarec (5).svg|files/slike/kuhinja/barvno/kozarec (6).svg|files/slike/kuhinja/barvno/kozarec01.svg|files/slike/kuhinja/barvno/kozarec02.svg|files/slike/kuhinja/barvno/kozarec03.svg|files/slike/kuhinja/barvno/kroznik.svg|files/slike/kuhinja/barvno/krozniki.svg|files/slike/kuhinja/barvno/kuhalnica.svg|files/slike/kuhinja/barvno/metlica (1).svg|files/slike/kuhinja/barvno/metlica (2).svg|files/slike/kuhinja/barvno/metlica (3).svg|files/slike/kuhinja/barvno/metlica (6).svg|files/slike/kuhinja/barvno/pladen.svg|files/slike/kuhinja/barvno/ponev.svg|files/slike/kuhinja/barvno/ponev01.svg|files/slike/kuhinja/barvno/poper.svg|files/slike/kuhinja/barvno/posoda.svg|files/slike/kuhinja/barvno/posoda01.svg|files/slike/kuhinja/barvno/posoda02.svg|files/slike/kuhinja/barvno/posoda03.svg|files/slike/kuhinja/barvno/posoda04.svg|files/slike/kuhinja/barvno/posoda05.svg|files/slike/kuhinja/barvno/skleda.svg|files/slike/kuhinja/barvno/skleda01.svg|files/slike/kuhinja/barvno/skleda02.svg|files/slike/kuhinja/barvno/skodelica.svg|files/slike/kuhinja/barvno/skodelica01.svg|files/slike/kuhinja/barvno/skodelica02.svg|files/slike/kuhinja/barvno/sol.svg|files/slike/kuhinja/barvno/steklenica.svg|files/slike/kuhinja/barvno/steklenica01.svg|files/slike/kuhinja/barvno/steklenica02.svg|files/slike/kuhinja/barvno/vaza.svg|files/slike/kuhinja/barvno/vaza01.svg|files/slike/kuhinja/barvno/vaza02.svg|files/slike/kuhinja/barvno/vaza03.svg|files/slike/kuhinja/barvno/vrc.svg|files/slike/kuhinja/barvno/vrc01.svg'),
(3630, 527, 0, 'elements7', 'filelist', 'files/slike/kuhinja/barvno/kuhalnica.svg|files/slike/kuhinja/barvno/metlica (1).svg|files/slike/kuhinja/barvno/metlica (2).svg|files/slike/kuhinja/barvno/metlica (3).svg|files/slike/kuhinja/barvno/metlica (6).svg|files/slike/kuhinja/barvno/noz.svg|files/slike/kuhinja/barvno/noz01.svg|files/slike/kuhinja/barvno/noz02.svg|files/slike/kuhinja/barvno/obracalka.svg|files/slike/kuhinja/barvno/obracalka.svg|files/slike/kuhinja/barvno/vilice.svg|files/slike/kuhinja/barvno/zajemalka.svg|files/slike/kuhinja/barvno/zlica (2).svg|files/slike/kuhinja/barvno/zlica.svg|files/slike/kuhinja/barvno/zlica01.svg'),
(3649, 530, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih zapomni. Med množico slik izberi tiste, ki si jih prej videl.'),
(3650, 530, 0, 'selectN', 'int', '2'),
(3651, 530, 0, 'groupN', 'int', '4'),
(3652, 530, 0, 'starLossOnFail', 'int', '1'),
(3653, 530, 0, 'progressToContinue', 'int', '3'),
(3654, 530, 0, 'files', 'filelist', 'files/slike/zivali/linijsko/cebela.svg|files/slike/zivali/linijsko/hrosc.svg|files/slike/zivali/linijsko/jez.svg|files/slike/zivali/linijsko/lev.svg|files/slike/zivali/linijsko/macek.svg|files/slike/zivali/linijsko/medved.svg|files/slike/zivali/linijsko/metulj (1).svg|files/slike/zivali/linijsko/metulj (2).svg|files/slike/zivali/linijsko/metulj (3).svg|files/slike/zivali/linijsko/metulj (4).svg|files/slike/zivali/linijsko/metulj (5).svg|files/slike/zivali/linijsko/mis.svg|files/slike/zivali/linijsko/ovca.svg|files/slike/zivali/linijsko/pajek.svg|files/slike/zivali/linijsko/pujs.svg|files/slike/zivali/linijsko/raca.svg|files/slike/zivali/linijsko/raca01.svg|files/slike/zivali/linijsko/riba (1).svg|files/slike/zivali/linijsko/riba (2).svg|files/slike/zivali/linijsko/riba (3).svg|files/slike/zivali/linijsko/riba (4).svg|files/slike/zivali/linijsko/riba (5).svg|files/slike/zivali/linijsko/sova.svg|files/slike/zivali/linijsko/tiger.svg|files/slike/zivali/linijsko/zaba (1).svg|files/slike/zivali/linijsko/zaba (2).svg|files/slike/zivali/linijsko/zaba (3).svg|files/slike/zivali/linijsko/zaba (4).svg|files/slike/zivali/linijsko/zaba (5).svg|files/slike/zivali/linijsko/zajec.svg'),
(3680, 534, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih zapomni. Med novimi slikami izberi prej videne v pravem zaporedju.'),
(3681, 534, 0, 'selectN', 'int', '2'),
(3682, 534, 0, 'groupN', 'int', '4'),
(3683, 534, 0, 'starLossOnFail', 'int', '1'),
(3684, 534, 0, 'progressToContinue', 'int', '3'),
(3685, 534, 0, 'files', 'filelist', 'files/slike/geometrija/sivinsko/krog (1).svg|files/slike/geometrija/sivinsko/krog (2).svg|files/slike/geometrija/sivinsko/krog (3).svg|files/slike/geometrija/sivinsko/krog (4).svg|files/slike/geometrija/sivinsko/krog (5).svg|files/slike/geometrija/sivinsko/krog (6).svg|files/slike/geometrija/sivinsko/krog (7).svg|files/slike/geometrija/sivinsko/krog (8).svg|files/slike/geometrija/sivinsko/krog.svg|files/slike/geometrija/sivinsko/kvadrat (1).svg|files/slike/geometrija/sivinsko/kvadrat (2).svg|files/slike/geometrija/sivinsko/kvadrat (3).svg|files/slike/geometrija/sivinsko/kvadrat (4).svg|files/slike/geometrija/sivinsko/kvadrat (5).svg|files/slike/geometrija/sivinsko/kvadrat (6).svg|files/slike/geometrija/sivinsko/kvadrat (7).svg|files/slike/geometrija/sivinsko/kvadrat.svg|files/slike/geometrija/sivinsko/petkotnik (1).svg|files/slike/geometrija/sivinsko/petkotnik (2).svg|files/slike/geometrija/sivinsko/petkotnik (3).svg|files/slike/geometrija/sivinsko/petkotnik (4).svg|files/slike/geometrija/sivinsko/petkotnik (5).svg|files/slike/geometrija/sivinsko/petkotnik (6).svg|files/slike/geometrija/sivinsko/petkotnik (7).svg|files/slike/geometrija/sivinsko/petkotnik.svg|files/slike/geometrija/sivinsko/sestkotnik (1).svg|files/slike/geometrija/sivinsko/sestkotnik (2).svg|files/slike/geometrija/sivinsko/sestkotnik (3).svg|files/slike/geometrija/sivinsko/sestkotnik (4).svg|files/slike/geometrija/sivinsko/sestkotnik (5).svg|files/slike/geometrija/sivinsko/sestkotnik (6).svg|files/slike/geometrija/sivinsko/sestkotnik (7).svg|files/slike/geometrija/sivinsko/sestkotnik.svg|files/slike/geometrija/sivinsko/trikotnik (1).svg|files/slike/geometrija/sivinsko/trikotnik (2).svg|files/slike/geometrija/sivinsko/trikotnik (3).svg|files/slike/geometrija/sivinsko/trikotnik (4).svg|files/slike/geometrija/sivinsko/trikotnik (5).svg|files/slike/geometrija/sivinsko/trikotnik (6).svg|files/slike/geometrija/sivinsko/trikotnik (7).svg|files/slike/geometrija/sivinsko/trikotnik.svg'),
(3686, 535, 0, 'instructions', 'string', 'Sestavi sestavljanko.'),
(3687, 535, 0, 'files', 'filelist', 'files/sestavi me/motiv/barvno/kuhar.svg|files/sestavi me/motiv/barvno/jez.svg|files/sestavi me/motiv/barvno/morje.svg|files/sestavi me/motiv/barvno/pingvin.svg|files/sestavi me/motiv/barvno/potapljac.svg|files/sestavi me/motiv/barvno/polje.svg|files/sestavi me/motiv/barvno/luna.svg|files/sestavi me/motiv/barvno/sova.svg|files/sestavi me/motiv/barvno/raca.svg|files/sestavi me/motiv/barvno/riba.svg|files/sestavi me/motiv/barvno/ucenec.svg|files/sestavi me/motiv/barvno/tenis.svg'),
(3688, 535, 0, 'puzzles0', 'filelist', 'files/sestavi me/razrez/puzli_9/puzli_9 (1).svg|files/sestavi me/razrez/puzli_9/puzli_9 (2).svg|files/sestavi me/razrez/puzli_9/puzli_9 (3).svg|files/sestavi me/razrez/puzli_9/puzli_9 (4).svg|files/sestavi me/razrez/puzli_9/puzli_9 (5).svg|files/sestavi me/razrez/puzli_9/puzli_9 (6).svg|files/sestavi me/razrez/puzli_9/puzli_9 (7).svg|files/sestavi me/razrez/puzli_9/puzli_9 (8).svg|files/sestavi me/razrez/puzli_9/puzli_9 (9).svg'),
(3689, 535, 0, 'puzzles1', 'filelist', 'files/sestavi me/razrez/puzli01_9/puzli01_9 (1).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (2).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (3).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (4).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (5).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (6).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (7).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (8).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (9).svg'),
(3690, 535, 0, 'puzzles2', 'filelist', 'files/sestavi me/razrez/kvadrat_9/kvadrat_9 (1).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (2).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (3).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (4).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (5).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (6).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (7).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (8).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (9).svg'),
(3691, 535, 0, 'puzzles3', 'filelist', ''),
(3692, 535, 0, 'puzzles4', 'filelist', ''),
(3699, 537, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih dobro oglej. Med novimi slikami izberi samo nove.'),
(3700, 537, 0, 'selectN', 'int', '2'),
(3701, 537, 0, 'groupN', 'int', '4'),
(3702, 537, 0, 'starLossOnFail', 'int', '1'),
(3703, 537, 0, 'progressToContinue', 'int', '3'),
(3704, 537, 0, 'files', 'filelist', 'files/slike/garderoba/temno_ozadje/bluza (1).svg|files/slike/garderoba/temno_ozadje/bluza (2).svg|files/slike/garderoba/temno_ozadje/bluza (3).svg|files/slike/garderoba/temno_ozadje/bluza (4).svg|files/slike/garderoba/temno_ozadje/bluza (5).svg|files/slike/garderoba/temno_ozadje/bluza (6).svg|files/slike/garderoba/temno_ozadje/deznik (1).svg|files/slike/garderoba/temno_ozadje/deznik (2).svg|files/slike/garderoba/temno_ozadje/deznik (3).svg|files/slike/garderoba/temno_ozadje/deznik (4).svg|files/slike/garderoba/temno_ozadje/deznik (5).svg|files/slike/garderoba/temno_ozadje/deznik.svg|files/slike/garderoba/temno_ozadje/gleznar.svg|files/slike/garderoba/temno_ozadje/gumb (1).svg|files/slike/garderoba/temno_ozadje/gumb (2).svg|files/slike/garderoba/temno_ozadje/gumb (3).svg|files/slike/garderoba/temno_ozadje/gumb (4).svg|files/slike/garderoba/temno_ozadje/gumb (5).svg|files/slike/garderoba/temno_ozadje/gumb.svg|files/slike/garderoba/temno_ozadje/hlace.svg|files/slike/garderoba/temno_ozadje/hlace_kratke.svg|files/slike/garderoba/temno_ozadje/kapa (1).svg|files/slike/garderoba/temno_ozadje/kapa (2).svg|files/slike/garderoba/temno_ozadje/kapa (3).svg|files/slike/garderoba/temno_ozadje/kapa (4).svg|files/slike/garderoba/temno_ozadje/kapa (5).svg|files/slike/garderoba/temno_ozadje/kapa.svg|files/slike/garderoba/temno_ozadje/kopalke.svg|files/slike/garderoba/temno_ozadje/kopica.svg|files/slike/garderoba/temno_ozadje/krilo.svg|files/slike/garderoba/temno_ozadje/majica.svg|files/slike/garderoba/temno_ozadje/majica_rokav.svg|files/slike/garderoba/temno_ozadje/natikac.svg|files/slike/garderoba/temno_ozadje/nogavica.svg|files/slike/garderoba/temno_ozadje/nogavica01.svg|files/slike/garderoba/temno_ozadje/obesalnik.svg|files/slike/garderoba/temno_ozadje/obleka (1).svg|files/slike/garderoba/temno_ozadje/obleka (2).svg|files/slike/garderoba/temno_ozadje/obleka (3).svg|files/slike/garderoba/temno_ozadje/obleka (4).svg|files/slike/garderoba/temno_ozadje/obleka (5).svg|files/slike/garderoba/temno_ozadje/obleka.svg|files/slike/garderoba/temno_ozadje/pas.svg|files/slike/garderoba/temno_ozadje/rokavica.svg|files/slike/garderoba/temno_ozadje/sal.svg|files/slike/garderoba/temno_ozadje/salonar.svg|files/slike/garderoba/temno_ozadje/spodja_majica.svg|files/slike/garderoba/temno_ozadje/spodnjice.svg'),
(3716, 540, 0, 'instructions', 'string', 'Sestavi sestavljanko.'),
(3717, 540, 0, 'files', 'filelist', 'files/sestavi me/motiv/linijsko/jez.svg|files/sestavi me/motiv/linijsko/kuhar.svg|files/sestavi me/motiv/linijsko/luna.svg|files/sestavi me/motiv/linijsko/morje.svg|files/sestavi me/motiv/linijsko/potapljac.svg|files/sestavi me/motiv/linijsko/raca.svg|files/sestavi me/motiv/linijsko/riba.svg|files/sestavi me/motiv/linijsko/sova.svg|files/sestavi me/motiv/linijsko/tenis.svg|files/sestavi me/motiv/linijsko/ucenec.svg'),
(3718, 540, 0, 'puzzles0', 'filelist', 'files/sestavi me/razrez/puzli_9/puzli_9 (1).svg|files/sestavi me/razrez/puzli_9/puzli_9 (2).svg|files/sestavi me/razrez/puzli_9/puzli_9 (3).svg|files/sestavi me/razrez/puzli_9/puzli_9 (4).svg|files/sestavi me/razrez/puzli_9/puzli_9 (5).svg|files/sestavi me/razrez/puzli_9/puzli_9 (6).svg|files/sestavi me/razrez/puzli_9/puzli_9 (7).svg|files/sestavi me/razrez/puzli_9/puzli_9 (8).svg|files/sestavi me/razrez/puzli_9/puzli_9 (9).svg'),
(3719, 540, 0, 'puzzles1', 'filelist', 'files/sestavi me/razrez/puzli01_9/puzli01_9 (1).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (2).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (3).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (4).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (5).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (6).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (7).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (8).svg|files/sestavi me/razrez/puzli01_9/puzli01_9 (9).svg'),
(3720, 540, 0, 'puzzles2', 'filelist', 'files/sestavi me/razrez/kvadrat_9/kvadrat_9 (1).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (2).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (3).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (4).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (5).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (6).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (7).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (8).svg|files/sestavi me/razrez/kvadrat_9/kvadrat_9 (9).svg'),
(3721, 540, 0, 'puzzles3', 'filelist', ''),
(3722, 540, 0, 'puzzles4', 'filelist', ''),
(3723, 541, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih zapomni. Med novimi slikami izberi prej videne v pravem zaporedju.'),
(3724, 541, 0, 'selectN', 'int', '2'),
(3725, 541, 0, 'groupN', 'int', '4'),
(3726, 541, 0, 'starLossOnFail', 'int', '1'),
(3727, 541, 0, 'progressToContinue', 'int', '3'),
(3728, 541, 0, 'files', 'filelist', 'files/slike/prazniki/sivinsko/balon.svg|files/slike/prazniki/sivinsko/baloncek.svg|files/slike/prazniki/sivinsko/baloni.svg|files/slike/prazniki/sivinsko/bodicevje.svg|files/slike/prazniki/sivinsko/buca.svg|files/slike/prazniki/sivinsko/darilo.svg|files/slike/prazniki/sivinsko/jelka.svg|files/slike/prazniki/sivinsko/kapa_bozic.svg|files/slike/prazniki/sivinsko/klobuk.svg|files/slike/prazniki/sivinsko/mafin.svg|files/slike/prazniki/sivinsko/netopir.svg|files/slike/prazniki/sivinsko/maska.svg|files/slike/prazniki/sivinsko/torta.svg|files/slike/prazniki/sivinsko/sveca.svg|files/slike/prazniki/sivinsko/srce.svg|files/slike/prazniki/sivinsko/snezinka.svg|files/slike/prazniki/sivinsko/snezak.svg|files/slike/prazniki/sivinsko/sladkorna_palica.svg|files/slike/prazniki/sivinsko/repatica.svg|files/slike/prazniki/sivinsko/pirh.svg|files/slike/prazniki/sivinsko/pajcevina.svg'),
(3729, 542, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih dobro oglej. Med novimi slikami izberi samo nove.'),
(3730, 542, 0, 'selectN', 'int', '2'),
(3731, 542, 0, 'groupN', 'int', '4'),
(3732, 542, 0, 'starLossOnFail', 'int', '1'),
(3733, 542, 0, 'progressToContinue', 'int', '3'),
(3734, 542, 0, 'files', 'filelist', 'files/slike/zivali/sivinsko/cebela.svg|files/slike/zivali/sivinsko/hrosc.svg|files/slike/zivali/sivinsko/jez.svg|files/slike/zivali/sivinsko/lev.svg|files/slike/zivali/sivinsko/macek.svg|files/slike/zivali/sivinsko/medved.svg|files/slike/zivali/sivinsko/metulj (1).svg|files/slike/zivali/sivinsko/metulj (2).svg|files/slike/zivali/sivinsko/metulj (3).svg|files/slike/zivali/sivinsko/metulj (4).svg|files/slike/zivali/sivinsko/metulj (5).svg|files/slike/zivali/sivinsko/metulj.svg|files/slike/zivali/sivinsko/zajec.svg|files/slike/zivali/sivinsko/zaba.svg|files/slike/zivali/sivinsko/zaba (5).svg|files/slike/zivali/sivinsko/zaba (4).svg|files/slike/zivali/sivinsko/zaba (3).svg|files/slike/zivali/sivinsko/zaba (2).svg|files/slike/zivali/sivinsko/zaba (1).svg|files/slike/zivali/sivinsko/tiger.svg|files/slike/zivali/sivinsko/sova.svg|files/slike/zivali/sivinsko/riba.svg|files/slike/zivali/sivinsko/riba (5).svg|files/slike/zivali/sivinsko/riba (4).svg|files/slike/zivali/sivinsko/riba (3).svg|files/slike/zivali/sivinsko/riba (2).svg|files/slike/zivali/sivinsko/riba (1).svg|files/slike/zivali/sivinsko/raca01.svg|files/slike/zivali/sivinsko/raca.svg|files/slike/zivali/sivinsko/pujs.svg|files/slike/zivali/sivinsko/pajek.svg|files/slike/zivali/sivinsko/ovca.svg|files/slike/zivali/sivinsko/mis.svg'),
(3735, 543, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih zapomni. Med novimi slikami poišči manjkajoče.'),
(3736, 543, 0, 'selectN', 'int', '2'),
(3737, 543, 0, 'reduceN', 'int', '1'),
(3738, 543, 0, 'groupN', 'int', '4'),
(3739, 543, 0, 'starLossOnFail', 'int', '1'),
(3740, 543, 0, 'progressToContinue', 'int', '3'),
(3741, 543, 0, 'files', 'filelist', 'files/slike/rastline/linijsko/bukev.svg|files/slike/rastline/linijsko/cvetlicni_lonec (1).svg|files/slike/rastline/linijsko/cvetlicni_lonec (2).svg|files/slike/rastline/linijsko/cvetlicni_lonec (3).svg|files/slike/rastline/linijsko/cvetlicni_lonec (4).svg|files/slike/rastline/linijsko/cvetlicni_lonec (5).svg|files/slike/rastline/linijsko/cvetlicni_lonec (6).svg|files/slike/rastline/linijsko/detelja.svg|files/slike/rastline/linijsko/hrast.svg|files/slike/rastline/linijsko/iglavec.svg|files/slike/rastline/linijsko/javor.svg|files/slike/rastline/linijsko/lipa.svg|files/slike/rastline/linijsko/zvoncek.svg|files/slike/rastline/linijsko/tulipan.svg|files/slike/rastline/linijsko/marjetica (5).svg|files/slike/rastline/linijsko/marjetica (4).svg|files/slike/rastline/linijsko/marjetica (3).svg|files/slike/rastline/linijsko/marjetica (2).svg|files/slike/rastline/linijsko/marjetica (1).svg|files/slike/rastline/linijsko/listavec.svg'),
(3742, 544, 0, 'instructions', 'string', 'Pojavile se bodo slike, ki si jih zapomni. Med množico slik izberi tiste, ki si jih prej videl. '),
(3743, 544, 0, 'selectN', 'int', '2'),
(3744, 544, 0, 'groupN', 'int', '4'),
(3745, 544, 0, 'starLossOnFail', 'int', '1'),
(3746, 544, 0, 'progressToContinue', 'int', '3'),
(3747, 544, 0, 'files', 'filelist', 'files/slike/prevoz/linijsko/avto.svg|files/slike/prevoz/linijsko/avtobus.svg|files/slike/prevoz/linijsko/balon (1).svg|files/slike/prevoz/linijsko/balon (2).svg|files/slike/prevoz/linijsko/balon (3).svg|files/slike/prevoz/linijsko/balon (4).svg|files/slike/prevoz/linijsko/jadrnica (1).svg|files/slike/prevoz/linijsko/jadrnica (2).svg|files/slike/prevoz/linijsko/jadrnica (3).svg|files/slike/prevoz/linijsko/jadrnica (4).svg|files/slike/prevoz/linijsko/jadrnica (5).svg|files/slike/prevoz/linijsko/ladja.svg|files/slike/prevoz/linijsko/vlak.svg|files/slike/prevoz/linijsko/tovornjak.svg|files/slike/prevoz/linijsko/stopinje.svg|files/slike/prevoz/linijsko/podmornica.svg'),
(3912, 569, 0, 'crke', 'stringlist', 'a|b|c|č|d|e|f|g|h|i|j|k|l|m|n|o|p|r|s|š|t|u|v|z|ž'),
(3913, 569, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4394, 614, 0, 'text', 'string', 'ffff jjjj ffjj jjff fjfj jfjf fjjf jffj fffj jjjf ffff jjjj'),
(4395, 614, 0, 'displayType', 'int', '1'),
(4396, 614, 0, 'lineLength', 'int', '20'),
(4397, 614, 0, 'lineLengthWhole', 'int', '53'),
(4398, 614, 0, 'markColor', 'string', '000000'),
(4399, 614, 0, 'fontColor', 'string', '223344'),
(4400, 614, 0, 'fontSize', 'int', '50'),
(4401, 614, 0, 'cFontColor', 'string', '000000'),
(4402, 614, 0, 'mFontColor', 'string', '000000'),
(4403, 614, 0, 'cBackColor', 'string', 'FFF277'),
(4404, 614, 0, 'mBackColor', 'string', 'FF7C78'),
(4405, 614, 0, 'backgroundColor', 'string', 'ffffff'),
(4406, 614, 0, 'nacin', 'int', '0'),
(4407, 615, 0, 'text', 'string', 'dddd kkkk ddkk kkdd dkdk kdkd dkkd kddk dddk kkkd'),
(4408, 615, 0, 'displayType', 'int', '1'),
(4409, 615, 0, 'lineLength', 'int', '20'),
(4410, 615, 0, 'lineLengthWhole', 'int', '53'),
(4411, 615, 0, 'markColor', 'string', '000000'),
(4412, 615, 0, 'fontColor', 'string', '223344'),
(4413, 615, 0, 'fontSize', 'int', '50'),
(4414, 615, 0, 'cFontColor', 'string', '000000'),
(4415, 615, 0, 'mFontColor', 'string', '000000'),
(4416, 615, 0, 'cBackColor', 'string', 'FFF277'),
(4417, 615, 0, 'mBackColor', 'string', 'FF7C78'),
(4418, 615, 0, 'backgroundColor', 'string', 'ffffff'),
(4419, 615, 0, 'nacin', 'int', '0'),
(4420, 616, 0, 'text', 'string', 'ačač aačč ačča čaač aaaa čččč fajč afčj'),
(4421, 616, 0, 'displayType', 'int', '1'),
(4422, 616, 0, 'lineLength', 'int', '20'),
(4423, 616, 0, 'lineLengthWhole', 'int', '53'),
(4424, 616, 0, 'markColor', 'string', '000000'),
(4425, 616, 0, 'fontColor', 'string', '223344'),
(4426, 616, 0, 'fontSize', 'int', '50'),
(4427, 616, 0, 'cFontColor', 'string', '000000'),
(4428, 616, 0, 'mFontColor', 'string', '000000'),
(4429, 616, 0, 'cBackColor', 'string', 'FFF277'),
(4430, 616, 0, 'mBackColor', 'string', 'FF7C78'),
(4431, 616, 0, 'backgroundColor', 'string', 'ffffff'),
(4432, 616, 0, 'nacin', 'int', '0'),
(4433, 617, 0, 'text', 'string', 'ssss llll ssll llss slls lssl fsjl asdf jklč fdsa člkj'),
(4434, 617, 0, 'displayType', 'int', '1'),
(4435, 617, 0, 'lineLength', 'int', '20'),
(4436, 617, 0, 'lineLengthWhole', 'int', '53'),
(4437, 617, 0, 'markColor', 'string', '000000'),
(4438, 617, 0, 'fontColor', 'string', '223344'),
(4439, 617, 0, 'fontSize', 'int', '50'),
(4440, 617, 0, 'cFontColor', 'string', '000000'),
(4441, 617, 0, 'mFontColor', 'string', '000000'),
(4442, 617, 0, 'cBackColor', 'string', 'FFF277'),
(4443, 617, 0, 'mBackColor', 'string', 'FF7C78'),
(4444, 617, 0, 'backgroundColor', 'string', 'ffffff'),
(4445, 617, 0, 'nacin', 'int', '0'),
(4446, 618, 0, 'text', 'string', 'as aja čaj čas daj dal kad kaj kal lak las saj ajda čaja čaka časa dala dasa jafa jaka jasa kača kaja slad čakal dlaka klada ladja lasal skala sklad čakala fasada lasala skakal sladak skakala sladkal safalada sladkala'),
(4447, 618, 0, 'displayType', 'int', '1'),
(4448, 618, 0, 'lineLength', 'int', '30'),
(4449, 618, 0, 'lineLengthWhole', 'int', '53'),
(4450, 618, 0, 'markColor', 'string', '000000'),
(4451, 618, 0, 'fontColor', 'string', '223344'),
(4452, 618, 0, 'fontSize', 'int', '50'),
(4453, 618, 0, 'cFontColor', 'string', '000000'),
(4454, 618, 0, 'mFontColor', 'string', '000000'),
(4455, 618, 0, 'cBackColor', 'string', 'FFF277'),
(4456, 618, 0, 'mBackColor', 'string', 'FF7C78'),
(4457, 618, 0, 'backgroundColor', 'string', 'ffffff'),
(4458, 618, 0, 'nacin', 'int', '0'),
(4459, 619, 0, 'text', 'string', 'ffjj fgfg ffgg ggff fggf gffg jjhh jhhh hjjj jhjh hjhj jhfg fghj jfhg dgdg khkh'),
(4460, 619, 0, 'displayType', 'int', '1'),
(4461, 619, 0, 'lineLength', 'int', '20'),
(4462, 619, 0, 'lineLengthWhole', 'int', '53'),
(4463, 619, 0, 'markColor', 'string', '000000'),
(4464, 619, 0, 'fontColor', 'string', '223344'),
(4465, 619, 0, 'fontSize', 'int', '50'),
(4466, 619, 0, 'cFontColor', 'string', '000000'),
(4467, 619, 0, 'mFontColor', 'string', '000000'),
(4468, 619, 0, 'cBackColor', 'string', 'FFF277'),
(4469, 619, 0, 'mBackColor', 'string', 'FF7C78'),
(4470, 619, 0, 'backgroundColor', 'string', 'ffffff'),
(4471, 619, 0, 'nacin', 'int', '0'),
(4472, 620, 0, 'text', 'string', 'aha dah gad gaj gas had haj hja lah alga gada gaja gasa glad glas haha hlad jaga jaha halja jahač jahal kahla lagal lahak lahka lahka gladka hahaha hlačah jahala lagala skalah'),
(4473, 620, 0, 'displayType', 'int', '1'),
(4474, 620, 0, 'lineLength', 'int', '30'),
(4475, 620, 0, 'lineLengthWhole', 'int', '53'),
(4476, 620, 0, 'markColor', 'string', '000000'),
(4477, 620, 0, 'fontColor', 'string', '223344'),
(4478, 620, 0, 'fontSize', 'int', '50'),
(4479, 620, 0, 'cFontColor', 'string', '000000'),
(4480, 620, 0, 'mFontColor', 'string', '000000'),
(4481, 620, 0, 'cBackColor', 'string', 'FFF277'),
(4482, 620, 0, 'mBackColor', 'string', 'FF7C78'),
(4483, 620, 0, 'backgroundColor', 'string', 'ffffff'),
(4484, 620, 0, 'nacin', 'int', '0'),
(4485, 621, 0, 'text', 'string', 'dede deed edde kiki kkii kiik ikki kkik ikkk kdei eiei iiee'),
(4486, 621, 0, 'displayType', 'int', '1'),
(4487, 621, 0, 'lineLength', 'int', '20'),
(4488, 621, 0, 'lineLengthWhole', 'int', '53'),
(4489, 621, 0, 'markColor', 'string', '000000'),
(4490, 621, 0, 'fontColor', 'string', '223344'),
(4491, 621, 0, 'fontSize', 'int', '50'),
(4492, 621, 0, 'cFontColor', 'string', '000000'),
(4493, 621, 0, 'mFontColor', 'string', '000000'),
(4494, 621, 0, 'cBackColor', 'string', 'FFF277'),
(4495, 621, 0, 'mBackColor', 'string', 'FF7C78'),
(4496, 621, 0, 'backgroundColor', 'string', 'ffffff'),
(4497, 621, 0, 'nacin', 'int', '0'),
(4498, 622, 0, 'text', 'string', 'ali ded del dih gre hči hej ida jed jej jel jih ker kje led lej les lik lil reč red res sel sij sil sle čase časi čehi čela česa črke daje dali dasi deda dela dele deli desk diha dlje glej grde greh hčer idej ječe ječi jedi jeli jera kače kadi kake kaki kjer klel lahi lase lesa lila lisa lise rade raje rase rdeč reče reda reka reke seči sede sedi sije sila sile sili sled slej slik časih česar daleč dalje deček dečka dejal dekla dekle delal delih dihal drage gleda glede grede greha grehe hčere hlače ideje iskal jedel jedla jelka kraje ladje laseh lasje legel legla rdeča rdeče redke rekel rekla sadje sedaj sedel sedla segal segel segla skale sreča sreče čakali dajali dejala delala delali gledal iskala iskali jajček kakega sedela sedeli silila sledil slekel srečal čedalje gledala gledale gledali sledila sledili'),
(4499, 622, 0, 'displayType', 'int', '1'),
(4500, 622, 0, 'lineLength', 'int', '40'),
(4501, 622, 0, 'lineLengthWhole', 'int', '53'),
(4502, 622, 0, 'markColor', 'string', '000000'),
(4503, 622, 0, 'fontColor', 'string', '223344'),
(4504, 622, 0, 'fontSize', 'int', '50'),
(4505, 622, 0, 'cFontColor', 'string', '000000'),
(4506, 622, 0, 'mFontColor', 'string', '000000'),
(4507, 622, 0, 'cBackColor', 'string', 'FFF277'),
(4508, 622, 0, 'mBackColor', 'string', 'FF7C78'),
(4509, 622, 0, 'backgroundColor', 'string', 'ffffff'),
(4510, 622, 0, 'nacin', 'int', '0'),
(4511, 623, 0, 'text', 'string', 'ffjj frfr ffrr rffr frrf fffr rrrf juju jjuu uuuj jjju fujr jfur'),
(4512, 623, 0, 'displayType', 'int', '1'),
(4513, 623, 0, 'lineLength', 'int', '20'),
(4514, 623, 0, 'lineLengthWhole', 'int', '53'),
(4515, 623, 0, 'markColor', 'string', '000000'),
(4516, 623, 0, 'fontColor', 'string', '223344'),
(4517, 623, 0, 'fontSize', 'int', '50'),
(4518, 623, 0, 'cFontColor', 'string', '000000'),
(4519, 623, 0, 'mFontColor', 'string', '000000'),
(4520, 623, 0, 'cBackColor', 'string', 'FFF277'),
(4521, 623, 0, 'mBackColor', 'string', 'FF7C78'),
(4522, 623, 0, 'backgroundColor', 'string', 'ffffff'),
(4523, 623, 0, 'nacin', 'int', '0'),
(4524, 624, 0, 'text', 'string', 'čar črk čul dar dir duh grd gre hud jud jug kar ker krč kri luč rad raj rak reč red rek res ris rus sir srd srh suh uči ude udi ura ure urh uri audi čaju času čelu čeri črka črke čuda čudi čuje čula čuli daru dedu delu dere diru drag drla drli drsi drug duha duhu duri erik fare fari gluh grad grda grde grdi gred greh grel grič grki grla grlu hčer huda hude hudi huje iger igra igre igri jera jeri juda jude judi juga jugu juha juhe juhi juri karl kjer kraj kras krči kres krik kril krki kruh kuga kuha kuje kure lasu ledu lesu lira luči luka luke luki rada rade radi raja raje raju rase raus rdeč reče reči reda redi redu reka reke reki rudi rusi sadu sira sluh srda suče suha suhe suhi učil udih udje uide ujel urad urah česar čigar čudil dečku dihur draga drage dragi druga druge drugi glasu gluha grada gradu grede greha grehe grehi griču gruča gruče hčere hčeri hudič hudih igral jadra jarek jarku ječar judje julka jurij kadar karel karla ključ kraja kraje kraji kraju kralj kriči kriki krila krili kruha kuhar ljudi radiu rahla rasel rasla rdeča rdeče rdeči rečeh redke redki rekel rekla rekli rjuhe sedlu skril sledu sluga sreča sreče sreči sredi suhih sukal učila učili udari udari ugaja ugled ujela ujeli ulrik afriki čudila čudili deluje drugič drugih drugje hčerka hudega hudiča hudiču igrala igrali julija jurija juriju kradel krajih kralja kralji kralju kričal kuhala ljudeh ljudje rdečih redkih sekira skalar skladu skrila slikar slučaj srečal suhega udaril ugleda uredil usedel'),
(4525, 624, 0, 'displayType', 'int', '1'),
(4526, 624, 0, 'lineLength', 'int', '40'),
(4527, 624, 0, 'lineLengthWhole', 'int', '53'),
(4528, 624, 0, 'markColor', 'string', '000000'),
(4529, 624, 0, 'fontColor', 'string', '223344'),
(4530, 624, 0, 'fontSize', 'int', '50'),
(4531, 624, 0, 'cFontColor', 'string', '000000'),
(4532, 624, 0, 'mFontColor', 'string', '000000'),
(4533, 624, 0, 'cBackColor', 'string', 'FFF277'),
(4534, 624, 0, 'mBackColor', 'string', 'FF7C78'),
(4535, 624, 0, 'backgroundColor', 'string', 'ffffff'),
(4536, 624, 0, 'nacin', 'int', '0'),
(4537, 625, 0, 'text', 'string', 'fvfv fvvv vffv fvfv jmjm jjmm jmmm mjmj jmjm frfv jujm fvfr jmju frjm jufv'),
(4538, 625, 0, 'displayType', 'int', '1'),
(4539, 625, 0, 'lineLength', 'int', '20'),
(4540, 625, 0, 'lineLengthWhole', 'int', '53'),
(4541, 625, 0, 'markColor', 'string', '000000'),
(4542, 625, 0, 'fontColor', 'string', '223344'),
(4543, 625, 0, 'fontSize', 'int', '50'),
(4544, 625, 0, 'cFontColor', 'string', '000000'),
(4545, 625, 0, 'mFontColor', 'string', '000000'),
(4546, 625, 0, 'cBackColor', 'string', 'FFF277'),
(4547, 625, 0, 'mBackColor', 'string', 'FF7C78'),
(4548, 625, 0, 'backgroundColor', 'string', 'ffffff'),
(4549, 625, 0, 'nacin', 'int', '0'),
(4550, 626, 0, 'text', 'string', 'čem čim črv dam dim drv dva dve eva grm ima ime jem jim kam lev mah maj mak mar meč med meh mej mir mrk muh muk rim sam sem siv sme sum sva uma vaj val vam vas vda več vej vek vem ves vid vil vir vrč vre vrh vrv vsa vse vsi čemu dama dame davi dima drva dveh film glav grem grma grmu hlev hram imam imel jame jami jima kava kave kriv krvi leva leve levi maja mala male mali mama mame mami mara meča meče meja meje meji mere meri mesa miha mila mili milj miru mlad mrak muči mudi muha muhe muke rame rami reva sama same sami seme siva sive sivi smeh smel smem smer sram umil umre umrl vaje vaju vama vame vami vase vasi vdal vede vedi veja veje veji veke veli vera vere veri vida vidi vije vila vile visi vlak vmes vrag vred vrha vrhu vrli vrvi vsaj vsak vseh vsej vsem čakam čemer čemur čimer čuvaj davek david delam divja divje divji durmi dvema dviga glava glave glavi greva hleva hlevu hvala hvali imava imela imele imeli javil jemal kakem kimal kmalu krava krave krčme krčmi kriva krivi krvav kumer lasmi levem maček mačka mahal malči malem malih malim maral maram marka marku mečem megla megle megli mehak mehka mehke mehki meril mesar midva mirka misel misli mlada mlade mladi mleka mraka mraku mučil rečem rjave samem samih samim sedem sivih slava slave smeha smehu smeje smela smeli smeri sramu suhem sveča sveče ujame umrje umrla umrli varuh vaseh včasi vdala večer večja večje večji vedel vejah velel velik velja vesel videč videl vidim vidva vihar vikar visel vlada vlade vlaka vlaku vleče vleči vrača vraga vreče vreči vreme vrgel vrgla vrgli vsaka vsake vsaki vsega vsemi vsemu vsled almira aramis armada čevlje čevlji črkami divjih dreves drevja drevje drugam drugem drugim dvigal glavah glavar gledam hvalil jemala jemali jemlje kmečki krajev krčmar krivde krivdi krvavi ljudem ljudmi ludvik malega marala marija marveč merila misleč mislih mislil mislim mladih mladim mrmral mučila rahlim rdečim samega samemu sedmih semkaj seveda severu sivimi skriva smejal smilil smisel smisla smislu smreke umikal umiril usmili varuje včasih včeraj večera večeru večjih vedela vedeli velela velika velike veliki verige verjel veruje vesela vesele veseli videla videle videli vidika visela visele viseli vladar vlekel vlekla vlekli vračal vsakem vsakič vsakih vsakim vselej'),
(4551, 626, 0, 'displayType', 'int', '1'),
(4552, 626, 0, 'lineLength', 'int', '40'),
(4553, 626, 0, 'lineLengthWhole', 'int', '53'),
(4554, 626, 0, 'markColor', 'string', '000000'),
(4555, 626, 0, 'fontColor', 'string', '223344'),
(4556, 626, 0, 'fontSize', 'int', '50'),
(4557, 626, 0, 'cFontColor', 'string', '000000'),
(4558, 626, 0, 'mFontColor', 'string', '000000'),
(4559, 626, 0, 'cBackColor', 'string', 'FFF277'),
(4560, 626, 0, 'mBackColor', 'string', 'FF7C78'),
(4561, 626, 0, 'backgroundColor', 'string', 'ffffff'),
(4562, 626, 0, 'nacin', 'int', '0'),
(4563, 627, 0, 'text', 'string', 'dcdc k,k, dccd k,,k cddc ,kk, dck, k,dc dedc kik, dcde k,ki'),
(4564, 627, 0, 'displayType', 'int', '1'),
(4565, 627, 0, 'lineLength', 'int', '20'),
(4566, 627, 0, 'lineLengthWhole', 'int', '53'),
(4567, 627, 0, 'markColor', 'string', '000000'),
(4568, 627, 0, 'fontColor', 'string', '223344'),
(4569, 627, 0, 'fontSize', 'int', '50'),
(4570, 627, 0, 'cFontColor', 'string', '000000'),
(4571, 627, 0, 'mFontColor', 'string', '000000'),
(4572, 627, 0, 'cBackColor', 'string', 'FFF277'),
(4573, 627, 0, 'mBackColor', 'string', 'FF7C78'),
(4574, 627, 0, 'backgroundColor', 'string', 'ffffff'),
(4575, 627, 0, 'nacin', 'int', '0'),
(4576, 628, 0, 'text', 'string', 'cel, cev, hec, lic, mac, muc, rac, src, vic, cela, cele, celi, cevi, cika, cilj, klic, lica, lice, lici, licu, luca, mica, mici, muca, raca, race, reci, rejc, srca, srce, srcu, ulic, vice, alica, alice, alici, celem, celic, celih, celim, celje, cesar, cilja, cilje, cilji, cilju, curek, curki, curku, dedca, dedci, dedec, drejc, jajca, jajce, jajci, jurca, klice, klici, licem, licih, malce, marca, mavca, mavec, mesec, micka, micki, mucka, samec, scela, sicer, sirec, srajc, srcem, srcih, ulica, ulice, ulici, vejic, akcije, cedila, cedilu, celega, celica, celice, celici, cerkev, cerkva, cerkve, cerkvi, ciljem, ciljev, cirkus, deklic, devica, device, drejca, drejcu, jamice, jelica, jerica, jerice, jerici, klical, krivic, levica, levici, lisica, lisice, lucija, lucije, luciji, mamica, marcel, merice, meseca, mesece, meseci, mesecu, milica, revica, slavec, srajca, srajce, srajci, udarca, udarce, udarci, udarcu, udarec, ulicah, vilice, vkrcal, cerkvah, cerkvam, cesarja, cesarju, deklica, deklice, deklici, delavca, delavce, delavci, delavec, glavica, glavice, idrijca, igralca, igralci, igralec, jaslice, jecljal, klicala, klicali, kljukec, krajcar, krivica, krivice, krivici, marcela, mavrica, meglica, meglice, mesecem, mesecev, mesecih, rdečica, rdečice, sklical, udarcem, udarcev, udarcih, cecilija, cerkvica, cerkvice, cerkvici, cesarjem, cesarjev, cesarska, cesarske, cesarski, curljala, delavcem, delavcev, drevesca, drevesce, gledalci, igralcev, jaslicah, kraljica, kraljice, kraljici, kuharica, kuharice, marelice, reakcija, reakcije, smrkavec, vejicami, veselica, veselice, vilicami,'),
(4577, 628, 0, 'displayType', 'int', '1'),
(4578, 628, 0, 'lineLength', 'int', '40'),
(4579, 628, 0, 'lineLengthWhole', 'int', '53');
INSERT INTO `zs_variable` (`variable_id`, `variable_instance_id`, `variable_game_id`, `variable_name`, `variable_type`, `variable_value`) VALUES
(4580, 628, 0, 'markColor', 'string', '000000'),
(4581, 628, 0, 'fontColor', 'string', '223344'),
(4582, 628, 0, 'fontSize', 'int', '50'),
(4583, 628, 0, 'cFontColor', 'string', '000000'),
(4584, 628, 0, 'mFontColor', 'string', '000000'),
(4585, 628, 0, 'cBackColor', 'string', 'FFF277'),
(4586, 628, 0, 'mBackColor', 'string', 'FF7C78'),
(4587, 628, 0, 'backgroundColor', 'string', 'ffffff'),
(4588, 628, 0, 'nacin', 'int', '0'),
(4589, 629, 0, 'text', 'string', 'lolo čpčp lool čppč ollo pččp ločp olpč olčp pčlo oplč lčop'),
(4590, 629, 0, 'displayType', 'int', '1'),
(4591, 629, 0, 'lineLength', 'int', '20'),
(4592, 629, 0, 'lineLengthWhole', 'int', '53'),
(4593, 629, 0, 'markColor', 'string', '000000'),
(4594, 629, 0, 'fontColor', 'string', '223344'),
(4595, 629, 0, 'fontSize', 'int', '50'),
(4596, 629, 0, 'cFontColor', 'string', '000000'),
(4597, 629, 0, 'mFontColor', 'string', '000000'),
(4598, 629, 0, 'cBackColor', 'string', 'FFF277'),
(4599, 629, 0, 'mBackColor', 'string', 'FF7C78'),
(4600, 629, 0, 'backgroundColor', 'string', 'ffffff'),
(4601, 629, 0, 'nacin', 'int', '0'),
(4602, 630, 0, 'text', 'string', 'ako čip čoh dol dom ego evo god gol gor hip hoj hop joj jok kap kdo kip koč kod koj kol kom kos kup lep log lok lov moč moj mom oče oči oho oko osi pač pak par pas peč ped pek pel pes pij pik pil pod pok pol pri psa pse psi psu pul rep rod rog rok rop rov slo smo sod sok sol spi uho upa uro vod vol vso cape celo čelo čepi čipe čipi čipu črko čudo culo čulo dado dajo dalo damo delo doda dolg doli dolu doma domu drog dvom dvor figo ford godi gola gole goli golo gora gore gori goro gosi grdo grlo grof grom halo hipi hipu hlod hoče hoda hodi hoja hoje hoji hojo holm hrup hudo iglo igro jako jamo jaso ječo jedo jelo jemo jero joče joka joku juho kačo kako kaos kape kapi kapo kavo kdor kepe kilo kipa kipi klop koča koče koči kock kočo kode kodo koga koli kolo komu koru kosa kose kosi koso kosu krmo krog krov krpe krpo kupa kupe kupi kupo kupu lego lepa lepe lepi lepo levo lilo lipa lipe lipo liso loči loki loku lomi lope lopi lopo lord lova lovi lovu luko malo mamo mejo mero meso mico milo mimo miro moči moda modi moja moje moji mojo moka moke moko moli molk mora more mrko muho muko očeh očem očka očmi oder odra odru ogla oglu ojoj okus oleg olga olge olgi olgo olja olje opis oprl oral oreh orel orje orla orlu osel osem osip osla osle oslo osmi osmo ovac ovca ovce ovco ovil ovir pada pade palm papa para pare pari park paro pasa pase pasu peče peči pege pela pele peli pelo pepa pepi pero piha pije pika pike piki piko pila pili pipa pipe pipi pipo piva pivo plah ples plev plod plug plus poči poda podi podu poje pojo polh polj polo pome pouk pove prag prah prav preč pred prej prek prev prič prid proč prod prsa prsi prva prve prvi prvo psom psov rado rajo ramo reko repa repe repi repo rodi rodu rogu roji roka roke roki roko roma rosa rose rosi rovu samo sapa sape sapi sapo savo sero shod silo sivo skok slap slep slog sloj soča soče soči soda sodi soha soka soli sova sovi sovo spal spil spim spod spol spor suho svoj udov umor upal upam upor upov upre uprl uspe uvod vedo vejo vemo vero vhod vido vilo vkup voda vode vodi vodo vola vole voli volk vpil vroč alico ampak časom časov čelom čepel čipov dečko deklo delom delov divjo dodal dokaj dolga dolge dolgi dolgo domov drago drevo drugo duhom duhov dvoje dvoma dvora dvoru filip glavo godec gorah gorje gorju gorko gospa gospe gospo govor grapo gredo gremo grofa grofu haljo hipov hočem hodil hodim hrupa hvalo idejo imajo imamo imelo jedjo jokal kakor kamor kdove kislo klopi koder kogar komaj komur korak kosom kovač kramp kravo krčmo krilo krogu krovu krvjo kupil ladjo lahko lepem lepih ločil lorda lovci lovec lovil lovro mačko major maori marko maslo meglo mehko micko mihor mirko mlado mleko močeh močjo močmi model moder modre modri modro mogel mogla mogle mogli moglo mojem mojih mojim mokra mokre mokri mokro molče molči molil molku moral moram morda morem morja morje morju očala očesa odeje odejo odhod odide odkar odkod odmev odpor odpre odpri odprl odpro odveč ogled okoli okrog okvir omare omari omaro osmih ovire padal padel padla padle padli padlo palec papir parku pasel pasom pavel pavla pavle pečjo pekel pekla pelje pepel pesek pesem peska pesku pesmi pihal pipec pisal pisar pisem pisma pismo pismu plača plaho plava plesu počel podal podol podrl pogum pojde pojdi pojem pojma poleg polja polje polju pomoč popil posel posla posle posli poslu povej povem povrh praga pragi prago pragu prahu prava prave pravi pravo preko priča priče priči pričo prida pride pridi prime prior proda progo prosi prsih prvem prvič prvih prvim radio rahlo rdečo redko reklo rekoč repom rjuho rodil rokah rokav romal sedlo skalo sklep skoči skoro slamo slavo slepi slepo sliko slovo smejo smelo smemo sodil sodim sosed spada spala spali sploh sprva srečo sredo srepo svečo svoja svoje svoji svojo upala upali upira uprla uspeh uspel vpije vpili vpliv aprila čakajo čepico čeprav človek dajejo darilo delajo deloma docela dodala dogaja dokler dolgem dolgih dolgim domača domače domači domačo domala doseči doslej dospel dovolj drugod duhovi evropi evropo gladko glasom glasov godilo gospod govore govori hipoma hlapca hlapci hlapcu hlapec hočejo hočemo hodijo hodila hodili jokala kajpak kaplja kaplje kljuko klopeh kočija kočijo kolegi kolesa koliko koraka korake koraki kosilo kosilu krepak krepko krogla krvavo kupček kupila kupili lepega levico lovrek mahoma mogoče mojega mojemu molčal molila molili morajo morala morale morali moralo moramo morava morejo moremo mudilo očesom odhodu odkril odpira odpora odprla odprle odprli odprlo odslej oglasi ograjo okolje okolju okviru opravi opreme opremo orodje osupel osuplo padajo padala padale padali palače palcem palica palice palico peljal perilo pijača pijače pijači pijačo pijejo pisala pisali pisalo pisavo plačal plaval plemič plesal pljuča počaka počasi počela počeli počilo počiva podala pogled poglej pogoje poguma pojavi pojdem poklic pokril pokrov police polici polico poljih pomaga pomiri pomlad pomoči poprej poreče poroke poroki poroko poskus poslal poslej posode posodo povrhu povsem povsod praksi pravdo pravem pravih pravil pravim precej predal predse prehod prejel prepad prepir prerok preveč pričel pridem prihod prijel primer privid proces prodal prosil prosim prvega prvemu recimo rodila rokama rokami sekiro sijalo skočil skoraj skupaj sladko slepar smehom soseda splača spodaj spopad spravi spričo srajco svojem svojih svojim upiral uspeha uspelo valovi veliko veselo videlo vidijo vidimo visoka visoke visoki visoko vleklo vodijo vodila vojaka vojake vojaki vojska vojske vojski vojsko vpliva vpričo vsakdo mikroprocesor operacijskega priporočljivo pripovedovala pripovedovale pripovedovali pripovedujejo spremljevalca spremljevalci spremljevalec pripovedovalec spremljevalcem'),
(4603, 630, 0, 'displayType', 'int', '1'),
(4604, 630, 0, 'lineLength', 'int', '40'),
(4605, 630, 0, 'lineLengthWhole', 'int', '53'),
(4606, 630, 0, 'markColor', 'string', '000000'),
(4607, 630, 0, 'fontColor', 'string', '223344'),
(4608, 630, 0, 'fontSize', 'int', '50'),
(4609, 630, 0, 'cFontColor', 'string', '000000'),
(4610, 630, 0, 'mFontColor', 'string', '000000'),
(4611, 630, 0, 'cBackColor', 'string', 'FFF277'),
(4612, 630, 0, 'mBackColor', 'string', 'FF7C78'),
(4613, 630, 0, 'backgroundColor', 'string', 'ffffff'),
(4614, 630, 0, 'nacin', 'int', '0'),
(4615, 631, 0, 'text', 'string', 'fvfb fbfb frfb jmjn jnjn jujn fbjn fbfr jnju bnfj rbun fbbf jnnj'),
(4616, 631, 0, 'displayType', 'int', '1'),
(4617, 631, 0, 'lineLength', 'int', '20'),
(4618, 631, 0, 'lineLengthWhole', 'int', '53'),
(4619, 631, 0, 'markColor', 'string', '000000'),
(4620, 631, 0, 'fontColor', 'string', '223344'),
(4621, 631, 0, 'fontSize', 'int', '50'),
(4622, 631, 0, 'cFontColor', 'string', '000000'),
(4623, 631, 0, 'mFontColor', 'string', '000000'),
(4624, 631, 0, 'cBackColor', 'string', 'FFF277'),
(4625, 631, 0, 'mBackColor', 'string', 'FF7C78'),
(4626, 631, 0, 'backgroundColor', 'string', 'ffffff'),
(4627, 631, 0, 'nacin', 'int', '0'),
(4628, 632, 0, 'text', 'string', 'ana ane ani ano bab bal bas beg bel bes bič bik bil bob bog boj bok bom bos brk bum čin črn dan dna dne dni dno dnu dob don ena ene eni eno gib gin grb gub jan krn nad nag naj nak nam nas nek nem nič nje nji njo noč nog nor nos nov oba obe ona one oni ono pob ran rib rob san sen sin sna snu sob van ven vrb alba amen anca apna apno baba babe babo baje bala bale bali balo barv beda bega begu bela bele beli belo bere beri besa biča biči bife bije bika bila bile bili bilo bios biva blag bled blok bobi boca bode bodi bodo boga bogu boja boje boji bojo boju boke boki boku boli bolj bomb bomo bore bori bosa bosi bova bral bran brda brdo brdu breg bril brke brki brvi buča buče buči bučo buda cena cene ceni ceno član čoln črna črne črni črno cunj dana dani dano dene dina dlan dneh dnem doba dobe dobi dobo doni eden egon enak enem enih enim erna fani fino fran gane giba gibi gina gnal gnoj gnus goba gobe gobo goni grob guba gube gumb hans hrib imen iran ivan jana john kino kinu klin klub konj kron lani lena leni leno lina line lini lino ljub luna lune luni luno mana mane manj mano mene meni mine mlin nace naju nama name nami nanj nase nauk neba nebo nebu neče neda nego neha neka neke neki neko nema neme nemi nemo nese nima nini nisi niso njej njem njen njih njim njiv njun noče noči noga noge nogi nogo nora nore nori noro nosa nosi nosu nova nove novi novo nudi nuna nune obed obeh obok obrv obul obup oken okna okni okno oknu ondi onem onih onim oseb ovnu pena pene peni plan plen plin poba pobu poln ponj rabi rabo rana rane rani rano riba ribe ribi ribo roba robe robo robu sabo sani sanj sebe sebi sena senc seno senu sina sine sinu skrb slab slon sneg snel snob snop snov soba sobe sobi sobo srbi srna sune ubil ubog učen urno vabi vanj vdan venc vina vino vinu vnel vnuk vonj vran vrbe vrbo vrne vrni alena aleni aleno ančka angel anica anici anico baron barva barve barvo belem belih belim berač besed besno blaga blago bleda blisk bodem bogom bogov bogve bojim bojni bolan bolje bolna bolni bolno boris brado brala brani bregu breme briga bruno burja burno člani črnem črnih črnim čuden čudna čudne čudni čudno cunje danes davno debel denar desni desno dlani dneva dneve dnevi dnevu dober dobil dobim dobra dobre dobri dobro dunaj edina edini edino enaka enake enaki enako enega enemu evgen franc ganil gnali groba grobu hrana hrane hrani hrano hriba hribe hribu hugon imena imenu ivana jakob jasen jasna jasno javno jesen junak kajne kamen kamna kamne kamni kamnu kljub knjig kolen konca konča koncu konec konja konje konji konju krpan lačen lačni lahno ljuba ljubi ljubo lonec menda menih menil menim menoj milan minil minka miren mirna mirne mirni mirno mnoge mnogi mnogo močan močna močne močni močno način naglo nagne najde najel najsi nakar nalil namen nanje nanjo napad napak napil napis napol narod nebes nebom nečem nehaj nehal nekaj nekak nekam nekdo nekem nekim nekje nekoč nekod nemci nemir nesel nesla nihče nikar nimam nisem nismo nisva njega njemu njena njene njeni njeno njiju njima njimi njive njivi njivo njuna njune njuni njuno noben nočem nočjo nočni nocoj nogah nogam nosil nosom novem novih novim nujno obali obema obilo obisk objel oblak oboje obrne obrvi odnos ogenj ognja ognju oknih oknom onega onemu oseba osebe pijan plane počne polna polne polni polno pomen ponos račun ramen raven ravna ravni ravno redno resen resno robec ročno rojen roman sanja sanje seboj senca sence senci senco silno simon sinom skrbi slaba slabe slabi slabo slino sloni snega snegu sodbo sodni sonca sonce soncu srčno sunil ubili uboga ubogi ubogo vajen vanje vanjo varno vdano večna večni večno vedno vidno vinom vnemo vojna vojne vojni vojno vrnem vrnil andrej angela angeli babica babice babico baliju balkon baraba barake barako barica barona baroni baronu barvah bedaka begale belega belemu belila belimi belina beline belino bencin berača berače berači beraču berejo beremo berlin beseda besede besedi besedo besnel bičali billom biriča biriči biseri blagne blagom blagor bledem bledih bledim bliska bliski blodil bobija bobiju bobnov bodemo bodeva bodimo bodisi bodiva bodoče bodoči bogove bogovi boječa boječe bojijo bojimo bojnih bojuje boleče bolela bolelo bolijo bolnik borijo borila borili borisa borisu bosimi bralec branil branja branje branju brcnil brdavs bregom bridka bridke bridki bridko brigal brisal brlela brnela brskal bruhal buljil čebele črnega črnimi debela debele debeli debelo dlanjo dnevno dobijo dobila dobili dobilo dobimo dobiva dobrem dobrih dobrim dobrin dolina doline dolini dolino droban drobce drobci drobec droben drobil drobna drobne drobni drobno dunaju dvigne dvoboj dvojbe france giblje glasba glasbe glasbi glasbo glasno glavna glavni glavno globel globin globok goloba grabil greben grenko grobar grobom grobov gubice helena hladna hladno hlebec hlebov hodnik hrabri hrabro hribih hribom hribov hrupno imenom indiji ivanka jagnje jakoba jakobu jambor jankec jerbas jernej jeseni kabine kabini kabino kaplan klancu klobas klobuk knjiga knjige knjigi knjigo kobilo kolena koleni koleno končal koncem koncev končno krasno krenil labodi ledeno lenoba lenobo lesena lesene leseni leseno ljubil ljubim ljubka ljubko london luknje luknjo mahnil majcen majhen majhna majhne majhni majhno malone manjka mednje menihi menila menili minila minilo mnenja mnenje mnenju mnogih mornar mračno nabere nabira nabral načina načine nadenj nadvse nagiba naglas nagnil nahaja najdem največ naloga naloge nalogo namene namreč napake naprej narava narave naravi naravo naredi narobe naroda narodi naslov nasmeh naučil navada navade navadi navado nebesa nečesa nekako nekdaj nekega nekoga nemara neumen neumna neumno nikdar nikjer nikoli nimajo nimamo njegov njenem njenih njenim njihov njunih nobena nobene nobeni nobeno nočejo nogami norček nosijo nosila nosili novega novemu novica novice novico obadva občina obdaja obdana obenem obesil običaj obilja obilju obilna obilne obilno obiral obiska obiske obiski obisku objame objela objema objemu oblači oblaka oblake oblaki oblaku obleče obleči obleka obleke obleki obleko oblika oblike obliki obliko oblila obljub obnese obokom obrača obreda obrede obrise obrisi obrnem obrnil obrvmi obudil obupal obupan obupno obupom odbija odbije odbila odbilo odboru odvrne ogabno ogibal ognjem ohrani okence okrene omembe omenil onadva onkraj osebje osebna osebne osebni osebno oskrbi osorno ovinku palubi pesnik pijani pisano plamen planil plovbe plovbo pobere poberi pobili pobira pobral pobudo poceni podboj podoba podobe podobi podobo pogani pognal pogreb pogubi pogubo poljub polona pomena pomeni ponoči ponosa ponovi popoln povabi prebil preden predno pribil pribor priden pridno rabelj rabijo rabila rabili ramena rameni ranjen ravnal rečeno resnik ribami ribiči ribnik robček robcem robove robovi rosana rumena rumeni rumeno sabina sablja sablje sabljo sanjah sanjal sekund semenj simbol sinoči sinove sinovi skloni skrbeh skrbel skrbjo skrbne skrbno skupno slabem slabih slabim slonel sobici sobico sodnik soncem sončne sončni spanec spanja spanje spanju spomin spomni srbske srebra srebrn srebro srečen srečna srečni srečno suknjo ubijal ubogal ubogih ubogim učenci učenec učinek udoben udobja udobje udobna udobno uganil ugibal ugodno umakne uničil upanja upanje upanju uradno vabila vabili vabilo vabilu varnem večina večini večino venček vendar vkljub vnovič vodnik vrabci vrabec vrbami vreden vredna vredne vredni vredno vrnejo vrnila vrnili vseeno jablana jabolko blagoslovljen jugoslovanske jugoslovanski karabinjerjev ljubljanskega nepopravljivo nepremagljiva nepričakovano osvobodilnega prepričevalno presečnikovih pričakovanjem primanjkovalo pripravljenih blagoslovljeno jugoslovanskih pripovedovanja pripovedovanje pripovedovanju'),
(4629, 632, 0, 'displayType', 'int', '1'),
(4630, 632, 0, 'lineLength', 'int', '40'),
(4631, 632, 0, 'lineLengthWhole', 'int', '53'),
(4632, 632, 0, 'markColor', 'string', '000000'),
(4633, 632, 0, 'fontColor', 'string', '223344'),
(4634, 632, 0, 'fontSize', 'int', '50'),
(4635, 632, 0, 'cFontColor', 'string', '000000'),
(4636, 632, 0, 'mFontColor', 'string', '000000'),
(4637, 632, 0, 'cBackColor', 'string', 'FFF277'),
(4638, 632, 0, 'mBackColor', 'string', 'FF7C78'),
(4639, 632, 0, 'backgroundColor', 'string', 'ffffff'),
(4640, 632, 0, 'nacin', 'int', '0'),
(4641, 633, 0, 'text', 'string', 'frft jujz fttf jzzj ftft jzjz tfft zjjz ftjz jzft rtfv uzjm'),
(4642, 633, 0, 'displayType', 'int', '1'),
(4643, 633, 0, 'lineLength', 'int', '20'),
(4644, 633, 0, 'lineLengthWhole', 'int', '53'),
(4645, 633, 0, 'markColor', 'string', '000000'),
(4646, 633, 0, 'fontColor', 'string', '223344'),
(4647, 633, 0, 'fontSize', 'int', '50'),
(4648, 633, 0, 'cFontColor', 'string', '000000'),
(4649, 633, 0, 'mFontColor', 'string', '000000'),
(4650, 633, 0, 'cBackColor', 'string', 'FFF277'),
(4651, 633, 0, 'mBackColor', 'string', 'FF7C78'),
(4652, 633, 0, 'backgroundColor', 'string', 'ffffff'),
(4653, 633, 0, 'nacin', 'int', '0'),
(4654, 634, 0, 'text', 'string', 'ata bit čez črt čut itd iti itn jaz jez kot krt laz let mit miz nit pet pot prt raz rit sit sta ste sto tak tal tam tat teh tej tek tem ter tih tik tim tip tja tla tod tok ton top trd trg tri trn tuj ust ute uti uto vez voz vrt zde zdi zid zla zle zli zlo zlu zna zob zre zrl zve avta avto avtu bati baze bazi bazo biti brat brez brzo čast cest četa čete četi četo čist čita črta črte črti črto čuta čuti cvet dati dete dote doto efez enot fant fazi gazi gost gozd hiti hote ista iste isti isto itak izba izbe izbi izbo izda izve izza jata jeza jeze jezi jezo jezu kart kati kite kiti kito klet kmet knez kost kota kote kotu koza koze kozo krst kuto last lazi lazu leta leti leto letu leze lezi list liza lizi lizo lojz loti mast mati mest meta meti meto miza mize mizi mizo most moti mraz muza nate nato niti note obrt omet opat otel otok otrl ozek ozka ozke ozki ozko ozre ozrl past pazi pest peta pete peti peto piti plat plaz plot polt port pota poti potu prst prti prtu ptic ptič pult pust rast reza riti roza rozi ruta rute ruti ruto sita siti skoz smrt solz sort spat spet stal stan star sten stez stik stoj stok stol svet svit tabo taji taka take taki tako tale tank tatu tebe tebi teče teci teči tega teka teku tele telo tema teme temi temo temu tepe teta tete teti teto tiča tiče tiči tiha tihe tihi tiho tile tina tinč tine tipa tipi tira tiru tisk tlak tleh tlom toča toče toda togo toku tole tomo tona tone toni tono tonu topa topi topo trak tram trav trda trde trdi trdo treh trem trga trge trgu trka trla trlo trma trme trmo trpi trst trta trte trud trup tudi tuja tuje tuji tujo tule tuli tvoj ubit uiti ujet ukaz usta usti uzrl vate vest vezi vnet voza vozi vozu vrat vrst vrta vrti vrtu vrzi vtem vtis vzel vzet vzor zala zali zalo zame zanj zase zate zato zave zbal zbil zbit zbor zdaj zdel zebe zeli zelo zeta zevs zida zidu zima zime zimi zimo zine zlat zlil zlom zmaj zmes znak znal znam znan znoj zobe zofi zofo zora zore zori zoro zrak zrel zrla zrle zrli zrna zrno zroč zven zver zvez zvil zvit zvok zvon agent anton azije aziji bajta bajte bajti bajto bitja bitje blato blatu blizu bluzi bluzo bogat bojte bosta boste botra brata brate brati bratu brest buzet časti cesta ceste cesti cesto često četrt čista čiste čisti čisto čital čutil čutim dajte deset devet dokaz dosti drzen drzna drzne drzno enote fanta fante fantu gates gazil gosta goste gosti gosto gotov gozda gozdu grize groza groze grozi grozo hitel hitro hosti hotel hrast hrbet hrbta hrbtu hunze imata imate imeti iskat istem istih istim izdal izhod izlet izlil izmed iznad izpil izpit izpod izraz izven izvil izvir izvod izvor izziv janez jesti jezen jezik jezil jezna jezni jezno jezom jezus jutra jutri jutro jutru kadet kajti karte katra katri katro kazal kazen kazni kleti kmeta kmete kmetu kosti kotih kozji kozla krsto lazar lazil letal letih letom letos lezel lezla lista liste listi lizal lojze lotil lotus mater matic mesta mesto mestu metal meter metra minut mizah mizar mostu motil motim motno motor mozeg mraza mrazu mrtev mrtva mrtve mrtvi mrtvo mrzel mrzla mrzle mrzli mrzlo muzal muzej načrt najti nazaj naziv nazor nista niste nizek nizka nizke nizki nizko notar noter notri obraz očeta očetu oditi odprt odtod odziv oltar opata opatu opazi ostal oster ostra ostre ostri ostro otoka otoku otrok ozira oziru ozkem ozkih ozrem ozrla ozrli pamet pariz pasti pater pazil pazim pazno pečat pesti petek peter petih petje petra petru pilat pilot plast plati plazi pleza počez poraz poteh potem potez potjo potne potni potno potok potrt poziv pozna pozne pozni pozno pozor priti prost proti prste prsti ptica ptice ptiči pusta puste pusti pusto raste rasti razen razna razne razni razum rezal rezek rezka rezko ritem ritmu ropot rotil satan sezul sitno skozi skrit slast smete smeti smrti solza solze solzi solzo spati stala stale stali stalo stane stanu stara stare stari staro stati stavo steče stena stene steni steno steza steze stezi stezo stike stilu stoje stoji stola stolp stolu stopa stopi stori strah stran strel stric strme strmi strmo stroj strop strup stvar sveta svete sveti sveto svetu svitu tabor tačas tajno takem takih takim takle takoj tamle tanka tanki taval teboj tedaj teden tedna tedne tednu tekel tekla tekle tekli teklo teles teman temle temna temne temni temno tenak tenka tenke tenko tepec tepel tepko tepli tesar tesno tičal tihem tihim tinka tiran tisoč tista tiste tisti tisto tlaku tnalo tobak točki točno topel topla tople topli toplo torbo torej traja trapa trati trava trave travi travo trden trdih trdil trdim trdna trdno treba tremi trese trgal trije troje trpel trpko trstu truda trudi trzne tujca tujci tujec tujem tujih tujim tukaj tulil tvoja tvoje tvoji tvojo ubiti učiti ujeti ukaza ukaze ukazi ukazu ustih verzi vesta veste vesti veter vetra vetru videz vitez vneto votlo vozel vozil vozom vozov vrata vrati vratu vrsta vrste vrsti vrsto vrtel vrzel vsoto vstal vstop vzame vzdih vzela vzele vzeli vzelo vzemi vzeti vzhod vzlic vzmet vznak vzpel vzpon vzrok zabil zaboj začel začne začni začno začul zadaj zadal zadel zadet zadev zadrl zahod zaide zajca zajci zajec zajel zakaj zakon zalil zaliv zaman zanič zanje zanjo zanju zanko zanos zapah zapel zapil zapor zapre zapri zaprl zapro zaprt zardi zares zaril zarja zarje zarjo zarod zaspi zatem zaupa zavil zavit zavod zavoj zavre zazdi zazre zazrl zbala zbere zbira zbora zboru zbral zbran zbudi zbuja zdela zdele zdeli zdelo zdeti zdijo zdoma zdrav zeblo zelen zelje zgaga zgane zgled zgodb zgodi zgolj zgrda zgubi zibal zibel zidal zidar zidom zidov zijal zinil zinka zinko zlata zlate zlati zlato zlatu zlega zlepa zleze zliva zlobe zmaga zmage zmagi zmago zmaje zmeda zmede zmedo zmeni zmore zmoti zmoto znajo znake znaki znala znali znamo znana znane znani znano znate znati znebi znoja znoju znova zobeh zobje zobmi zoper zopet zoprn zraka zraku zrejo zrela zrele zrelo zrkli zrnje zunaj zveni zveri zvest zveza zvezd zveze zvezi zvezo zvija zvite zvito zvoka zvoke zvoki zvoku zvona zvoni bistra bistro bistvo bistvu bodete bodite bogata bogate bogati bogato boriti bratje bratom bratov brazde bresta brezno butnil čakati celoti center centru cestah cestar četrti četrto četudi čezenj čustev čustva čustvo čutijo čutila čutili čutiti cvetja cvetje čvrsto dajati deklet delate delati dihati dobiti dokaze doktor dotlej drznil dvesto enajst enkrat ernest fantje fantom fantov felton funtov ganiti gledat glejte gnezda gnezdo gostje gostom gostov gotovo gozdni gozdom gozdov gresta greste grizel grozil grozna grozne grozno hitela hiteli hkrati hočeta hočete hoditi hotela hotele hoteli hotelo hotelu hrbtom igrati imejte iskati istega izbere izbira izbire izbiro izbral izdaja izdala izdali izgine izgube izgubi izgubo izhaja izhoda izhodu izidor izjema izjeme izlila izpije izpred izraza izrazi izreči izroči izumil izvila izvira janeza janezu jetnik jezdec jezdil jezera jezero jezeru jezika jezike jeziki jeziku jezila jezilo jezusa jezusu jokati katera katere kateri katero katica kazala kazale kazali kazalo kmetih kmetje kmetom kmetov konzul kopita korist korito koritu kratek kratka kratke kratki kratko kultur kupiti kuralt lakota lakote lakoto lastna lastne lastni lastno lepota lepote lepoto lestev letala letalo leteli listek listja listje listov ločiti lojzka lotila martin matere materi matica matija matijo medtem mestih mestne mestni mestno mestom metali metrov milost minute minuto mizici mojzes moliti morata morate moreta morete motila motilo motiti motril mrtvih mrzlem mrzlih muzeju načrta načrte načrtu nadzor napeti napeto napoti napoto nastal nasvet navzoč nehote neznan nimate nizdol nizkem nizkih nizkim norost nosite nositi oblast obraza obraze obrazi obrazu obstal očetje očetom očetov očital očitno odprta odprte odprto odtlej odzval opazil opazno ostaja ostala ostale ostali ostalo ostane ostani ostati ostrim otroci otroka otroke otroki otroku ozadja ozadje ozadju oziral pameti pariza parizu pastir pazila pazili pazite paziti peruti pestjo pestmi petami petimi petsto pisati plasti početi poleti postal poteka poteza poteze potlej potoka potoku potrpi potuje povest povzel pozabi pozimi poznal poznam poznih prazen prazna prazne prazni prazno prizor prosti prosto prstan prstih prstom prstov pustil pustim radost razbil razlog raznih razred razuma razume razvil razvoj recite rezala rezika rozina rozino samoti sedeti sedita sestra sestre sestri sestro seznam sirota sistem skrita skrite skriti skrito slutil smrtjo smrtna smrtni smrtno soboto sokrat solzah spozna sproti spusti srdito stalno stanje stanju starca starci starcu starec starem starih starim starka stavbe stavek stavka steber stegne stekel stekla steklo stenah stezah stično stiska stiske stiski stisne stojim stokal stolov stolpa stopal stopil stopim storil storim strahu strani strast streha strehe strehi streho strela strese strgal strica stricu strmel strmem strogo stroja stroji stropa stropu strugo stvari svatje svetel svetem svetih svetil svetim svetin svetla svetle svetli svetlo svetom taboru takega takemu takimi takole takrat tamkaj tečejo tednih tednov telesa telesu temnem temnih temveč teodor tereza tilnik tineta tinica tisoče tistem tistih tistim tobaka tokrat toliko tolkel tolmun tončka toplem trdega trdijo trdili trditi trebuh trenil tresel tresla tresle tretja tretje tretji tretjo trezen tristo trpela trpeli trpeti truden trudil trudna trudne trudno truplo trznil tujega tujini tvojem tvojih tvojim ukazal ukazov umazan umetno umreti ustavi ustnic utegne vajeti valpet vedeti vestno videti videza videzu vidite viktor viteza vizije vozijo vozila vozili vozilo voziti voznik vozove vozovi vpitje vratca vratih vratom vrniti vrstah vrtela vstala vstali vstane vstani vstati vstopi vstran vzamem vzgoja vzgoje vzgoji vzgojo vzhoda vzhodu vzorec vzroka zabava zabave zabavo zaboga začela začele začeli začelo začeti začnem začuda začudi začuti zadela zadeli zadelo zadene zadere zadeti zadeva zadeve zadevi zadevo zadnja zadnje zadnji zadnjo zagnal zagrmi zahoda zahodu zajame zajček zajela zajema zajeti zajtrk zaklad zaklel zakona zakone zakoni zakonu zalila zaliva zaloge zameno zamere zameri zamude zamudo zanima zapela zapeli zapoje zaporu zaprla zaprle zaprli zaprlo zaprta zaprte zaprti zaprto zaradi zardel zaspal zasuče zaupal zaupam zaupno zaveda zavese zaveso zavest zaveze zavije zavila zavili zavist zavita zaviti zavito zavodu zavpil zavrne zavrti zavzel zaznal zazrla zbegan zbiral zbirke zbirko zbogom zbolel zbrala zbrali zbralo zbrana zbrane zbrani zbrano zbrati zbudil zdajci zdajle zdaleč zdrava zdrave zdravi zdravo zdrsne zdrzne zelena zelene zeleni zeleno zemlja zemlje zemlji zemljo zganil zgodaj zgodba zgodbe zgodbo zgodil zgoraj zgrabi zgrudi zgubil zibala ziblje zidove zidovi zimsko zlagal zlahka zlasti zlatih zlatim zlatom zletel zlezel zlezla zlobno zločin zlomil zmagal zmajal zmedel zmeden zmenil zmeraj zmerom zmogel zmogli zmotil značaj znakov znanca znance znanci znanec znanih znanja znanje znatno znebil znorel zoprno zračni zrakom zrasel zrasla zrasli zraven zravna zrcalo zrcalu zvečer zvedel zvenel zvesta zvesti zvesto zvezda zvezde zvezdo zvezek zvijal zvonce zvonci zvonec zvonik zvonil zvrnil devetnajstega dokumentacijo dvajsetletnik gostilničarja gostilničarju intelektualci intelektualno izpostavljeni izpregovorila izpregovoriti ljubosumnosti metaprogramer najverjetneje naklonjenosti nepravičnosti neprijetnosti nerazkrivanju občutljivosti petinpetdeset petintrideset poslednjikrat predstavljajo predstavljala predstavljali predstavljati presenetljivo prijateljstva prijateljstvo prijateljstvu pripovedovati prizanesljivo proizvajalcev prostovoljcev radovednostjo razburjenosti raziskovalcev razpravljanja razpravljanje razpravljanju skrivnostnega teritorialcev tradicionalni tradicionalno zadovoljstvom znanstvenikov dostojanstveno konservatoriju pripravljenost tradicionalnih ustvarjalnosti'),
(4655, 634, 0, 'displayType', 'int', '1'),
(4656, 634, 0, 'lineLength', 'int', '40'),
(4657, 634, 0, 'lineLengthWhole', 'int', '53'),
(4658, 634, 0, 'markColor', 'string', '000000'),
(4659, 634, 0, 'fontColor', 'string', '223344'),
(4660, 634, 0, 'fontSize', 'int', '50'),
(4661, 634, 0, 'cFontColor', 'string', '000000'),
(4662, 634, 0, 'mFontColor', 'string', '000000'),
(4663, 634, 0, 'cBackColor', 'string', 'FFF277'),
(4664, 634, 0, 'mBackColor', 'string', 'FF7C78'),
(4665, 634, 0, 'backgroundColor', 'string', 'ffffff'),
(4666, 634, 0, 'nacin', 'int', '0'),
(4667, 635, 0, 'text', 'string', 'čščš l.l. lol. l.lo l..l l..l čššč šččš ščl. .lčš čpčš pčpš'),
(4668, 635, 0, 'displayType', 'int', '1'),
(4669, 635, 0, 'lineLength', 'int', '20'),
(4670, 635, 0, 'lineLengthWhole', 'int', '53'),
(4671, 635, 0, 'markColor', 'string', '000000'),
(4672, 635, 0, 'fontColor', 'string', '223344'),
(4673, 635, 0, 'fontSize', 'int', '50'),
(4674, 635, 0, 'cFontColor', 'string', '000000'),
(4675, 635, 0, 'mFontColor', 'string', '000000'),
(4676, 635, 0, 'cBackColor', 'string', 'FFF277'),
(4677, 635, 0, 'mBackColor', 'string', 'FF7C78'),
(4678, 635, 0, 'backgroundColor', 'string', 'ffffff'),
(4679, 635, 0, 'nacin', 'int', '0'),
(4680, 636, 0, 'text', 'string', 'baš. boš. češ. daš. duš. hiš. ješ. koš. miš. naš. peš. piš. šah. šal. šef. šel. šla. šle. šli. šlo. šok. šol. šop. šum. uši. vaš. veš. čaše. čašo. diši. duša. duše. duši. dušo. greš. hiša. hiše. hiši. hišo. imaš. išče. maša. maše. maši. mašo. meša. miši. naša. naše. naši. našo. paša. pašo. piše. piši. reši. šala. šale. šali. šalo. šefa. šele. šest. šiba. šibe. šibo. šine. šipe. šipo. širi. šiva. škaf. škof. smeš. šola. šole. šoli. šolo. spiš. štab. štel. šuma. šumi. tiše. ušel. ušes. ušla. ušlo. vaša. vaše. vaši. vašo. više. všeč. znaš. bivši. bodeš. bojiš. briše. čakaš. češke. češki. češko. čutiš. delaš. dobiš. duška. greši. hišah. hišna. hišni. hočeš. hodiš. hujša. hujše. hujši. iščem. kaščo. kokoš. laški. laško. lepša. lepše. lepši. meniš. miloš. moraš. moreš. moška. moške. moški. moško. našel. našem. naših. našim. našla. našli. nimaš. nočeš. obšel. obšla. obšlo. odšel. odšla. odšli. ošine. piješ. pišem. plašč. pleše. plošč. pošto. poveš. pušča. puške. puško. rajši. rečeš. rekši. rešen. rešil. šalil. sediš. šibka. šimen. šinil. širil. širok. širše. škoda. škode. škodi. škodo. skuša. sliši. šofer. šopek. šotor. štaba. šteje. šteti. štiri. tašča. taščo. tišči. trušč. ušesa. ušesi. ušesu. vašem. vaših. vašim. vaške. vaški. vešče. vidiš. višja. višje. višji. vrneš. zašel. zašla. boljša. boljše. boljši. boljšo. češkem. čeških. češnje. daljše. daljši. dišala. dišalo. gašper. gledaš. grešil. hišami. hruška. hruške. hruško. iščejo. iščete. kakšen. kakšna. kakšne. kakšni. kakšno. kašelj. kokoši. košaro. košato. košček. koščen. koščke. krajši. kvišku. manjša. manjše. manjši. manjšo. maruša. miloša. milošu. mišice. misliš. mlajša. mlajši. moških. moškim. mošnjo. moštvo. najdeš. nalašč. nanaša. našega. našemu. našimi. nemška. nemške. nemški. nemško. obišče. obriše. ošabno. ošinil. pišejo. plašča. plašču. plašno. plošče. plošči. ploščo. počneš. poišče. pojdeš. pošast. pošlje. poštar. pošten. poštev. poznaš. prašni. praviš. prešel. prešla. prešlo. prideš. prišel. prišla. prišle. prišli. prišlo. puščal. razšla. razšli. rešila. rešili. rešiti. ščepec. šepnil. šestih. šinila. šinilo. širijo. širila. široka. široke. široki. široko. škatle. škatlo. skušal. slabše. slabši. slišal. slišim. slišiš. slišno. smešen. smešno. šolske. šolski. šotora. šotoru. spušča. starše. starši. štefan. štirih. štirje. stiški. stojiš. študij. šumela. šumelo. takšen. takšna. takšne. takšni. takšno. tiščal. tišina. tišine. tišini. tišino. turške. turški. ušesih. ušesom. vašega. vašemu. vaških. višine. višini. višino. vmešal. vpraša. vštric. vzameš. zmešan. znašel. znašla. znašli. zrušil. zviška. muzikološkega. nevoščljivost. nezaslišanega. povpraševanje. premišljevala. premišljevali. premišljevati. prevzvišenost. prisluškovala. pustolovščina. pustolovščine. računalniškem. računalniških. računalništva. šestindvajset. strokovnjaško. računalniškega. štiriindvajset.'),
(4681, 636, 0, 'displayType', 'int', '1'),
(4682, 636, 0, 'lineLength', 'int', '40'),
(4683, 636, 0, 'lineLengthWhole', 'int', '53'),
(4684, 636, 0, 'markColor', 'string', '000000'),
(4685, 636, 0, 'fontColor', 'string', '223344'),
(4686, 636, 0, 'fontSize', 'int', '50'),
(4687, 636, 0, 'cFontColor', 'string', '000000'),
(4688, 636, 0, 'mFontColor', 'string', '000000'),
(4689, 636, 0, 'cBackColor', 'string', 'FFF277'),
(4690, 636, 0, 'mBackColor', 'string', 'FF7C78'),
(4691, 636, 0, 'backgroundColor', 'string', 'ffffff'),
(4692, 636, 0, 'nacin', 'int', '0'),
(4693, 637, 0, 'text', 'string', 'čžčž ččžž žžčč čččž žžžč čžžč čžžž žččž jčjž jžžj jžžž'),
(4694, 637, 0, 'displayType', 'int', '1'),
(4695, 637, 0, 'lineLength', 'int', '20'),
(4696, 637, 0, 'lineLengthWhole', 'int', '53'),
(4697, 637, 0, 'markColor', 'string', '000000'),
(4698, 637, 0, 'fontColor', 'string', '223344'),
(4699, 637, 0, 'fontSize', 'int', '50'),
(4700, 637, 0, 'cFontColor', 'string', '000000'),
(4701, 637, 0, 'mFontColor', 'string', '000000'),
(4702, 637, 0, 'cBackColor', 'string', 'FFF277'),
(4703, 637, 0, 'mBackColor', 'string', 'FF7C78'),
(4704, 637, 0, 'backgroundColor', 'string', 'ffffff'),
(4705, 637, 0, 'nacin', 'int', '0'),
(4706, 638, 0, 'text', 'string', 'brž dež jež laž mož nož ože oži ožo riž rož žak žal žar ždi žel žen žep žge žid žig žil živ žre žrl anže beži blaž boža drže drži jože kaže koža kože koži kožo križ laže laži leže leži liže luže luži moža može možu mrož neža neže neži nežo niže nože opaž ožje ožji reže reži režo riža roža rože rožo seže svež teža teže teži težo toži veža veže veži vežo vrže žabe žaga žage žagi žago žari ždel žeja žeje žejo žela želi žena žene ženi ženo žepa žepe žepu žgal žice žico žila žile žita žito živa žive živi živo žleb žoga žogo žolč bežal bežen bežno bliža bliže božal božič božja božje božji božjo čudež delež dežel dežja dežju draži družb druži držal držav držeč držim držiš fižol jožef judež južne južni južno kožuh križa križe križi križu kroži lažem lažeš lažje lažji lažjo lažni ležal ležeč ležim madež možak možeh možem možje možmi možna možne možni možno mreža mreže mreži mrežo nežen nežna nežne nežni nežno nižje nižji nožem omoži opaža ožbej oženi oživi papež plaži požar požre požrl preži revež režal režim rožco rožni rožno sedež služb služi sneži sveže sveži svežo težak težav težek težja težje težka težke težki težko tjaža tjažu tomaž tožba tožbe tožbo tožil tožit tožno tržne tržni tržno ukaže ureže uživa važen važna važne važni važno vežna vidži vrvež vržem vržen žagal žalil žamet žarek žarel žarke žarki zbeži žejen žejna žejni želel želim želiš želja želje želji željo želva želve ženam ženil ženin žensk žepih žetev žezlo žgalo žilah žival živce živci živel živem živež živih živim živio živiš život žlebu žlica žlice žlico žrela žrelo žrtev žrtve žulji župan žveči žvižg anglež anžetu bežala bežali bežati bežite blažen blažič bližal bližje božala božena božiča božiču božjem božjih božjim čudeže čudeži čudežu deleža dežela dežele deželi deželo dežjem dežnik dežuje dolžan dolžna dolžni doseže doživi dražil drobiž družba družbe družbi družbo družil družin družno držala držale držali držalo držati država države državi državo držijo držimo držite fižola izkaže izraža jožefa južino južnem južnih kažejo kražem križan križem križev krožil lažejo ležala ležale ležali ležalo ležati ležeče ležeči ležijo ležita madžar matjaž mežnar mladež množic možaka možato možnih mrežno najbrž nežnih nežnim obdrži obleži odleže odloži odreže odžene orožja orožje orožju otožen otožna otožne otožni otožno oženil oženim oživel oživil papeža pokaže pokaži poleže položi poseže poželi požene poženi požira požrla požrli prižge reveža reveži režali režejo rožami rožice sadeži sedeža sedeže sedežu služba službe službi službo služil služim služit snežni snežno sproži stežaj stežka straža straže straži stražo streže suženj sužnja sužnji svežem svežih težava težave težavo težilo težkem težkih težkim težnje tolaži tomaža tomažu tožijo tožila tožili tožiti tržani tržnih ubožec užitek užitka uživaj užival uživam uživaš važnih vežnih vlažna vlažne vlažni vlažno vložil vožnja vožnje vožnji vožnjo vražje vržejo vzdolž zadrži žalega zaleže žalost žameta zamiži žandar žareča žareče žarela žarele žarelo zareži žarijo žarkov zaužil zažene zažgal zbežal zdržal žebelj žeblji želela želeli želeti železa železo želijo želimo želite željah željan željno ženami ženejo ženica ženice ženico ženina ženinu ženiti ženska ženske ženski žensko žganja žganje žganju židane živali živcev živčne živčni živčno živega živela živele živeli živelo živemu živeti živeža živijo živimi živimo živina živine živini živino živita živite života životu zložil zmožen zmožni župana županu župnik žvečil žvižga aboridžinskih izobraževanje nepotrpežljiv pristriženimi sramežljivost ženitovanjsko življenjskega nepotrpežljivo potrpežljivost sramežljivosti potrpežljivosti potrpežljivostjo'),
(4707, 638, 0, 'displayType', 'int', '1'),
(4708, 638, 0, 'lineLength', 'int', '40'),
(4709, 638, 0, 'lineLengthWhole', 'int', '53'),
(4710, 638, 0, 'markColor', 'string', '000000'),
(4711, 638, 0, 'fontColor', 'string', '223344'),
(4712, 638, 0, 'fontSize', 'int', '50'),
(4713, 638, 0, 'cFontColor', 'string', '000000'),
(4714, 638, 0, 'mFontColor', 'string', '000000'),
(4715, 638, 0, 'cBackColor', 'string', 'FFF277'),
(4716, 638, 0, 'mBackColor', 'string', 'FF7C78'),
(4717, 638, 0, 'backgroundColor', 'string', 'ffffff'),
(4718, 638, 0, 'nacin', 'int', '0'),
(4719, 639, 0, 'text', 'string', 'a b c č d e f g h i j k l m n o p r s š t u v z ž'),
(4720, 639, 0, 'displayType', 'int', '5'),
(4721, 639, 0, 'lineLength', 'int', '50'),
(4722, 639, 0, 'lineLengthWhole', 'int', '53'),
(4723, 639, 0, 'markColor', 'string', '000000'),
(4724, 639, 0, 'fontColor', 'string', '223344'),
(4725, 639, 0, 'fontSize', 'int', '50'),
(4726, 639, 0, 'cFontColor', 'string', '000000'),
(4727, 639, 0, 'mFontColor', 'string', '000000'),
(4728, 639, 0, 'cBackColor', 'string', 'FFF277'),
(4729, 639, 0, 'mBackColor', 'string', 'FF7C78'),
(4730, 639, 0, 'backgroundColor', 'string', 'ffffff'),
(4731, 639, 0, 'nacin', 'int', '0'),
(4771, 643, 0, 'text', 'string', 'delfini so sesalci, ki živijo v morju in so odlični plavalci. so družabne in igrive živali. njihova telesa so popolnoma prilagojena življenju v vodi. telo je tako oblikovano, da z lahkoto drsijo skozi vodo. tudi dlake nimajo, ker bi jih ta ovirala pri plavanju. pred mrazom jih varuje plast maščevje pod kožo.  živijo v jatah. radi skačejo iz vode ali spremljajo ladje. ponekod izvajajo tudi terapijo plavanja z delfini. orientirajo in sporazumevajo se z zvočnimi signali. delfini imajo jezik, sestavljen iz različnih zvokov. drugim delfinom lahko povedo, kje je hrana ali jih opozorijo na nevarnosti. njihove klice ljudje lahko slišimo, kadar so nad vodo, kadar so pod vodo pa ne. delfini oddajajo tudi posebne signale, s katerimi opazujejo okolico. v glavi imajo kepo maščobe, skozi katero spuščajo signale v okolico. ko ti signali zadenejo ob kak predmet, se odbijejo in se vrnejo nazaj k delfinu. delfin tako ve, kaj se nahaja v okolici. poznate žival po imenu jazbec spada po vašem mnenju v skupino zimskih zaspancev poglejmo jazbec je znan po svojih nočnih pohajanjih po navadi se giblje počasi in previdno je dober plezalec če je treba tudi plava voh in sluh ima dobro razvita medtem ko vidi slabše pozimi večinoma miruje v jazbini vendar to ni pravo zimsko spanje je le nekoliko bolj umirjen kot v drugih letnih časih zaradi veliko podkožne tolšče dolgo zdrži brez hrane jeseni in pozimi je povprečna teža jazbecev večja kot spomladi pozimi zapušča brlog tudi ko je zunaj globok sneg in hud mraz labirint samo jaz poznam pot iz njega  jazbečevo podzemno domovanje je sestavljeno iz labirinta hodnikov in kotlov v katerih se sam odlično znajde v bližini jazbine ima jazbec posebno latrino oziroma toaletne prostore kjer se iztreblja kaj bomo pa danes jedli jazbec od rastlinske hrane najraje posega po koreninah in gomoljih obožuje tudi sadje razna žita jagodičevje žir želod in druge plodove največji delež v prehrani pa zavzemajo deževniki ko se jazbečji mladiči skotijo ostanejo pod zemljo v labirintnih rovih še kakšna dva meseca življenjska doba jazbeca je precej visoka in sicer lahko dočaka tudi do petnajst let kar petintrideset procentov živali pa ne preživi tretjega leta naravna sovražnika jazbecev sta volk in ris'),
(4772, 643, 0, 'displayType', 'int', '4'),
(4773, 643, 0, 'lineLength', 'int', '50'),
(4774, 643, 0, 'lineLengthWhole', 'int', '50'),
(4775, 643, 0, 'markColor', 'string', '000000'),
(4776, 643, 0, 'fontColor', 'string', '223344'),
(4777, 643, 0, 'fontSize', 'int', '50'),
(4778, 643, 0, 'cFontColor', 'string', '000000'),
(4779, 643, 0, 'mFontColor', 'string', '000000'),
(4780, 643, 0, 'cBackColor', 'string', 'FFF277'),
(4781, 643, 0, 'mBackColor', 'string', 'FF7C78'),
(4782, 643, 0, 'backgroundColor', 'string', 'ffffff'),
(4783, 643, 0, 'nacin', 'int', '0'),
(4784, 644, 0, 'instructions', 'string', 'Kopalniška zavesa ima luknjo. Katera slika jo zakrpa?'),
(4785, 644, 0, 'selectN', 'int', '9'),
(4786, 644, 0, 'files', 'filelist', 'files/manjkajoci del/vzorec/sivinsko/metulj/metuj (1).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (10).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (2).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (3).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (5).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (6).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (4).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (7).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (8).svg|files/manjkajoci del/vzorec/sivinsko/metulj/metuj (9).svg'),
(4787, 644, 0, 'masks', 'filelist', 'files/manjkajoci del/izrez/krog.svg|files/manjkajoci del/izrez/kvadrat.svg|files/manjkajoci del/izrez/osemkotnik.svg'),
(4788, 645, 0, 'crke', 'stringlist', 'a|b|c|č|d|e|f|g|h|i|j|k|l|m|n|o|p|r|s|š|t|u|v|z|ž'),
(4789, 645, 0, 'vrstaZacetneAbecede', 'int', '0'),
(4792, 647, 0, 'besede', 'stringlist', 'sonce|maslo|jagoda|morje|korala|dežnik|medved|šolar|čistilo|mačka|češnja|vrtiček|čebula|čmrlj|vijak|trikot|zmaga|skodela|kostanj'),
(4793, 647, 0, 'vrstaZacetneAbecede', 'int', '0'),
(4794, 648, 0, 'besede', 'stringlist', 'sonce|maslo|jagoda|morje|korala|dežnik|medved|šolar|čistilo|mačka|češnja|vrtiček|čebula|čmrlj|vijak|trikot|zmaga|skodela|kostanj'),
(4795, 648, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4796, 651, 0, 'crke', 'stringlist', 'a|b'),
(4797, 651, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4798, 653, 0, 'crke', 'stringlist', 'a|b|g|k|c'),
(4799, 653, 0, 'vrstaZacetneAbecede', 'int', '0'),
(4800, 654, 0, 'crke', 'stringlist', 'a|b|c'),
(4801, 654, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4802, 655, 0, 'crke', 'stringlist', 'a|b|c|č|d'),
(4803, 655, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4804, 656, 0, 'crke', 'stringlist', 'a|b|c|č|d|e'),
(4805, 656, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4806, 657, 0, 'crke', 'stringlist', 'a|b|c|č|d|e'),
(4807, 657, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4808, 658, 0, 'crke', 'stringlist', 'a|b|c|č|d|e|f|g|h|i|j|k|l|m|n|o|p|r|s|š|t|u|v|z|ž'),
(4809, 658, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4810, 659, 0, 'crke', 'stringlist', 'a|b|c|č|d|e|f'),
(4811, 659, 0, 'vrstaZacetneAbecede', 'int', '1'),
(4812, 660, 0, 'besede', 'stringlist', 'sonce|maslo|jagoda|morje|korala|dežnik|medved|šolar|čistilo|mačka|češnja|vrtiček|čebula|čmrlj|vijak|trikot|zmaga|skodela|kostanj'),
(4813, 660, 0, 'vrstaZacetneAbecede', 'int', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `zs_class`
--
ALTER TABLE `zs_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `zs_constant`
--
ALTER TABLE `zs_constant`
  ADD PRIMARY KEY (`constant_id`);

--
-- Indexes for table `zs_game`
--
ALTER TABLE `zs_game`
  ADD PRIMARY KEY (`game_id`);

--
-- Indexes for table `zs_instance`
--
ALTER TABLE `zs_instance`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `zs_score`
--
ALTER TABLE `zs_score`
  ADD PRIMARY KEY (`score_id`);

--
-- Indexes for table `zs_session`
--
ALTER TABLE `zs_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `zs_student`
--
ALTER TABLE `zs_student`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `zs_user`
--
ALTER TABLE `zs_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `zs_variable`
--
ALTER TABLE `zs_variable`
  ADD PRIMARY KEY (`variable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `zs_class`
--
ALTER TABLE `zs_class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `zs_constant`
--
ALTER TABLE `zs_constant`
  MODIFY `constant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11661;
--
-- AUTO_INCREMENT for table `zs_game`
--
ALTER TABLE `zs_game`
  MODIFY `game_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=365;
--
-- AUTO_INCREMENT for table `zs_instance`
--
ALTER TABLE `zs_instance`
  MODIFY `instance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=661;
--
-- AUTO_INCREMENT for table `zs_score`
--
ALTER TABLE `zs_score`
  MODIFY `score_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=431;
--
-- AUTO_INCREMENT for table `zs_student`
--
ALTER TABLE `zs_student`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `zs_user`
--
ALTER TABLE `zs_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `zs_variable`
--
ALTER TABLE `zs_variable`
  MODIFY `variable_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4814;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
